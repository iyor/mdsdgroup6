/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IOFactoryImpl extends EFactoryImpl implements IOFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IOFactory init() {
		try {
			IOFactory theIOFactory = (IOFactory)EPackage.Registry.INSTANCE.getEFactory(IOPackage.eNS_URI);
			if (theIOFactory != null) {
				return theIOFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new IOFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IOFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case IOPackage.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			case IOPackage.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case IOPackage.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES: return createHotelAdministratorProvides();
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES: return createHotelReceptionistProvides();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		HotelCustomerProvidesImpl hotelCustomerProvides = new HotelCustomerProvidesImpl();
		return hotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelAdministratorProvides createHotelAdministratorProvides() {
		HotelAdministratorProvidesImpl hotelAdministratorProvides = new HotelAdministratorProvidesImpl();
		return hotelAdministratorProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelReceptionistProvides createHotelReceptionistProvides() {
		HotelReceptionistProvidesImpl hotelReceptionistProvides = new HotelReceptionistProvidesImpl();
		return hotelReceptionistProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IOPackage getIOPackage() {
		return (IOPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IOPackage getPackage() {
		return IOPackage.eINSTANCE;
	}

} //IOFactoryImpl
