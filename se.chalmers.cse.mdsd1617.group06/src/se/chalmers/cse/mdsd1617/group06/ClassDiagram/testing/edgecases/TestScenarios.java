package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.edgecases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;

public class TestScenarios {

	protected HotelReceptionistProvides receptionist;
	protected HotelAdministratorProvides admin;

	
	@Before
	public void init() {
		this.receptionist = IOFactory.eINSTANCE.createHotelReceptionistProvides();
		this.admin = IOFactory.eINSTANCE.createHotelAdministratorProvides();
		
		//Creates a credit card
		try{
			se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bankingAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires
					.instance();
			bankingAdmin.addCreditCard("12345678", "000", 11, 16, "Peter", "Pan");
			
			bankingAdmin.makeDeposit("12345678", "000", 15, 16, "Peter", "Pan", 2000.0);

		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
	}
	
	/*
	 * 3.8 A (0) customer has created his booking and wants to book 1 room but has not yet added
the room when another customer (1) creates and confirms 3 rooms.  Customer 0 wants then
to confirm his booking. Booking B checks in.
	 */
	@Test
	public void twoCustomersBookingAtTheSameTimeTest() {
		admin.startup(1);
		
		admin.addRoomType(1, "single room", 300.0);
		admin.addRoomType(2, "double room", 600.0);
		admin.addRoom(101, "single room");
		admin.addRoom(102, "single room");
		admin.addRoom(103, "double room");
		
		//Creates booking A
		int bookingAID = receptionist.initiateBooking("Anna", "20141010", "20141015", "A");
		
		//Creates and confirms booking B and added three rooms to this booking
		int bookingBID = receptionist.initiateBooking("Bertil", "20141010", "20141015", "B");
		receptionist.addRoomToBooking("single room", bookingBID);
		receptionist.addRoomToBooking("single room", bookingBID);
		receptionist.addRoomToBooking("double room", bookingBID);
		receptionist.confirmBooking(bookingBID);
		
		//Booking A wants to add 1 room
		boolean isAddedOneRoomToA = receptionist.addRoomToBooking("single room", bookingAID);
		boolean isConfirmedA = receptionist.confirmBooking(bookingAID);
		assertFalse("No rooms are added to booking A", isAddedOneRoomToA);
		assertFalse("Booking A is not cofirmed ", isConfirmedA);
		
		int r1, r2, r3;
		r1 = receptionist.checkInRoom("single room", bookingBID);
		r2 = receptionist.checkInRoom("single room", bookingBID);
		r3 = receptionist.checkInRoom("double room", bookingBID);
		boolean isCheckedInB = r1 >= 0 && r2 >= 0 && r3 >= 0; 
		assertTrue("Booking B and its rooms are checked in", isCheckedInB);
	}
		
	/*
	 * A (1) customer creates a booking with 2 rooms (single), then another customer creates a booking
with 2 room (one single two double). But will only get 1 since there are only three rooms in the hotel. 
* 
*/
	@Test
	public void testLessRoomsAvaialbleThanDesired() {
		admin.startup(1);
		
		admin.addRoomType(1, "single room", 300.0);
		admin.addRoomType(2, "double room", 600.0);
		admin.addRoom(101, "single room");
		admin.addRoom(102, "single room");
		admin.addRoom(103, "double room");
		
		//Creates booking A
		int bookingAID = receptionist.initiateBooking("Anna", "20141010", "20141015", "A");
		receptionist.addRoomToBooking("single room", bookingAID);
		receptionist.addRoomToBooking("single room", bookingAID);
		receptionist.confirmBooking(bookingAID);
		
		
		//Creates and confirms booking B and added three rooms to this booking
		int bookingBID = receptionist.initiateBooking("Bertil", "20141010", "20141015", "B");
		assertFalse("No single rooms are available", receptionist.addRoomToBooking("single room", bookingBID));
		assertTrue("Double room is added to this booking", receptionist.addRoomToBooking("double room", bookingBID));

	}
	
	/*
	 * 4.8 A customer wants to pay before checking out
	 * 
	 */
	@Test
	public void testPaysBeforeCheckedOut() {
		admin.startup(1);

		admin.addRoomType(1, "single room", 300.0);
		admin.addRoom(101, "single room");
		
		//Creates booking A
		int bookingAID = receptionist.initiateBooking("Alex", "20141010", "20141015", "A");	
		receptionist.addRoomToBooking("single room", bookingAID);
		receptionist.confirmBooking(bookingAID);
		
		boolean isNotPaid = receptionist.payDuringCheckout("12345678", "000", 15, 16, "Peter", "Pan");
		assertFalse("Booking A is not paid ", isNotPaid);
		
	}
	
    /*
     * Blocking causes mayhem.
     */
	@Test
    public void testBlocking() {
            admin.startup(1);
            admin.addRoomType(1, "single room", 300.0);
            admin.addRoom(101, "single room");

            int bookingAID = receptionist.initiateBooking("Alex", "20141010", "20141015", "A");
            assertTrue(receptionist.addRoomToBooking("single room", bookingAID));
            assertTrue(receptionist.confirmBooking(bookingAID));

            admin.blockRoom(101);
            EList<Room> availableRooms = receptionist.initiateCheckIn(bookingAID);
            assertTrue("Even though they booked it in advance, due to blocking the room is not available anyore", availableRooms.isEmpty());
            
            admin.unblockRoom(101);
            int bookingBID = receptionist.initiateBooking("Balex", "20141010", "20141015", "B");
            
            // The reservations made by the previous booking still hold.
            assertFalse(receptionist.addRoomToBooking("single room", bookingBID));
            availableRooms = receptionist.initiateCheckIn(bookingAID);
            assertFalse("Now the room is available again", availableRooms.isEmpty());
            
            // Is blocked during the course of a check in.
            admin.blockRoom(101);
            assertTrue("Still no rooms available", 0 > receptionist.checkInRoom("single room", bookingAID));
            assertFalse("Can't check in, even though it was marked available", receptionist.checkInRoomById(bookingAID, availableRooms.get(0).getId()));
            
            admin.unblockRoom(101);
            int roomId = receptionist.checkInRoom("single room", bookingAID);
            assertTrue("Now checking in should work", roomId >= 0);
            
            admin.blockRoom(101);
            double price = receptionist.initiateRoomCheckout(101, bookingAID);
            assertTrue("It should be possible to check out a blocked room", price > 0);

            assertFalse("Room should remain blocked after it is checked out", receptionist.addRoomToBooking("single room", bookingBID));

            admin.unblockRoom(101);
            assertFalse("Room should be made available again", receptionist.addRoomToBooking("single room", bookingBID));
	}
	
	@Test
	public void testExtraReset() {
		admin.startup(1);
		
		int roomId1 = 101;
		
		admin.addRoomType(1, "single bed", 300.0);
		admin.addRoom(roomId1, "single bed");
		
		int booking1 = receptionist.initiateBooking("Beng", "20160101","20160102", "Entrålarenn");
		receptionist.addRoomToBooking("single bed", booking1);
		receptionist.confirmBooking(booking1);
		receptionist.checkInRoomById(booking1, roomId1);
		receptionist.addExtra(roomId1,"Room service", 200.0f);
		
		double price1 = receptionist.initiateCheckout(booking1);
		assertEquals(500, price1, 0.001);
		
		int booking2 = receptionist.initiateBooking("Bang","20160103","20160104", "Olufsen");
		receptionist.addRoomToBooking("single bed", booking2);
		receptionist.confirmBooking(booking2);
		receptionist.checkInRoomById(booking2, roomId1);
		
		int roomId2 = receptionist.getBookinghandler().bookingcatalogue().getBooking(booking2).getRoomsAssociated().get(0).getId();
		//Make sure the room from booking2 is the same as in booking1
		assertEquals(roomId1, roomId2);
		
		double price2 = receptionist.initiateCheckout(booking2);
		//Price should not be affected by extras from past booking of room
		assertEquals(300, price2, 0.001);
		
		
	}
	
	/*
	 * A customer A wants to book one room but the hotel is fully booked. Another customer edits the period of her booking. 
	 * There is available rooms  for A.
	 */
	@Test
	public void testEditBooking() {
		admin.startup(1);
		
		admin.addRoomType(1, "single room", 300.0);
		admin.addRoom(101, "single room");
		admin.addRoom(102, "single room");
		
		int booking1 = receptionist.initiateBooking("Beng", "20160101","20160302", "Entrålarenn");
		receptionist.addRoomToBooking("single room", booking1);
		receptionist.confirmBooking(booking1);
		receptionist.checkInRoomById(booking1, 101);

		int booking2 = receptionist.initiateBooking("Anna", "20160101","20160202", "A");
		receptionist.addRoomToBooking("single room", booking2);
		receptionist.addRoomToBooking("default", booking2);

		receptionist.confirmBooking(booking2);
		receptionist.checkInRoomById(booking1, 102);

		int booking3 = receptionist.initiateBooking("Bertil","20160120","20160202", "B");
		EList<FreeRoomTypesDTO> list = receptionist.getFreeRooms(1, "20160120","20160202");
		assertFalse("No single rooms avaialbe", receptionist.addRoomToBooking("single room", booking3));
		assertEquals("There is no rooms avaialbe during this period", 0, list.size());

		receptionist.editBooking(booking1,  "20160101","20160119");
		assertFalse("After 1 edits his booking, 3 should be able to book", receptionist.addRoomToBooking("single room", booking3));
	
	}
	
	/*
	 * Robustness test: Make sure the system doesn't throw exceptions on bad values.
	 * TODO: Add more attempts on each method, with a combination of valid and invalid
	 * parameters.
	 */
	@Test
	public void robustnessTest() {
		admin.startup(-10);
		admin.startup(10);
		
		admin.addRoomType(-1, null, -100);
		admin.blockRoom(-1);
		admin.unblockRoom(-1);
		admin.changeRoomTypeRoom(-2, "");
		admin.removeRoom(-1);
		admin.removeRoomType(null);
		admin.updateRoomType(null, -123, "adf", -100);
		
		receptionist.addExtra(-1, null, -199);
		receptionist.addRoomToBooking(null, -1);
		receptionist.cancelBooking(-11);
		receptionist.checkInRoom("asfa", -10);
		receptionist.confirmBooking(-2);
		receptionist.convertDate(null);
		receptionist.editBooking(0, null);
		receptionist.editBooking(-1, null, "");
		receptionist.editBooking(-1, null, "asdf", null);
		receptionist.getFreeRooms(-12, "sadd", null);
		receptionist.initiateBooking(null, null, null, null);
		receptionist.initiateCheckIn(-1);
		receptionist.initiateCheckout(-1);
		receptionist.initiateRoomCheckout(-1234, -32);
		receptionist.listCheckIns(null);
		receptionist.listCheckIns(null, null);
		receptionist.listCheckOuts(null);
		receptionist.listCheckOuts(null, null);
		receptionist.listOccupiedRooms(null);
		receptionist.payDuringCheckout(null, null, -1, -2, null, null);
		receptionist.payRoomDuringCheckout(-1, null, null, -1, -2, null, null);
		
	}
}
