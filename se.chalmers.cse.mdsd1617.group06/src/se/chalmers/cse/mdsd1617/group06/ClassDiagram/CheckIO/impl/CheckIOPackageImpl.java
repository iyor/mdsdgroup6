/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CheckIOPackageImpl extends EPackageImpl implements CheckIOPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkIOCatalogueEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkInEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkOutEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomIdToBookingIdEntryEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkIOHandlerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CheckIOPackageImpl() {
		super(eNS_URI, CheckIOFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CheckIOPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CheckIOPackage init() {
		if (isInited) return (CheckIOPackage)EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI);

		// Obtain or create and register package
		CheckIOPackageImpl theCheckIOPackage = (CheckIOPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CheckIOPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CheckIOPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		IOPackageImpl theIOPackage = (IOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) instanceof IOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) : IOPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);

		// Create package meta-data objects
		theCheckIOPackage.createPackageContents();
		theIOPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theBookingPackage.createPackageContents();

		// Initialize created meta-data
		theCheckIOPackage.initializePackageContents();
		theIOPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCheckIOPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CheckIOPackage.eNS_URI, theCheckIOPackage);
		return theCheckIOPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckIOCatalogue() {
		return checkIOCatalogueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOCatalogue_CheckIns() {
		return (EReference)checkIOCatalogueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOCatalogue_CheckOuts() {
		return (EReference)checkIOCatalogueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOCatalogue__ClearAllCheckIO() {
		return checkIOCatalogueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOCatalogue__RegisterCheckIn__CheckIn() {
		return checkIOCatalogueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOCatalogue__RegisterCheckOut__CheckOut() {
		return checkIOCatalogueEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckIn() {
		return checkInEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckIn_Date() {
		return (EAttribute)checkInEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckIn_BookingId() {
		return (EAttribute)checkInEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckIn_RoomIds() {
		return (EAttribute)checkInEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckOut() {
		return checkOutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOut_Date() {
		return (EAttribute)checkOutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOut_BookingId() {
		return (EAttribute)checkOutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOut_RoomId() {
		return (EAttribute)checkOutEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomIdToBookingIdEntry() {
		return roomIdToBookingIdEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomIdToBookingIdEntry_RoomId() {
		return (EAttribute)roomIdToBookingIdEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomIdToBookingIdEntry_BookingId() {
		return (EAttribute)roomIdToBookingIdEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckIOHandler() {
		return checkIOHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOHandler_CheckIOCatalogue() {
		return (EReference)checkIOHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOHandler_RoomHandler() {
		return (EReference)checkIOHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOHandler_BookingHandler() {
		return (EReference)checkIOHandlerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckIOHandler_PriceForInitiatedCheckout() {
		return (EAttribute)checkIOHandlerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOHandler_BookingInitCheckout() {
		return (EReference)checkIOHandlerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckIOHandler_PriceForInitiatedRoomCheckout() {
		return (EAttribute)checkIOHandlerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCheckIOHandler_RoomInitCheckout() {
		return (EReference)checkIOHandlerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__InitiateCheckIn__int() {
		return checkIOHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__CheckInRoomById__int_int() {
		return checkIOHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__CheckInRoomByType__int_String() {
		return checkIOHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__PayRoomDuringCheckout__int_String_String_int_int_String_String() {
		return checkIOHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__PayDuringCheckout__String_String_int_int_String_String() {
		return checkIOHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__InitiateCheckout__int() {
		return checkIOHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__InitiateRoomCheckout__int_int() {
		return checkIOHandlerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ClearAllCheckIO() {
		return checkIOHandlerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ListOccupiedRooms__Date() {
		return checkIOHandlerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__CheckIOCatalogue() {
		return checkIOHandlerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ListCheckIns__Date() {
		return checkIOHandlerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ListCheckIns__Date_Date() {
		return checkIOHandlerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ListCheckOuts__Date() {
		return checkIOHandlerEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__ListCheckOuts__Date_Date() {
		return checkIOHandlerEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__CheckInReg__EList_int() {
		return checkIOHandlerEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__CheckOutReg__int_int() {
		return checkIOHandlerEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCheckIOHandler__AreOnSameDay__Date_Date() {
		return checkIOHandlerEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOFactory getCheckIOFactory() {
		return (CheckIOFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		checkIOCatalogueEClass = createEClass(CHECK_IO_CATALOGUE);
		createEReference(checkIOCatalogueEClass, CHECK_IO_CATALOGUE__CHECK_INS);
		createEReference(checkIOCatalogueEClass, CHECK_IO_CATALOGUE__CHECK_OUTS);
		createEOperation(checkIOCatalogueEClass, CHECK_IO_CATALOGUE___CLEAR_ALL_CHECK_IO);
		createEOperation(checkIOCatalogueEClass, CHECK_IO_CATALOGUE___REGISTER_CHECK_IN__CHECKIN);
		createEOperation(checkIOCatalogueEClass, CHECK_IO_CATALOGUE___REGISTER_CHECK_OUT__CHECKOUT);

		checkInEClass = createEClass(CHECK_IN);
		createEAttribute(checkInEClass, CHECK_IN__DATE);
		createEAttribute(checkInEClass, CHECK_IN__BOOKING_ID);
		createEAttribute(checkInEClass, CHECK_IN__ROOM_IDS);

		checkOutEClass = createEClass(CHECK_OUT);
		createEAttribute(checkOutEClass, CHECK_OUT__DATE);
		createEAttribute(checkOutEClass, CHECK_OUT__BOOKING_ID);
		createEAttribute(checkOutEClass, CHECK_OUT__ROOM_ID);

		roomIdToBookingIdEntryEClass = createEClass(ROOM_ID_TO_BOOKING_ID_ENTRY);
		createEAttribute(roomIdToBookingIdEntryEClass, ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID);
		createEAttribute(roomIdToBookingIdEntryEClass, ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID);

		checkIOHandlerEClass = createEClass(CHECK_IO_HANDLER);
		createEReference(checkIOHandlerEClass, CHECK_IO_HANDLER__CHECK_IO_CATALOGUE);
		createEReference(checkIOHandlerEClass, CHECK_IO_HANDLER__ROOM_HANDLER);
		createEReference(checkIOHandlerEClass, CHECK_IO_HANDLER__BOOKING_HANDLER);
		createEAttribute(checkIOHandlerEClass, CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT);
		createEReference(checkIOHandlerEClass, CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT);
		createEAttribute(checkIOHandlerEClass, CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT);
		createEReference(checkIOHandlerEClass, CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___INITIATE_CHECK_IN__INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_ID__INT_INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_TYPE__INT_STRING);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___INITIATE_CHECKOUT__INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CLEAR_ALL_CHECK_IO);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___LIST_OCCUPIED_ROOMS__DATE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CHECK_IO_CATALOGUE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___LIST_CHECK_INS__DATE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___LIST_CHECK_INS__DATE_DATE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE_DATE);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CHECK_IN_REG__ELIST_INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___CHECK_OUT_REG__INT_INT);
		createEOperation(checkIOHandlerEClass, CHECK_IO_HANDLER___ARE_ON_SAME_DAY__DATE_DATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(checkIOCatalogueEClass, CheckIOCatalogue.class, "CheckIOCatalogue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCheckIOCatalogue_CheckIns(), this.getCheckIn(), null, "checkIns", null, 0, -1, CheckIOCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCheckIOCatalogue_CheckOuts(), this.getCheckOut(), null, "checkOuts", null, 0, -1, CheckIOCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getCheckIOCatalogue__ClearAllCheckIO(), null, "clearAllCheckIO", 1, 1, IS_UNIQUE, !IS_ORDERED);

		EOperation op = initEOperation(getCheckIOCatalogue__RegisterCheckIn__CheckIn(), null, "registerCheckIn", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getCheckIn(), "checkIn", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOCatalogue__RegisterCheckOut__CheckOut(), null, "registerCheckOut", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getCheckOut(), "checkOut", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(checkInEClass, CheckIn.class, "CheckIn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCheckIn_Date(), ecorePackage.getEDate(), "date", null, 1, 1, CheckIn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckIn_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CheckIn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckIn_RoomIds(), ecorePackage.getEInt(), "roomIds", null, 0, -1, CheckIn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(checkOutEClass, CheckOut.class, "CheckOut", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCheckOut_Date(), ecorePackage.getEDate(), "date", null, 1, 1, CheckOut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckOut_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CheckOut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckOut_RoomId(), ecorePackage.getEInt(), "roomId", null, 1, 1, CheckOut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomIdToBookingIdEntryEClass, RoomIdToBookingIdEntry.class, "RoomIdToBookingIdEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomIdToBookingIdEntry_RoomId(), ecorePackage.getEInt(), "roomId", null, 1, 1, RoomIdToBookingIdEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomIdToBookingIdEntry_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, RoomIdToBookingIdEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(checkIOHandlerEClass, CheckIOHandler.class, "CheckIOHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCheckIOHandler_CheckIOCatalogue(), this.getCheckIOCatalogue(), null, "checkIOCatalogue", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCheckIOHandler_RoomHandler(), theRoomPackage.getRoomHandler(), null, "roomHandler", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCheckIOHandler_BookingHandler(), theBookingPackage.getBookingHandler(), null, "bookingHandler", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckIOHandler_PriceForInitiatedCheckout(), ecorePackage.getEDouble(), "priceForInitiatedCheckout", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCheckIOHandler_BookingInitCheckout(), theBookingPackage.getBooking(), null, "bookingInitCheckout", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckIOHandler_PriceForInitiatedRoomCheckout(), ecorePackage.getEDouble(), "priceForInitiatedRoomCheckout", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCheckIOHandler_RoomInitCheckout(), theRoomPackage.getRoom(), null, "roomInitCheckout", null, 1, 1, CheckIOHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__InitiateCheckIn__int(), theRoomPackage.getRoom(), "initiateCheckIn", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__CheckInRoomById__int_int(), ecorePackage.getEBoolean(), "checkInRoomById", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__CheckInRoomByType__int_String(), ecorePackage.getEInt(), "checkInRoomByType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__PayRoomDuringCheckout__int_String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payRoomDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "_expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__PayDuringCheckout__String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "_expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__InitiateCheckout__int(), ecorePackage.getEDouble(), "initiateCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__InitiateRoomCheckout__int_int(), ecorePackage.getEDouble(), "initiateRoomCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getCheckIOHandler__ClearAllCheckIO(), null, "clearAllCheckIO", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__ListOccupiedRooms__Date(), this.getRoomIdToBookingIdEntry(), "listOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getCheckIOHandler__CheckIOCatalogue(), this.getCheckIOCatalogue(), "checkIOCatalogue", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__ListCheckIns__Date(), this.getCheckIn(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__ListCheckIns__Date_Date(), this.getCheckIn(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__ListCheckOuts__Date(), this.getCheckOut(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__ListCheckOuts__Date_Date(), this.getCheckOut(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__CheckInReg__EList_int(), null, "checkInReg", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomIds", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__CheckOutReg__int_int(), null, "checkOutReg", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getCheckIOHandler__AreOnSameDay__Date_Date(), ecorePackage.getEBoolean(), "areOnSameDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "d1", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "d2", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";	
		addAnnotation
		  ((getCheckIOHandler__PayRoomDuringCheckout__int_String_String_int_int_String_String()).getEParameters().get(3), 
		   source, 
		   new String[] {
			 "originalName", " expiryMonth"
		   });	
		addAnnotation
		  ((getCheckIOHandler__PayDuringCheckout__String_String_int_int_String_String()).getEParameters().get(2), 
		   source, 
		   new String[] {
			 "originalName", " expiryMonth"
		   });
	}

} //CheckIOPackageImpl
