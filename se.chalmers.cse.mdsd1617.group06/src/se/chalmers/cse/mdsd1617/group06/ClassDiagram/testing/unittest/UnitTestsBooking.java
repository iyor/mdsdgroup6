package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.unittest;

import static org.junit.Assert.*;

import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

public class UnitTestsBooking {

	BookingHandler bh;
	RoomHandler rh;
	IHotelStartupProvides sp;
	
	
	@Before
	public void init() {
		this.bh = BookingFactory.eINSTANCE.createBookingHandler();
		this.rh = RoomFactory.eINSTANCE.createRoomHandler();
		sp = IOFactory.eINSTANCE.createHotelStartupProvides();
	}

	@Test
	public void testBookingCatalogueSingleton() {
		BookingHandler bh2 = BookingFactory.eINSTANCE.createBookingHandler();
		// Make sure they are not the same object, by some ECore Voodoo
		assertNotEquals(bh, bh2);
		// ... but make sure they have the same catalogue
		assertEquals(bh.bookingcatalogue(), bh2.bookingcatalogue());
	}
	
	/**
	 * Check that successive bookingID:s from the catalogue are increasing.
	 */
	@Test
	public void testGetNewBookingId() {
		assertTrue(bh.bookingcatalogue().getNewBookingId() < bh.bookingcatalogue().getNewBookingId());
		int id1 = bh.bookingcatalogue().getNewBookingId();
		bh = BookingFactory.eINSTANCE.createBookingHandler();
		int id2 = bh.bookingcatalogue().getNewBookingId();
		assertTrue(id1 < id2);
	}
	
	/**
	 * Ensure adding bookings to catalogue works
	 */
	@Test
	public void testInitiateBooking() {
		String fn = "Rikard";
		String en = "Hjort";
		Date sd = new Date(70, 1, 1); // Date constructor is weird
		Date ed = new Date(116, 1, 1);
		int id = bh.initiateBooking(fn, en, sd, ed);
		Booking b = bh.bookingcatalogue().getBooking(id);
		assertEquals(id, b.getBookingId());
		assertEquals(fn, b.getFirstName());
		assertEquals(en, b.getLastName());
		assertEquals(sd, b.getStartDate());
		assertEquals(ed, b.getEndDate());
	}
	
	/**
	 * Test that confirming booking works, and fails when it should.
	 */
	@Test
	public void testConfirmBooking() {
		this.sp.startup(10);
		String fn = "Rikard";
		String en = "Hjort";
		Date sd = new Date(70, 1, 1); // Date constructor is weird.
		Date ed = new Date(116, 1, 1);
		int id = bh.initiateBooking(fn, en, sd, ed);
		// No rooms added, should fail.
		assertFalse(bh.confirmBooking(id));
		// Keep trying ...
		assertFalse(bh.confirmBooking(id));
		assertFalse(bh.confirmBooking(id));
		
		// Curveball: add room to some other booking!
		int id2 = bh.initiateBooking(fn, en, sd, ed);
		assertNotEquals(id, id2);
		assertTrue(bh.addRoomToBooking("default", id2));
		// Can't confirm the first booking.
		assertFalse(bh.confirmBooking(id));
		// But second one should work
		assertTrue(bh.confirmBooking(id2));
		
		//Good. Now add a room.
		assertTrue(bh.addRoomToBooking("default", id));
		assertTrue(bh.confirmBooking(id));
		// Should only be able to confirm once.
		assertFalse(bh.confirmBooking(id));
		assertFalse(bh.confirmBooking(id));
		
	}
	
	/**
	 * 
	 */
	@Test
	public void testSimpleGetFreeRoomsOfTypeAndDate() {
		sp.startup(10);
		int nbrFree = bh.getFreeRoomTypesForPeriod("default", new Date(0), new Date(2));
		assertEquals(nbrFree, 10);
	}
	
	@Test
	public void testGetBookingsForPeriod(){
		//Reset system
		RoomHandler rh = RoomFactory.eINSTANCE.createRoomHandler();
		rh.clearAllRooms();
		bh.clearAllBookings();
		
		//Create rooms
		rh.roomcatalogue().addRoomType(1, "singleBed", 42);
		for(int i = 0; i < 4; i++){
			rh.roomcatalogue().addRoom(i+1, "singleBed");
		}
		
		//0 Create bookings A jan-feb
		Date sdA = new Date(2016, 0, 1);
		Date edA = new Date(2016, 1, 1);
		bh.initiateBooking("Anna", "A", sdA, edA);
		bh.addRoomToBooking("singleBed", 1);
		bh.confirmBooking(1);
	
		//1 Create bookings B mid jan- mid feb
		Date sdB = new Date(2016, 0, 15);
		Date edB = new Date(2016, 1, 15);
		bh.initiateBooking("Bertil", "B", sdB, edB);
		bh.addRoomToBooking("singleBed", 2);
		bh.confirmBooking(2);

		//2 Create bookings C feb-march
		Date sdC = new Date(2016, 1, 1);
		Date edC = new Date(2016, 2, 1);
		bh.initiateBooking("Cecilia", "C", sdC, edC);
		bh.addRoomToBooking("singleBed", 3);
		bh.confirmBooking(3);

		//3 Create bookings D 2march-2april
		Date sdD = new Date(2016, 2, 2);
		Date edD = new Date(2016, 3, 1);
		bh.initiateBooking("David", "D", sdD, edD);
		bh.addRoomToBooking("singleBed", 4);
		bh.confirmBooking(4);
				
		//No overlaps
		Date sdO = new Date(2017, 0, 1);
		Date edO = new Date(2017, 0, 1);
		EList<Booking> no = new BasicEList<>();
		EList<Booking> noOverLapsBookings = bh.bookingcatalogue().getBookingsForPeriod(sdO, edO);
		assertEquals(0, noOverLapsBookings.size());
		assertEquals(no, noOverLapsBookings);

		//Overlaps with one Booking that has exactly the same period
		Date sdOne = new Date(2016, 2, 2);
		Date edOne = new Date(2016, 3, 1);
		EList<Booking> b = new BasicEList<>();
		b.add(bh.bookingcatalogue().getBooking(4));
		EList<Booking> one = bh.bookingcatalogue().getBookingsForPeriod(sdOne, edOne);
		assertEquals(1, one.size());
		assertEquals(b, one);
		
		//Overlaps with three bookings
		Date sdThree = new Date(2016, 0, 20);
		Date edThree = new Date(2016, 1, 10);
		EList<Booking> c = new BasicEList<>();
		c.add(bh.bookingcatalogue().getBooking(1));
		c.add(bh.bookingcatalogue().getBooking(2));
		c.add(bh.bookingcatalogue().getBooking(3));
		EList<Booking> three = bh.bookingcatalogue().getBookingsForPeriod(sdThree, edThree);
		assertEquals(3, three.size());
		assertEquals(c, three);

	}
}
