/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomPackageImpl extends EPackageImpl implements RoomPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomCatalogueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extrasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roomStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomPackageImpl() {
		super(eNS_URI, RoomFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomPackage init() {
		if (isInited) return (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Obtain or create and register package
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		IOPackageImpl theIOPackage = (IOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) instanceof IOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) : IOPackage.eINSTANCE);
		CheckIOPackageImpl theCheckIOPackage = (CheckIOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) instanceof CheckIOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) : CheckIOPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomPackage.createPackageContents();
		theIOPackage.createPackageContents();
		theCheckIOPackage.createPackageContents();
		theBookingPackage.createPackageContents();

		// Initialize created meta-data
		theRoomPackage.initializePackageContents();
		theIOPackage.initializePackageContents();
		theCheckIOPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomPackage.eNS_URI, theRoomPackage);
		return theRoomPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomCatalogue() {
		return roomCatalogueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomCatalogue_Rooms() {
		return (EReference)roomCatalogueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomCatalogue_RoomTypeList() {
		return (EReference)roomCatalogueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__RemoveRoomType__String() {
		return roomCatalogueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__UpdateRoomType__String_int_String_double() {
		return roomCatalogueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__AddRoomType__int_String_double() {
		return roomCatalogueEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__AddRoom__int_String() {
		return roomCatalogueEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__ClearAllRooms() {
		return roomCatalogueEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__AddExtra__int_String_float() {
		return roomCatalogueEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__GetAllRoomTypesWithMinNumberOfBeds__int() {
		return roomCatalogueEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__GetRoomById__int() {
		return roomCatalogueEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__GetRoomsOfType__String() {
		return roomCatalogueEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__RemoveRoom__Room() {
		return roomCatalogueEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__GetRoomType__String() {
		return roomCatalogueEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__ClearAllRoomTypes() {
		return roomCatalogueEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomCatalogue__ChangeRoomTypeRoom__String_int() {
		return roomCatalogueEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_NoOfBeds() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_RoomTypeDescription() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_PricePerNight() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomType__UpdateRoomType__int_String_double() {
		return roomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomType__Equals__Object() {
		return roomTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_RoomType() {
		return (EReference)roomEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_Extras() {
		return (EReference)roomEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoom__IsBlocked() {
		return roomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoom__SetBlocked__boolean() {
		return roomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoom__AddExtra__String_float() {
		return roomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Id() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_RoomStatus() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtras() {
		return extrasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtras_Description() {
		return (EAttribute)extrasEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtras_Price() {
		return (EAttribute)extrasEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomHandler() {
		return roomHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomHandler_Roomcatalogue() {
		return (EReference)roomHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__AddRoomType__int_String_double() {
		return roomHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__UpdateRoomType__String_int_String_double() {
		return roomHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__RemoveRoomType__String() {
		return roomHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__GetNbrOfRooms__String() {
		return roomHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__ClearAllRooms() {
		return roomHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__Startup__int() {
		return roomHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__Roomcatalogue() {
		return roomHandlerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__AddExtra__int_String_float() {
		return roomHandlerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__GetAllRoomTypesWithMinNumberOfBeds__int() {
		return roomHandlerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__BlockRoom__int() {
		return roomHandlerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__UnblockRoom__int() {
		return roomHandlerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__AddRoom__int_String() {
		return roomHandlerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__GetLstRoomType() {
		return roomHandlerEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__RemoveRoom__int() {
		return roomHandlerEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__ChangeRoomTypeRoom__int_String() {
		return roomHandlerEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__GetRoomById__int() {
		return roomHandlerEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomHandler__GetRoomsOfType__String() {
		return roomHandlerEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoomStatus() {
		return roomStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomFactory getRoomFactory() {
		return (RoomFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomCatalogueEClass = createEClass(ROOM_CATALOGUE);
		createEReference(roomCatalogueEClass, ROOM_CATALOGUE__ROOMS);
		createEReference(roomCatalogueEClass, ROOM_CATALOGUE__ROOM_TYPE_LIST);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___REMOVE_ROOM_TYPE__STRING);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___ADD_ROOM_TYPE__INT_STRING_DOUBLE);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___ADD_ROOM__INT_STRING);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___CLEAR_ALL_ROOMS);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___ADD_EXTRA__INT_STRING_FLOAT);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___GET_ROOM_BY_ID__INT);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___GET_ROOMS_OF_TYPE__STRING);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___REMOVE_ROOM__ROOM);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___GET_ROOM_TYPE__STRING);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___CLEAR_ALL_ROOM_TYPES);
		createEOperation(roomCatalogueEClass, ROOM_CATALOGUE___CHANGE_ROOM_TYPE_ROOM__STRING_INT);

		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__ID);
		createEAttribute(roomEClass, ROOM__ROOM_STATUS);
		createEReference(roomEClass, ROOM__ROOM_TYPE);
		createEReference(roomEClass, ROOM__EXTRAS);
		createEOperation(roomEClass, ROOM___IS_BLOCKED);
		createEOperation(roomEClass, ROOM___SET_BLOCKED__BOOLEAN);
		createEOperation(roomEClass, ROOM___ADD_EXTRA__STRING_FLOAT);

		roomTypeEClass = createEClass(ROOM_TYPE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NO_OF_BEDS);
		createEAttribute(roomTypeEClass, ROOM_TYPE__ROOM_TYPE_DESCRIPTION);
		createEAttribute(roomTypeEClass, ROOM_TYPE__PRICE_PER_NIGHT);
		createEOperation(roomTypeEClass, ROOM_TYPE___UPDATE_ROOM_TYPE__INT_STRING_DOUBLE);
		createEOperation(roomTypeEClass, ROOM_TYPE___EQUALS__OBJECT);

		extrasEClass = createEClass(EXTRAS);
		createEAttribute(extrasEClass, EXTRAS__DESCRIPTION);
		createEAttribute(extrasEClass, EXTRAS__PRICE);

		roomHandlerEClass = createEClass(ROOM_HANDLER);
		createEReference(roomHandlerEClass, ROOM_HANDLER__ROOMCATALOGUE);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___ADD_ROOM_TYPE__INT_STRING_DOUBLE);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___REMOVE_ROOM_TYPE__STRING);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___GET_NBR_OF_ROOMS__STRING);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___CLEAR_ALL_ROOMS);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___STARTUP__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___ROOMCATALOGUE);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___ADD_EXTRA__INT_STRING_FLOAT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___BLOCK_ROOM__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___UNBLOCK_ROOM__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___ADD_ROOM__INT_STRING);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___GET_LST_ROOM_TYPE);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___REMOVE_ROOM__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___CHANGE_ROOM_TYPE_ROOM__INT_STRING);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___GET_ROOM_BY_ID__INT);
		createEOperation(roomHandlerEClass, ROOM_HANDLER___GET_ROOMS_OF_TYPE__STRING);

		// Create enums
		roomStatusEEnum = createEEnum(ROOM_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(roomCatalogueEClass, RoomCatalogue.class, "RoomCatalogue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomCatalogue_Rooms(), this.getRoom(), null, "rooms", null, 0, -1, RoomCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomCatalogue_RoomTypeList(), this.getRoomType(), null, "roomTypeList", null, 0, -1, RoomCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = initEOperation(getRoomCatalogue__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__UpdateRoomType__String_int_String_double(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "oldDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newNoOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "newPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__AddRoomType__int_String_double(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "noOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "pricePerNight", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__AddRoom__int_String(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getRoomCatalogue__ClearAllRooms(), null, "clearAllRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__AddExtra__int_String_float(), null, "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__GetAllRoomTypesWithMinNumberOfBeds__int(), this.getRoomType(), "getAllRoomTypesWithMinNumberOfBeds", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "minNumBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__GetRoomById__int(), this.getRoom(), "getRoomById", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__GetRoomsOfType__String(), this.getRoom(), "getRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__RemoveRoom__Room(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoom(), "room", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__GetRoomType__String(), this.getRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getRoomCatalogue__ClearAllRoomTypes(), null, "clearAllRoomTypes", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomCatalogue__ChangeRoomTypeRoom__String_int(), ecorePackage.getEBoolean(), "changeRoomTypeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_RoomStatus(), this.getRoomStatus(), "roomStatus", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoom_RoomType(), this.getRoomType(), null, "roomType", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoom_Extras(), this.getExtras(), null, "extras", null, 0, -1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getRoom__IsBlocked(), ecorePackage.getEBoolean(), "isBlocked", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoom__SetBlocked__boolean(), null, "setBlocked", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "block", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoom__AddExtra__String_float(), null, "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomType_NoOfBeds(), ecorePackage.getEInt(), "noOfBeds", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_RoomTypeDescription(), ecorePackage.getEString(), "roomTypeDescription", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_PricePerNight(), ecorePackage.getEDouble(), "pricePerNight", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getRoomType__UpdateRoomType__int_String_double(), null, "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "noOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "pricePerNight", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomType__Equals__Object(), ecorePackage.getEBoolean(), "equals", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "obj", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(extrasEClass, Extras.class, "Extras", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtras_Description(), ecorePackage.getEString(), "description", null, 1, 1, Extras.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getExtras_Price(), ecorePackage.getEFloat(), "price", null, 1, 1, Extras.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomHandlerEClass, RoomHandler.class, "RoomHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomHandler_Roomcatalogue(), this.getRoomCatalogue(), null, "roomcatalogue", null, 1, 1, RoomHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getRoomHandler__AddRoomType__int_String_double(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "noOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "pricePerNight", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__UpdateRoomType__String_int_String_double(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "oldDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newNoOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "newPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__GetNbrOfRooms__String(), ecorePackage.getEInt(), "getNbrOfRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getRoomHandler__ClearAllRooms(), null, "clearAllRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__Startup__int(), null, "startup", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getRoomHandler__Roomcatalogue(), this.getRoomCatalogue(), "roomcatalogue", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__AddExtra__int_String_float(), null, "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__GetAllRoomTypesWithMinNumberOfBeds__int(), this.getRoomType(), "getAllRoomTypesWithMinNumberOfBeds", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "minNumBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__BlockRoom__int(), null, "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__UnblockRoom__int(), null, "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__AddRoom__int_String(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getRoomHandler__GetLstRoomType(), this.getRoomType(), "getLstRoomType", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__ChangeRoomTypeRoom__int_String(), ecorePackage.getEBoolean(), "changeRoomTypeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__GetRoomById__int(), this.getRoom(), "getRoomById", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomHandler__GetRoomsOfType__String(), this.getRoom(), "getRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(roomStatusEEnum, RoomStatus.class, "RoomStatus");
		addEEnumLiteral(roomStatusEEnum, RoomStatus.FREE);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.OCCUPIED);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.BOOKED);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.BLOCKED);

		// Create resource
		createResource(eNS_URI);
	}

} //RoomPackageImpl
