/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl#getCheckiohandler <em>Checkiohandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl#getBookinghandler <em>Bookinghandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements HotelCustomerProvides {
	/**
	 * The cached value of the '{@link #getRoomhandler() <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomhandler()
	 * @generated
	 * @ordered
	 */
	protected RoomHandler roomhandler;

	/**
	 * The cached value of the '{@link #getCheckiohandler() <em>Checkiohandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckiohandler()
	 * @generated
	 * @ordered
	 */
	protected CheckIOHandler checkiohandler;

	/**
	 * The cached value of the '{@link #getBookinghandler() <em>Bookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookinghandler()
	 * @generated
	 * @ordered
	 */
	protected BookingHandler bookinghandler;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelCustomerProvidesImpl() {
		super();
		this.bookinghandler = BookingFactory.eINSTANCE.createBookingHandler();
		this.checkiohandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		this.roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IOPackage.Literals.HOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomHandler getRoomhandler() {
		if (roomhandler != null && roomhandler.eIsProxy()) {
			InternalEObject oldRoomhandler = (InternalEObject)roomhandler;
			roomhandler = (RoomHandler)eResolveProxy(oldRoomhandler);
			if (roomhandler != oldRoomhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
			}
		}
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler basicGetRoomhandler() {
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomhandler(RoomHandler newRoomhandler) {
		RoomHandler oldRoomhandler = roomhandler;
		roomhandler = newRoomhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler getCheckiohandler() {
		if (checkiohandler != null && checkiohandler.eIsProxy()) {
			InternalEObject oldCheckiohandler = (InternalEObject)checkiohandler;
			checkiohandler = (CheckIOHandler)eResolveProxy(oldCheckiohandler);
			if (checkiohandler != oldCheckiohandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
			}
		}
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler basicGetCheckiohandler() {
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckiohandler(CheckIOHandler newCheckiohandler) {
		CheckIOHandler oldCheckiohandler = checkiohandler;
		checkiohandler = newCheckiohandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler getBookinghandler() {
		if (bookinghandler != null && bookinghandler.eIsProxy()) {
			InternalEObject oldBookinghandler = (InternalEObject)bookinghandler;
			bookinghandler = (BookingHandler)eResolveProxy(oldBookinghandler);
			if (bookinghandler != oldBookinghandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
			}
		}
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler basicGetBookinghandler() {
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookinghandler(BookingHandler newBookinghandler) {
		BookingHandler oldBookinghandler = bookinghandler;
		bookinghandler = newBookinghandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		Date sD = convertDate(startDate);
		Date eD = convertDate(endDate);
		if (eD.before(sD))
			return new BasicEList<FreeRoomTypesDTO>();
		return this.bookinghandler.getFreeRooms(numBeds, sD, eD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * UC 2.1.1. (CH)
	 * This method checks the input for error, converts the date strings to date objects and forwards call to responsible bookinghandler
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
		public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
			if (firstName == null || lastName == null || startDate == null || endDate == null) 
				return -1;
			
			Date sD = convertDate(startDate);
			Date eD = convertDate(endDate);
			
			if (eD.before(sD))
				return -1;
			
			return bookinghandler.initiateBooking(firstName,lastName, sD, eD);
		}

		/**
		 * <!-- begin-user-doc -->
		 * UC 2.1.1. (CH)
		 * This method converts the date from string format to date for internal use in bookinghandler.
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public Date convertDate(String date) {
			int year = Integer.parseInt(date.substring(0, 4));
			int month = Integer.parseInt(date.substring(4, 6));
			int day = Integer.parseInt(date.substring(6, 8));
			return new Date(year - 1900,month-1,day);
		}

	/**
	 * <!-- begin-user-doc -->
	 * UC 2.1.1. (CH)
	 * This method forwards the call to the responsible bookinghandler.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return bookinghandler.addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * UC 2.1.1. (CH)
	 * This method forwards the call to the responsible bookinghandler.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return bookinghandler.confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		return checkiohandler.initiateCheckout(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return checkiohandler.payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return checkiohandler.checkInRoomByType(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		return checkiohandler.initiateRoomCheckout(roomNumber, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return checkiohandler.payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER:
				if (resolve) return getRoomhandler();
				return basicGetRoomhandler();
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER:
				if (resolve) return getCheckiohandler();
				return basicGetCheckiohandler();
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER:
				if (resolve) return getBookinghandler();
				return basicGetBookinghandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)newValue);
				return;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)newValue);
				return;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)null);
				return;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)null);
				return;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER:
				return roomhandler != null;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER:
				return checkiohandler != null;
			case IOPackage.HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER:
				return bookinghandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case IOPackage.HOTEL_CUSTOMER_PROVIDES___CONVERT_DATE__STRING:
				return convertDate((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelCustomerProvidesImpl
