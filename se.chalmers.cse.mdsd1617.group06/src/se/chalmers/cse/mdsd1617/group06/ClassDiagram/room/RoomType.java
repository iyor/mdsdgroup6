/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getNoOfBeds <em>No Of Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getRoomTypeDescription <em>Room Type Description</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getPricePerNight <em>Price Per Night</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomType()
 * @model
 * @generated
 */
public interface RoomType extends EObject {
	/**
	 * Returns the value of the '<em><b>No Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Of Beds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Of Beds</em>' attribute.
	 * @see #setNoOfBeds(int)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomType_NoOfBeds()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getNoOfBeds();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getNoOfBeds <em>No Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Of Beds</em>' attribute.
	 * @see #getNoOfBeds()
	 * @generated
	 */
	void setNoOfBeds(int value);

	/**
	 * Returns the value of the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Description</em>' attribute.
	 * @see #setRoomTypeDescription(String)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomType_RoomTypeDescription()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getRoomTypeDescription();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getRoomTypeDescription <em>Room Type Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Description</em>' attribute.
	 * @see #getRoomTypeDescription()
	 * @generated
	 */
	void setRoomTypeDescription(String value);

	/**
	 * Returns the value of the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price Per Night</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price Per Night</em>' attribute.
	 * @see #setPricePerNight(double)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomType_PricePerNight()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPricePerNight();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getPricePerNight <em>Price Per Night</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price Per Night</em>' attribute.
	 * @see #getPricePerNight()
	 * @generated
	 */
	void setPricePerNight(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model noOfBedsRequired="true" noOfBedsOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" pricePerNightRequired="true" pricePerNightOrdered="false"
	 * @generated
	 */
	void updateRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" objRequired="true" objOrdered="false"
	 * @generated
	 */
	boolean equals(Object obj);

} // RoomType
