/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl#getCheckIns <em>Check Ins</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl#getCheckOuts <em>Check Outs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CheckIOCatalogueImpl extends MinimalEObjectImpl.Container implements CheckIOCatalogue {
	/**
	 * The cached value of the '{@link #getCheckIns() <em>Check Ins</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckIns()
	 * @generated
	 * @ordered
	 */
	protected EList<CheckIn> checkIns;

	/**
	 * The cached value of the '{@link #getCheckOuts() <em>Check Outs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckOuts()
	 * @generated
	 * @ordered
	 */
	protected EList<CheckOut> checkOuts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected CheckIOCatalogueImpl() {
		super();
		checkIns = new BasicEList<CheckIn>();
		checkOuts = new BasicEList<CheckOut>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CheckIOPackage.Literals.CHECK_IO_CATALOGUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CheckIn> getCheckIns() {
		if (checkIns == null) {
			checkIns = new EObjectResolvingEList<CheckIn>(CheckIn.class, this, CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_INS);
		}
		return checkIns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CheckOut> getCheckOuts() {
		if (checkOuts == null) {
			checkOuts = new EObjectResolvingEList<CheckOut>(CheckOut.class, this, CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_OUTS);
		}
		return checkOuts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllCheckIO() {
		this.getCheckIns().clear();
		this.getCheckOuts().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void registerCheckIn(CheckIn checkIn) {
		checkIns.add(checkIn);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void registerCheckOut(CheckOut checkOut) {
		checkOuts.add(checkOut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_INS:
				return getCheckIns();
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_OUTS:
				return getCheckOuts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_INS:
				getCheckIns().clear();
				getCheckIns().addAll((Collection<? extends CheckIn>)newValue);
				return;
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_OUTS:
				getCheckOuts().clear();
				getCheckOuts().addAll((Collection<? extends CheckOut>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_INS:
				getCheckIns().clear();
				return;
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_OUTS:
				getCheckOuts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_INS:
				return checkIns != null && !checkIns.isEmpty();
			case CheckIOPackage.CHECK_IO_CATALOGUE__CHECK_OUTS:
				return checkOuts != null && !checkOuts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CheckIOPackage.CHECK_IO_CATALOGUE___CLEAR_ALL_CHECK_IO:
				clearAllCheckIO();
				return null;
			case CheckIOPackage.CHECK_IO_CATALOGUE___REGISTER_CHECK_IN__CHECKIN:
				registerCheckIn((CheckIn)arguments.get(0));
				return null;
			case CheckIOPackage.CHECK_IO_CATALOGUE___REGISTER_CHECK_OUT__CHECKOUT:
				registerCheckOut((CheckOut)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //CheckIOCatalogueImpl
