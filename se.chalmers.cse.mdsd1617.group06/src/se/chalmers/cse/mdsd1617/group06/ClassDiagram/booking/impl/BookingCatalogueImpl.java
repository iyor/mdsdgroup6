/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl#getBookings <em>Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl#getCurrentBookingId <em>Current Booking Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingCatalogueImpl extends MinimalEObjectImpl.Container implements BookingCatalogue {
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> bookings;

	/**
	 * The default value of the '{@link #getCurrentBookingID() <em>Current Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentBookingID()
	 * @generated NOT
	 * @ordered
	 */
	protected static final int CURRENT_BOOKING_ID_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getCurrentBookingId() <em>Current Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentBookingId()
	 * @generated
	 * @ordered
	 */
	protected int currentBookingId = CURRENT_BOOKING_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingCatalogueImpl() {
		super();
		
		this.bookings = new BasicEList<Booking>();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_CATALOGUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return a shallow copy of the list of all confirmed bookings.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackage.BOOKING_CATALOGUE__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCurrentBookingId() {
		return currentBookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentBookingId(int newCurrentBookingId) {
		int oldCurrentBookingId = currentBookingId;
		currentBookingId = newCurrentBookingId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_CATALOGUE__CURRENT_BOOKING_ID, oldCurrentBookingId, currentBookingId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method return the booking object from the bookingcatalogue or null if bookingID does not exist. (CH)
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBooking(int bookingID) {
		for (int i = 0; i < bookings.size(); i++)
		{
			if (bookings.get(i).getBookingId() == bookingID)
				return bookings.get(i);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method adds a booking to the bookingcatalogue.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addBooking(Booking Booking) {
		bookings.add(Booking);
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method returns a not associated bookingID and increases the current bookingID. (CH)
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNewBookingId() {
		return currentBookingId++;
	}

	/**
	 * <!-- begin-user-doc -->
	 * This method deletes all booking from the bookingcatalogue and resets the bookingID counter to 1. (CH)
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllBookings() {
		this.bookings.clear();
		this.currentBookingId = 1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public EList<Booking> getBookingsForPeriod(Date startDate, Date endDate) {
		EList<Booking> bookingsForPeriod = new BasicEList<Booking>();
		for(Booking b : this.bookings){
			Date bStartDate = b.getStartDate();
			Date bEndDate = b.getEndDate();
			if(bStartDate.compareTo(startDate) >= 0 && bStartDate.compareTo(endDate) <=0 
					|| bEndDate.compareTo(startDate) >= 0 && bEndDate.compareTo(endDate) <=0
					|| bStartDate.compareTo(startDate) <0 && bEndDate.compareTo(endDate)>0) {
				bookingsForPeriod.add(b);
			}
		}
		return bookingsForPeriod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_CATALOGUE__BOOKINGS:
				return getBookings();
			case BookingPackage.BOOKING_CATALOGUE__CURRENT_BOOKING_ID:
				return getCurrentBookingId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_CATALOGUE__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends Booking>)newValue);
				return;
			case BookingPackage.BOOKING_CATALOGUE__CURRENT_BOOKING_ID:
				setCurrentBookingId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_CATALOGUE__BOOKINGS:
				getBookings().clear();
				return;
			case BookingPackage.BOOKING_CATALOGUE__CURRENT_BOOKING_ID:
				setCurrentBookingId(CURRENT_BOOKING_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_CATALOGUE__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
			case BookingPackage.BOOKING_CATALOGUE__CURRENT_BOOKING_ID:
				return currentBookingId != CURRENT_BOOKING_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_CATALOGUE___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CATALOGUE___ADD_BOOKING__BOOKING:
				addBooking((Booking)arguments.get(0));
				return null;
			case BookingPackage.BOOKING_CATALOGUE___GET_NEW_BOOKING_ID:
				return getNewBookingId();
			case BookingPackage.BOOKING_CATALOGUE___CLEAR_ALL_BOOKINGS:
				clearAllBookings();
				return null;
			case BookingPackage.BOOKING_CATALOGUE___GET_BOOKINGS_FOR_PERIOD__DATE_DATE:
				return getBookingsForPeriod((Date)arguments.get(0), (Date)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (currentBookingId: ");
		result.append(currentBookingId);
		result.append(')');
		return result.toString();
	}

} //BookingCatalogueImpl
