/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomStatus <em>Room Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getExtras <em>Extras</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' reference.
	 * @see #setRoomType(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoom_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomType <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(RoomType value);

	/**
	 * Returns the value of the '<em><b>Extras</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extras</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extras</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoom_Extras()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Extras> getExtras();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	boolean isBlocked();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model blockRequired="true" blockOrdered="false"
	 * @generated
	 */
	void setBlocked(boolean block);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model descriptionRequired="true" descriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	void addExtra(String description, float price);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoom_Id()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Room Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus
	 * @see #setRoomStatus(RoomStatus)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoom_RoomStatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomStatus getRoomStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomStatus <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus
	 * @see #getRoomStatus()
	 * @generated
	 */
	void setRoomStatus(RoomStatus value);

} // Room
