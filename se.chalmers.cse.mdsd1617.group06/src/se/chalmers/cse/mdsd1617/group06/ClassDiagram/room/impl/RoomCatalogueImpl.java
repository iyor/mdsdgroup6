/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Catalogue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl#getRoomTypeList <em>Room Type List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomCatalogueImpl extends MinimalEObjectImpl.Container implements RoomCatalogue {

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getRoomTypeList() <em>Room Type List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeList()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypeList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomCatalogueImpl() {
		super();
		this.roomTypeList = new BasicEList<RoomType>();
		this.rooms = new BasicEList<Room>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_CATALOGUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<Room>(Room.class, this, RoomPackage.ROOM_CATALOGUE__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypeList() {
		if (roomTypeList == null) {
			roomTypeList = new EObjectResolvingEList<RoomType>(RoomType.class, this, RoomPackage.ROOM_CATALOGUE__ROOM_TYPE_LIST);
		}
		return roomTypeList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String roomTypeDescription) {
		RoomType rt = getRoomType(roomTypeDescription);
		return roomTypeList.remove(rt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice) {
		RoomType rt = getRoomType(oldDescription);
		if (rt == null) {
			return false;
		}
		rt.setNoOfBeds(newNoOfBeds);
		rt.setRoomTypeDescription(newDescription);
		rt.setPricePerNight(newPrice);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllRoomTypes(){
		roomTypeList.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeRoom(String roomType, int roomId) {
		Room room = this.getRoomById(roomId);
		if( room == null ){
			return false; // The room does not exist
		}else{
			RoomType type = this.getRoomType(roomType);
			if( type == null ){
				return false; // The room type does not exist
			}else{
				room.setRoomType(type);
			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight) {
		RoomType roomType = new RoomTypeImpl(noOfBeds, roomTypeDescription, pricePerNight);
		if(!roomTypeList.contains(roomType))
			return false;
		int roomTypeIndex = roomTypeList.indexOf(roomType);
		roomTypeList.remove(roomTypeIndex);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(int oldNoOfBeds, String oldDescription, double oldPrice, int newNoOfBeds, String newDescription, double newPrice) {
		RoomType oldRoomType = new RoomTypeImpl(oldNoOfBeds, oldDescription, oldPrice);
		if(!roomTypeList.contains(oldRoomType))
			return false;
		int roomTypeIndex = roomTypeList.indexOf(oldRoomType);
		RoomType type = roomTypeList.get(roomTypeIndex);
		type.setNoOfBeds(newNoOfBeds);
		type.setRoomTypeDescription(newDescription);
		type.setPricePerNight(newPrice);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(int noOfBeds, String roomDescription, double pricePerNight) {
		if (noOfBeds < 1
				|| roomDescription == null
				|| pricePerNight < 0) return false;
		
		RoomType newRoomType = new RoomTypeImpl(noOfBeds, roomDescription, pricePerNight);
		if(roomTypeList.contains(newRoomType))
			return false;
		return roomTypeList.add(newRoomType);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomId, String roomType) {
		for(Room r : rooms){
			if(r.getId() == roomId) return false;
		}
		
		Room newRoom = RoomFactory.eINSTANCE.createRoom();
		newRoom.setId(roomId);
		RoomType rt = getRoomType(roomType);
		if (rt == null) return false;
		newRoom.setRoomType(rt);
		
		this.rooms.add(newRoom);
		return true;
	}

	/**
	 * <!-- begin-user-doc --> 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Room getRoomById(int roomId) {
		for(Room r : rooms)
			if(r.getId() == roomId)
				return r;
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a shallow copy of the room list, but with only the rooms of a given type left.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getRoomsOfType(String roomType) {
		EList<Room> tmp = new BasicEList<>();
		for (Room r : rooms) {
			if (r.getRoomType().getRoomTypeDescription().equals(roomType)){
				tmp.add(r);
			}
		}
		return tmp;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(Room room) {
		return this.rooms.remove(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomType getRoomType(String roomType) {
		for (RoomType rt : roomTypeList) {
			if (rt.getRoomTypeDescription().equals(roomType)) {
				return rt;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllRooms() {
		this.rooms.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(int roomNumber, String description, float price) {
		Room r = this.getRoomById(roomNumber);
		if (r != null) r.addExtra(description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public EList<RoomType> getAllRoomTypesWithMinNumberOfBeds(int minNumBeds) {
		EList<RoomType> roomTypesWithMinBedsList = new BasicEList<RoomType>();
		for(RoomType rt : roomTypeList){
			if(rt.getNoOfBeds()>=minNumBeds){
				roomTypesWithMinBedsList.add(rt);
			}
		}
		return roomTypesWithMinBedsList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_CATALOGUE__ROOMS:
				return getRooms();
			case RoomPackage.ROOM_CATALOGUE__ROOM_TYPE_LIST:
				return getRoomTypeList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_CATALOGUE__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case RoomPackage.ROOM_CATALOGUE__ROOM_TYPE_LIST:
				getRoomTypeList().clear();
				getRoomTypeList().addAll((Collection<? extends RoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_CATALOGUE__ROOMS:
				getRooms().clear();
				return;
			case RoomPackage.ROOM_CATALOGUE__ROOM_TYPE_LIST:
				getRoomTypeList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_CATALOGUE__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case RoomPackage.ROOM_CATALOGUE__ROOM_TYPE_LIST:
				return roomTypeList != null && !roomTypeList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_CATALOGUE___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE:
				return updateRoomType((String)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case RoomPackage.ROOM_CATALOGUE___ADD_ROOM_TYPE__INT_STRING_DOUBLE:
				return addRoomType((Integer)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2));
			case RoomPackage.ROOM_CATALOGUE___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackage.ROOM_CATALOGUE___CLEAR_ALL_ROOMS:
				clearAllRooms();
				return null;
			case RoomPackage.ROOM_CATALOGUE___ADD_EXTRA__INT_STRING_FLOAT:
				addExtra((Integer)arguments.get(0), (String)arguments.get(1), (Float)arguments.get(2));
				return null;
			case RoomPackage.ROOM_CATALOGUE___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT:
				return getAllRoomTypesWithMinNumberOfBeds((Integer)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___GET_ROOM_BY_ID__INT:
				return getRoomById((Integer)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___GET_ROOMS_OF_TYPE__STRING:
				return getRoomsOfType((String)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___REMOVE_ROOM__ROOM:
				return removeRoom((Room)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___GET_ROOM_TYPE__STRING:
				return getRoomType((String)arguments.get(0));
			case RoomPackage.ROOM_CATALOGUE___CLEAR_ALL_ROOM_TYPES:
				clearAllRoomTypes();
				return null;
			case RoomPackage.ROOM_CATALOGUE___CHANGE_ROOM_TYPE_ROOM__STRING_INT:
				return changeRoomTypeRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}
} 