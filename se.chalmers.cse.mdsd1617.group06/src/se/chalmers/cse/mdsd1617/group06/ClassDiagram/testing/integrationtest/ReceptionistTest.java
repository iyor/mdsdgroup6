/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.integrationtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus;

/**
 * 
 */
public class ReceptionistTest {
	
	protected BookingHandler bookingHandler;
	protected RoomHandler roomHandler;
	protected CheckIOHandler checkIOHandler;
	protected AdministratorRequires bankingAdmin;
	protected HotelReceptionistProvides receptionist;
	protected HotelAdministratorProvides admin;

	/**
	 * 
	 */
	public ReceptionistTest() {
		super();
		this.roomHandler = RoomFactory.eINSTANCE.createRoomHandler();
		this.admin = IOFactory.eINSTANCE.createHotelAdministratorProvides();
		this.bookingHandler = BookingFactory.eINSTANCE.createBookingHandler();
		this.receptionist = IOFactory.eINSTANCE.createHotelReceptionistProvides();
		this.checkIOHandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		try {
			this.bankingAdmin = AdministratorRequires.instance();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void init() {
		this.roomHandler.clearAllRooms();
		this.roomHandler.roomcatalogue().clearAllRoomTypes();
		this.bookingHandler.clearAllBookings();
		this.checkIOHandler.clearAllCheckIO();
	}
	
	/**
	 * UC 2.1.1. (CH)
	 * This test assures that available rooms can be added to a booking and no rooms will be added if there are no free rooms. (CH)
	 */
	@Test
	public void addRoomsToBookingTest() {
		// Add two room types including rooms to the hotel
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
		this.admin.addRoom(2, "single room");
		this.admin.addRoom(3, "single room");
		this.admin.addRoomType(2, "double room", 500.0);
		this.admin.addRoom(4, "double room");
		this.admin.addRoom(5, "double room");
		this.admin.addRoom(6, "double room");
		
		// Initiation of three bookings
		receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		receptionist.initiateBooking("Klaus", "20141010", "20141015","Dieter");
		
		// Adding four rooms to booking 1
		receptionist.addRoomToBooking("single room", 1);
		receptionist.addRoomToBooking("double room", 1);
		receptionist.addRoomToBooking("double room", 1);
		assertTrue(receptionist.addRoomToBooking("double room", 1));
		
		// No more double rooms are available
		assertFalse(receptionist.addRoomToBooking("double room", 1));
		assertFalse(receptionist.addRoomToBooking("double room", 3));
		
		// Confirmation does not work without a room
		assertTrue(receptionist.confirmBooking(1));
		assertFalse(receptionist.confirmBooking(3));
		
		//Booking 1 has right amount of rooms associated
		Booking bk = bookingHandler.getBookingcatalogue().getBooking(1);
		assertEquals("Booking 1 has an entry for single room", "single room", bk.getRoomMap().get(0).getKey().getRoomTypeDescription());
		assertEquals("Booking 1 has an entry for double room", "double room", bk.getRoomMap().get(1).getKey().getRoomTypeDescription());
		assertEquals("Booking 1 has one single room", 1, (int) bk.getRoomMap().get(0).getValue());
		assertEquals("Booking 1 has three double rooms", 3, (int) bk.getRoomMap().get(1).getValue());
	}
	
	/**
	 * UC 2.1.1. (CH)
	 * This test makes sure that only bookings with rooms can be confirmed.
	 */
	@Test
	public void confirmBookingTest() {
		// Initial hotel setup
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
		this.admin.addRoom(2, "single room");
		
		// Initiate bookings 
		this.receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		this.receptionist.addRoomToBooking("single room", 1);
		this.receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		this.receptionist.initiateBooking("Klaus", "20141010", "20141015", "Dieter");
		
		// Invalid IDs should not be confirmed.
		assertFalse(this.receptionist.confirmBooking(0)); 
		assertFalse(this.receptionist.confirmBooking(-1));
		assertFalse(this.receptionist.confirmBooking(20));
		
		// Confirm non-empty bookings
		assertTrue(this.receptionist.confirmBooking(1)); 
    	assertEquals("Booking 1 is confirmed", BookingStatus.CONFIRMED, this.bookingHandler.getBookingcatalogue().getBooking(1).getBookingStatus());
    	
    	// already confirmed booking will stay confirmed and not confirm again
    	assertFalse(this.receptionist.confirmBooking(1)); // already confirmed
		assertEquals("Booking 1 is confirmed", BookingStatus.CONFIRMED, this.bookingHandler.getBookingcatalogue().getBooking(1).getBookingStatus());

		// Empty bookings will not be confirmed
		assertFalse(this.receptionist.confirmBooking(2));
		assertEquals("Booking 2 is unconfirmed", BookingStatus.UNCONFIRMED, this.bookingHandler.getBookingcatalogue().getBooking(2).getBookingStatus());
	}
	
	/**
	 * UC 2.1.1. (CH)
	 * This test assures that rooms will only be added it free ones are available.
	 */
	@Test
	public void addTooManyRoomsTest() {
		// Add room to hotel
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
		
		// Initiate bookings with interleaving start and end dates
		receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		receptionist.initiateBooking("Karl","20141011", "20141015", "Heinz");
		receptionist.initiateBooking("Karl", "20141010", "20141014", "Heinz");
		
		// Add available room to booking 1
		assertTrue(receptionist.addRoomToBooking("single room", 1));
		assertEquals("Booking 1 has an entry for single room", "single room", bookingHandler.getBookingcatalogue().getBooking(1).getRoomMap().get(0).getKey().getRoomTypeDescription());

		// trying to add another room should fail
		assertFalse(receptionist.addRoomToBooking("single room", 1));
		assertFalse(receptionist.addRoomToBooking("single room", 2));
		assertFalse(receptionist.addRoomToBooking("single room", 3));
		assertFalse(receptionist.addRoomToBooking("single room", 4));
		
		assertTrue(bookingHandler.getBookingcatalogue().getBooking(2).getRoomMap().isEmpty());
		assertTrue(bookingHandler.getBookingcatalogue().getBooking(4).getRoomMap().isEmpty());
		assertTrue(bookingHandler.getBookingcatalogue().getBooking(3).getRoomMap().isEmpty());
		
		// after adding another room, there will be one more available
		this.admin.addRoom(2, "single room");
		assertTrue(receptionist.addRoomToBooking("single room", 1));
	}
	
	/**
	 * UC 2.1.2
	 * Search for free rooms
	 */
	@Test
	public void searchForFreeRoomsTest() {
		this.admin.startup(100);
		// Start by checking the number of free rooms in January.
		String startDate, endDate, beforeDate, inbetweenDate, afterDate;
		startDate = "20170101";
		endDate = "20171231";
		beforeDate = "20161004";
		inbetweenDate = "20170416";
		afterDate = "20180610";
		EList<FreeRoomTypesDTO> initialFreeRooms = this.receptionist.getFreeRooms(2, startDate, endDate);
		int initialSumFreeRooms = 0;
		for (FreeRoomTypesDTO f : initialFreeRooms) {
			initialSumFreeRooms += f.getNumFreeRooms();
		}
		
		// Create a booking.
		int bookingId1 = this.receptionist.initiateBooking("Rikard", startDate, endDate, "Hjort");
		assertTrue(0 <= bookingId1); // Make sure this booking succeeds.
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId1));
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId1));
		assertTrue(this.receptionist.confirmBooking(bookingId1));
		EList<FreeRoomTypesDTO> freeRoomsAfterBooking1 = this.receptionist.getFreeRooms(2, startDate, endDate);
		int sumFreeRoomsAfterBooking1 = 0;
		for (FreeRoomTypesDTO f : freeRoomsAfterBooking1) {
			sumFreeRoomsAfterBooking1 += f.getNumFreeRooms();
		}
		assertEquals(initialSumFreeRooms - 2, sumFreeRoomsAfterBooking1);
		
		// Create a booking.
		int bookingId2 = this.receptionist.initiateBooking("Rikard", beforeDate, inbetweenDate, "Hjort");
		assertTrue(0 <= bookingId2); // Make sure this booking succeeds.
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId2));
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId2));
		assertTrue(this.receptionist.confirmBooking(bookingId2));
		EList<FreeRoomTypesDTO> freeRoomsAfterBooking2 = this.receptionist.getFreeRooms(2, startDate, endDate);
		int sumFreeRoomsAfterBooking2 = 0;
		for (FreeRoomTypesDTO f : freeRoomsAfterBooking2) {
			sumFreeRoomsAfterBooking2 += f.getNumFreeRooms();
		}
		assertEquals(sumFreeRoomsAfterBooking1 - 2, sumFreeRoomsAfterBooking2);
		
		// Create a booking.
		int bookingId3 = this.receptionist.initiateBooking("Rikard", beforeDate, inbetweenDate, "Hjort");
		assertTrue(0 <= bookingId3); // Make sure this booking succeeds.
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId3));
		assertTrue(this.receptionist.addRoomToBooking("default", bookingId3));
		assertTrue(this.receptionist.confirmBooking(bookingId3));
		EList<FreeRoomTypesDTO> freeRoomsAfterBooking3 = this.receptionist.getFreeRooms(2, startDate, endDate);
		int sumFreeRoomsAfterBooking3 = 0;
		for (FreeRoomTypesDTO f : freeRoomsAfterBooking3) {
			sumFreeRoomsAfterBooking3 += f.getNumFreeRooms();
		}
		assertEquals(sumFreeRoomsAfterBooking2 - 2, sumFreeRoomsAfterBooking3);	
	}
	
	/**
	 * UC 2.1.3 (Michelle)
	 */
	@Test
	public void checkInTest(){
		//Create a singleBed room
		this.admin.addRoomType(1, "singleBed", 42);
		this.admin.addRoom(1, "singleBed");
		Room r = this.roomHandler.roomcatalogue().getRoomById(1);

		//create and confirm a booking
		this.receptionist.initiateBooking("Michelle", "20160101", "20160110", "TL");
		this.receptionist.addRoomToBooking("singleBed", 1);
		this.receptionist.confirmBooking(1);
		Booking b = this.bookingHandler.bookingcatalogue().getBooking(1);
		
		//Tests if the method is returning one available rooms if there's only one room available
		EList<Room> roomsAvailable = this.receptionist.initiateCheckIn(1);
		EList<Room> expectedRoomsAvailable = new BasicEList<Room>();
		expectedRoomsAvailable.add(r);
		
		assertEquals(expectedRoomsAvailable.size(), roomsAvailable.size());
		assertEquals(expectedRoomsAvailable, roomsAvailable);
		
		//Test when no rooms are available
		r.setRoomStatus(RoomStatus.OCCUPIED);
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		expectedRoomsAvailable.clear();
		assertEquals(0, roomsAvailable.size());
		assertEquals(expectedRoomsAvailable, roomsAvailable);
		
		//Test when a room is blocked
		r.setRoomStatus(RoomStatus.BLOCKED);
		expectedRoomsAvailable.clear();
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		assertEquals(0, roomsAvailable.size());
		assertEquals(expectedRoomsAvailable, roomsAvailable);
		
		//Test when a booking is in an invalid state
		r.setRoomStatus(RoomStatus.FREE);
		b.setBookingStatus(BookingStatus.UNCONFIRMED);
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		assertNull(roomsAvailable);
		b.setBookingStatus(BookingStatus.CHECKED_IN);
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		assertNull(roomsAvailable);
		b.setBookingStatus(BookingStatus.CHECKED_OUT);
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		assertNull(roomsAvailable);
		b.setBookingStatus(BookingStatus.CANCELED);
		roomsAvailable = this.receptionist.initiateCheckIn(1);
		assertNull(roomsAvailable);
	}
	
	/**
	 * UC 2.1.4 CheckOout booking! 
	 */
	@Test
	public void checkOutTest() {
		this.admin.addRoomType(1, "single room", 300.0);
		int roomId1 = 1;
		this.admin.addRoom(roomId1, "single room");
		
		int booking1 = receptionist.initiateBooking("Liv", "20141010", "20141011", "Eterénphest");
		this.receptionist.addRoomToBooking("single room", booking1);
		this.receptionist.confirmBooking(booking1);
		
		this.receptionist.checkInRoomById(booking1, roomId1);
		
		double costRoom = this.receptionist.initiateCheckout(booking1);
		//
		boolean isBooking1Paid = this.receptionist.payDuringCheckout("99999999", "111", 11, 16, "Bertil", "B");
		assertEquals(300.0, costRoom, 0.001);
		assertEquals("Payment fails, credit card is not yet registered", false, isBooking1Paid);

		try{
			this.bankingAdmin.addCreditCard("88888888", "111", 11, 16, "Bertil", "B");
			
		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
		
		isBooking1Paid = this.receptionist.payDuringCheckout("88888888", "111", 11, 16, "Bertil", "B");
		assertFalse("Payment fails not enough balance", isBooking1Paid);
		
		try{
			this.bankingAdmin.addCreditCard("77777777", "111", 11, 16, "Bertil", "B");
			this.bankingAdmin.makeDeposit("77777777", "111", 11, 16, "Bertil", "B",2000.0);
		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
		isBooking1Paid = this.receptionist.payDuringCheckout("77777777", "111", 11, 16, "Bertil", "B");
		assertTrue("Payment succedded", isBooking1Paid);

	}
	
	/**
	 * UC 2.1.5. (CH)
	 * This test assures that the receptionist can edit the dates for a certain booking.
	 */
	@Test
	public void editBookingTimeTest() {
		// Setup for hotel
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
		
		//Initialize two subsequent bookings 		
		receptionist.initiateBooking("Hans", "20141011", "20141014", "Peter");
		assertTrue(this.receptionist.addRoomToBooking("single room", 1));
		Booking bking = bookingHandler.getBookingcatalogue().getBooking(1);
		assertEquals("Booking 1 has one single room", 1, (int) bking.getRoomMap().get(0).getValue());
		
		receptionist.initiateBooking("Karl","20141015", "20141017", "Heinz");
		assertTrue(this.receptionist.addRoomToBooking("single room", 2));
		Booking bking2 = bookingHandler.getBookingcatalogue().getBooking(2);
		assertEquals("Booking 2 has one single room", 1, (int) bking2.getRoomMap().get(0).getValue());
		
		// Check that only one rooms is counted
		assertEquals("There is only one room", 1, roomHandler.getNbrOfRooms("single room"));
		
		// Try to change the booking
		assertTrue(receptionist.editBooking(1, "20141008", "20141015")); // checkout before other booking checks in
		assertFalse(receptionist.editBooking(1, "20141008", "20141017")); // overlapping bookings
		
		// Create another booking and try to add a room to it
		receptionist.initiateBooking("Hans", "20141008", "20141016", "Peter");
		assertFalse(receptionist.addRoomToBooking("single room", 3)); // all rooms are taken
	}

	/**
	 * UC 2.1.5. (CH)
	 * This test assures that rooms can be added using the edit function after creation of a booking.
	 */
	@Test
	public void editBookingRoomsTest() {
		// Setup for hotel
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
		this.admin.addRoom(2, "single room");
		
		// Initial booking setup
		receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		this.receptionist.addRoomToBooking("single room", 1);
		Booking bk = bookingHandler.getBookingcatalogue().getBooking(1);
		assertEquals("Booking 1 has one single room", 1, (int) bk.getRoomMap().get(0).getValue());
		
		// Create a list of RoomTypeToNumberEntry to include 2 rooms
		RoomTypeToNumberEntry entry = BookingFactory.eINSTANCE.createRoomTypeToNumberEntry();
		entry.setKey(roomHandler.getLstRoomType().get(0));
		entry.setValue(2); // 2 rooms should be associated
		
		BasicEList<RoomTypeToNumberEntry> list = new BasicEList<RoomTypeToNumberEntry>();
		list.add(entry);
		
		// Add another room by editing booking
		receptionist.editBooking(1, list);
		bk = bookingHandler.getBookingcatalogue().getBooking(1);
		assertEquals("Booking 1 has two single rooms", 2, (int) bk.getRoomMap().get(0).getValue());
		
		// Add one room too much
		list.get(0).setValue(3);
		assertFalse(receptionist.editBooking(1, list));
		
		// Reduce number of rooms
		list.get(0).setValue(1);
		assertTrue(receptionist.editBooking(1, list));
		assertEquals("Booking 1 has one single rooms", 1, (int) bk.getRoomMap().get(0).getValue());		
	}

	/**
	 * UC 2.1.5. (CH)
	 * This test assures that changing both date and number of rooms will work as expected
	 */
	@Test
	public void editBookingDateAndRoomTest() {
		// Initial hotel setup
		this.admin.addRoomType(1, "single room", 300.0);
		this.admin.addRoom(1, "single room");
	    this.admin.addRoom(2, "single room");
		this.admin.addRoom(3, "single room");
		this.admin.addRoomType(2, "double room", 500.0);
		this.admin.addRoom(4, "double room");
	    this.admin.addRoom(5, "double room");
		this.admin.addRoom(6, "double room");
			
		// Initiate bookings
		receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		this.receptionist.addRoomToBooking("single room", 1);
			
		receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		this.receptionist.addRoomToBooking("single room", 2);
		
		// Assure correct setup
		Booking bk = bookingHandler.getBookingcatalogue().getBooking(1);
		assertEquals("Booking 1 has one single room", 1, (int) bk.getRoomMap().get(0).getValue());
			
		// Create list for changes
		BasicEList<RoomTypeToNumberEntry> list = new BasicEList<RoomTypeToNumberEntry>();
	
		// Add two single room and two double rooms		
		RoomTypeToNumberEntry singleRoomEntry = BookingFactory.eINSTANCE.createRoomTypeToNumberEntry();		
		singleRoomEntry.setKey(roomHandler.getLstRoomType().get(0));
		singleRoomEntry.setValue(3); 
		list.add(singleRoomEntry);
		RoomTypeToNumberEntry doubleRoomEntry = BookingFactory.eINSTANCE.createRoomTypeToNumberEntry();	
		doubleRoomEntry.setKey(roomHandler.getLstRoomType().get(1));
		doubleRoomEntry.setValue(2);
		list.add(doubleRoomEntry);
		
		// Add too many rooms
		assertFalse(receptionist.editBooking(1,"20141010", "20141015", list));
		assertEquals("Booking 1 has one single rooms", 1, (int) bk.getRoomMap().get(0).getValue());
		
		// Use different dates for three rooms
		assertTrue(receptionist.editBooking(1,"20140910","20140915", list));
		assertEquals("Booking 1 has three single rooms", 3, (int) bk.getRoomMap().get(0).getValue());
		assertEquals("Booking 1 has two double rooms", 2, (int) bk.getRoomMap().get(1).getValue());		
		
		// Use wrong dates
		assertFalse(receptionist.editBooking(1,"20140910", "20140908", list));
		assertEquals("Booking 1 has three single rooms", 3, (int) bk.getRoomMap().get(0).getValue());
		assertEquals("Booking 1 has two double rooms", 2, (int) bk.getRoomMap().get(1).getValue());	
		
		// Add rooms and change days
		list.get(0).setValue(2);
		assertTrue(receptionist.editBooking(1, "20141010", "20141017", list));
		assertEquals("Booking 1 has three single rooms", 2, (int) bk.getRoomMap().get(0).getValue());
		assertEquals("Booking 1 has two double rooms", 2, (int) bk.getRoomMap().get(1).getValue());			
	}

	/**
	 * UC 2.1.6. (CH)
	 * This test assures that unconfirmed and confirmed bookings can be canceled. CheckedIn bookings are assumed to be checked out.
	 */
	@Test
	public void cancelBookingTest() {
		// Setup hotel with rooms
		admin.addRoomType(1, "single room", 300.0);
		admin.addRoom(1, "single room");
		admin.addRoom(2, "single room");
		admin.addRoom(3, "single room");
		
		// Initiate bookings for test
		receptionist.initiateBooking("Hans", "20141010", "20141015", "Peter");
		
		receptionist.initiateBooking("Klaus", "20141010", "20141015", "Peter");
		receptionist.addRoomToBooking("single room", 2);
		
		receptionist.initiateBooking("Karl", "20141010", "20141015", "Heinz");
		receptionist.addRoomToBooking("single room", 3);	
		
		// Set status to unconfirmed, confirmed and checked in
		receptionist.confirmBooking(2);
		receptionist.confirmBooking(3);
		receptionist.checkInRoomById(3, 1);
		
		// Check status
		BookingCatalogue bC = bookingHandler.getBookingcatalogue();
		assertEquals("Booking 1 exists and is unconfirmed", BookingStatus.UNCONFIRMED, bC.getBooking(1).getBookingStatus());
		assertEquals("Booking 2 exists and is confirmed", BookingStatus.CONFIRMED, bC.getBooking(2).getBookingStatus());
		assertEquals("Booking 3 exists and is confirmed", BookingStatus.CHECKED_IN, bC.getBooking(3).getBookingStatus());
			
		// Cancel bookings and check states
		assertTrue(receptionist.cancelBooking(1)); // empty booking
		assertEquals("Booking 1 exists and is canceled", BookingStatus.CANCELED, bC.getBooking(1).getBookingStatus());
		
		assertTrue(receptionist.cancelBooking(2)); // booking with room
		assertEquals("Booking 2 exists and is canceled", BookingStatus.CANCELED, bC.getBooking(2).getBookingStatus());
		assertTrue("Booking 2 has an empty room list", bC.getBooking(2).getRoomMap().isEmpty());
		
		assertFalse(receptionist.cancelBooking(3)); // already checkedIn
		assertEquals("Booking 3 exists and stays checked-in", BookingStatus.CHECKED_IN, bC.getBooking(3).getBookingStatus()); // checked in Booking should be uncanceled
	}	
	
	/**
	 * UC 2.1.7
	 */
	@Test
	public void listBookingTest(){
		//Create rooms
		this.admin.addRoomType(1, "singleBed", 42);
		for(int i = 0; i < 5; i++){
			this.admin.addRoom(i+1, "singleBed");
		}
		
		//Create bookings
		for(int i = 0; i < 5; i++){
			this.receptionist.initiateBooking("A", "20011010", "21001010", "B");
			this.receptionist.addRoomToBooking("singleBed", i+1);
			this.receptionist.confirmBooking(i+1);
		}
		
		//Testing
		for(int i = 0; i < 5; i++){
			Booking b = this.bookingHandler.listBookings().get(i);
			assertEquals(this.bookingHandler.listBookings().size(), 5);
			assertEquals(b.getFirstName(), "A");
			assertEquals(b.getLastName(), "B");
			assertEquals(b.getBookingId(), i+1);
			assertEquals(b.getBookingStatus(), BookingStatus.CONFIRMED);
			assertEquals(new Date(101,9,10), b.getStartDate());
			assertEquals(new Date(200,9,10), b.getEndDate());
		}
	}
	
	/**
	 * UC 2.1.7
	 */
	@Test
	public void getBookingsForPeriodTest(){
		//Create rooms
		this.admin.addRoomType(1, "singleBed", 42);
		for(int i = 0; i < 4; i++){
			this.admin.addRoom(i+1, "singleBed");
		}
		
		//0 Create bookings A jan-feb
		String sdA = new Date(2016, 0, 1).toString();
		String edA = new Date(2016, 1, 1).toString();
		this.receptionist.initiateBooking("Anna","20160101", "20160201", "A");
		this.receptionist.addRoomToBooking("singleBed", 1);
		this.receptionist.confirmBooking(1);
	
		//1 Create bookings B mid jan- mid feb
		String sdB = new Date(2016, 0, 15).toString();
		String edB = new Date(2016, 1, 15).toString();
		this.receptionist.initiateBooking("Bertil", "20160115", "20160215", "B");
		this.receptionist.addRoomToBooking("singleBed", 2);
		this.receptionist.confirmBooking(2);

		//2 Create bookings C feb-march
		String sdC = new Date(2016, 1, 1).toString();
		String edC = new Date(2016, 2, 1).toString();
		this.receptionist.initiateBooking("Cecilia", "20160201", "20160301", "C");
		this.receptionist.addRoomToBooking("singleBed", 3);
		this.receptionist.confirmBooking(3);

		//3 Create bookings D 2march-2april
		String sdD = new Date(2016, 2, 2).toString();
		String edD = new Date(2016, 3, 1).toString();
		this.receptionist.initiateBooking("David", "20160302", "20160402", "D");
		this.receptionist.addRoomToBooking("singleBed", 4);
		this.receptionist.confirmBooking(4);
				
		//No overlaps
		Date sdO = new Date(117, 0, 1);
		Date edO = new Date(117, 0, 1);
		EList<Booking> no = new BasicEList<>();
		EList<Booking> noOverLapsBookings = this.bookingHandler.bookingcatalogue().getBookingsForPeriod(sdO, edO);
		assertEquals(0, noOverLapsBookings.size());
		assertEquals(no, noOverLapsBookings);

		//Overlaps with one Booking that has exactly the same period
		Date sdOne = new Date(116, 2, 2);
		Date edOne = new Date(116, 3, 1);
		EList<Booking> b = new BasicEList<>();
		b.add(this.bookingHandler.bookingcatalogue().getBooking(4));
		EList<Booking> one = this.bookingHandler.bookingcatalogue().getBookingsForPeriod(sdOne, edOne);
		assertEquals(1, one.size());
		assertEquals(b, one);
		
		//Overlaps with three bookings
		Date sdThree = new Date(116, 0, 20);
		Date edThree = new Date(116, 1, 10);
		EList<Booking> c = new BasicEList<>();
		c.add(this.bookingHandler.bookingcatalogue().getBooking(1));
		c.add(this.bookingHandler.bookingcatalogue().getBooking(2));
		c.add(this.bookingHandler.bookingcatalogue().getBooking(3));
		EList<Booking> three = this.bookingHandler.bookingcatalogue().getBookingsForPeriod(sdThree, edThree);
		assertEquals(3, three.size());
		assertEquals(c, three);
	}
	
	/**
	 * UC 2.1.8 (CH)
	 */
	@Test
	public void listOccupiedRoomsTest(){
		
		EList<RoomIdToBookingIdEntry> list;
		
		//Create rooms
		this.admin.addRoomType(1, "singleBed", 42);
		this.admin.addRoom(1, "singleBed");
		
		// At the "beginning of time", there were no rooms occupied.
		list = this.receptionist.listOccupiedRooms(new Date(0l));
		assertNotNull(list);
		assertTrue(list.isEmpty());
		
		// In the future, there are no rooms occupied.
		Date d = new Date(); // Today
		d.setDate(d.getDate() + 1); // Tomorrow
		list = this.receptionist.listOccupiedRooms(d);
		assertNotNull(list);
		assertTrue(list.isEmpty());
		
		//Setup bookings
		this.receptionist.initiateBooking("A", "20161212", "20161213", "B");
		this.receptionist.addRoomToBooking("singleBed", 1);
		this.receptionist.confirmBooking(1);
		
		//Save roomIds
		int roomId = this.receptionist.initiateCheckIn(1).get(0).getId();
		
		//Check in
		this.receptionist.checkInRoomById(1, roomId);
		
		//Fetch results 
		list = this.receptionist.listOccupiedRooms(new Date());
		
		//Testing
		assertNotNull(list);
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).getBookingId(), 1);
		assertEquals(list.get(0).getRoomId(), roomId);
		
		//Check out
		this.receptionist.initiateCheckout(1);
		
		try{
			this.bankingAdmin.addCreditCard("77777777", "111", 11, 16, "Bertil", "B");
			this.bankingAdmin.makeDeposit("77777777", "111", 11, 16, "Bertil", "B",2000.0);
			
			this.receptionist.payDuringCheckout("77777777", "111", 11, 16, "Bertil", "B");
			
			list = this.receptionist.listOccupiedRooms(new Date());

			//Testing
			assertNotNull(list);
			assertEquals(list.size(), 1);
			assertEquals(list.get(0).getBookingId(), 1);
			assertEquals(list.get(0).getRoomId(), roomId);
			
		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
	}
	
	/**
	 * UC 2.1.9
	 */
	@Test
	public void listCheckInsTest(){
		//Create rooms
		this.admin.addRoomType(1, "singleBed", 42);
		this.admin.addRoom(1, "singleBed");
		
		//Setup dates
		String date1 = "20161212";
		String date2 = "20161213";
		
		//Setup bookings
		this.receptionist.initiateBooking("A", date1, date2, "B");
		this.receptionist.addRoomToBooking("singleBed", 1);
		this.receptionist.confirmBooking(1);
		
		//Save roomId
		int roomId = this.receptionist.initiateCheckIn(1).get(0).getId();
		
		//Check in
		this.receptionist.checkInRoomById(1, roomId);
		
		//Fetch results
		EList<CheckIn> list = this.receptionist.listCheckIns(new Date());
		
		//Testing
		assertNotNull(list);
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).getBookingId(), 1);
		assertEquals(list.get(0).getRoomIds().size(), 1);
		assertEquals(list.get(0).getRoomIds().get(0), new Integer(roomId));
		
		Date today = new Date();
		Date tomorrow = new Date();
		Date yesterday = new Date();
		tomorrow.setDate(today.getDate() + 1);
		yesterday.setDate(today.getDate() - 1);
		
		//Overloaded version testing
		list = this.receptionist.listCheckIns(yesterday, tomorrow);
		
		assertNotNull(list);
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).getBookingId(), 1);
		assertEquals(list.get(0).getRoomIds().size(), 1);
		assertEquals(list.get(0).getRoomIds().get(0), new Integer(roomId));
	}
	
	/**
	 * UC 2.1.10
	 */
	@Test
	public void listCheckOutsTest(){
		//Create rooms
		this.admin.addRoomType(1, "singleBed", 42);
		this.admin.addRoom(1, "singleBed");
		
		//Setup dates
		String date1 = "20161212";
		String date2 = "20161213";
		
		//Setup bookings
		this.receptionist.initiateBooking("A", date1, date2, "B");
		this.receptionist.addRoomToBooking("singleBed", 1);
		this.receptionist.confirmBooking(1);
		
		//Save roomId
		int roomId = this.receptionist.initiateCheckIn(1).get(0).getId();
		
		//Check in
		this.receptionist.checkInRoomById(1, roomId);
		
		//Check out
		this.receptionist.initiateCheckout(1);
		try{
			AdministratorRequires admin = AdministratorRequires.instance();
			admin.addCreditCard("77777777", "111", 11, 16, "Bertil", "B");
			admin.makeDeposit("77777777", "111", 11, 16, "Bertil", "B", 2000.0);
		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
		this.receptionist.payDuringCheckout("77777777", "111", 11, 16, "Bertil", "B");
		
		//Fetch results
		EList<CheckOut> list = this.receptionist.listCheckOuts(new Date());
		
		//Testing
		assertNotNull(list);
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getBookingId());
		assertEquals(roomId, list.get(0).getRoomId());
		
		Date today = new Date();
		Date tomorrow = new Date();
		Date yesterday = new Date();
		tomorrow.setDate(today.getDate() + 1);
		yesterday.setDate(today.getDate() - 1);
		
		//Overloaded version testing
		list = this.receptionist.listCheckOuts(yesterday, tomorrow);
		
		assertNotNull(list);
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getBookingId());
		assertEquals(roomId, list.get(0).getRoomId());
	}
	
	/**
	 * UC 2.1.11
	 */
	@Test
	public void separateCheckInTest() {
		this.admin.addRoomType(1, "single room", 300.0);
		
		int roomId1 = 1;
		int roomId2 = 2;
		this.admin.addRoom(roomId1, "single room");
		this.admin.addRoom(roomId2, "single room");
		
		int booking1 = receptionist.initiateBooking("Roffe", "20141010", "20141015", "Ojage");
		int booking2 = receptionist.initiateBooking("Hadette", "20141010", "20141015", "Bandí");
		int booking3 = receptionist.initiateBooking("Ette", "20141010", "20141015", "Gárágë");
		
		this.receptionist.addRoomToBooking("single room", booking1);
		this.receptionist.addRoomToBooking("single room", booking2);
		
		this.receptionist.confirmBooking(booking1);

		/* booking1 is a valid checkIn-able booking,
		 * booking2 has a room but is unconfirmed,
		 * booking3 is without rooms and unconfirmed */
		
		this.receptionist.checkInRoomById(booking1, roomId1);
		assertEquals("Room 1 should be checked in", 
				RoomStatus.OCCUPIED, this.roomHandler.roomcatalogue().getRoomById(roomId1).getRoomStatus());
		
		this.receptionist.checkInRoomById(booking2, roomId2);
		assertEquals("Room 2 should not be checked in", 
				RoomStatus.FREE, this.roomHandler.roomcatalogue().getRoomById(roomId2).getRoomStatus());
		
		this.receptionist.checkInRoomById(booking3, roomId2);
		assertEquals("Room 2 should still not be checked in",
				RoomStatus.FREE, this.roomHandler.roomcatalogue().getRoomById(roomId2).getRoomStatus());
	}
	
	/**
	 * UC 2.1.12
	 */
	@Test
	public void separateCheckOutAndPayTest(){
		try{
			this.bankingAdmin.addCreditCard("00000000", "000", 11, 16, "Peter", "Pan");
			this.bankingAdmin.addCreditCard("11111111", "111", 11, 16, "Anna", "A");
			this.bankingAdmin.makeDeposit("00000000", "000", 11, 16, "Peter", "Pan", 3000.0);
			this.bankingAdmin.makeDeposit("11111111", "111", 11, 16, "Anna", "A", 2000.0);
		} catch (SOAPException e) {
 			e.printStackTrace();
 		} 
		
		//Create room type
		admin.addRoomType(1, "single room", 300.0);
		
		//Create 2 rooms
		int roomId1 = 1;
		int roomId2 = 2;
		int roomId3 = 3;
		admin.addRoom(roomId1, "single room");
		admin.addRoom(roomId2, "single room");
		admin.addRoom(roomId3, "single room");

		//Create 3 bookings
		int booking1 = receptionist.initiateBooking("Anna", "20141010", "20141015", "A");
		int booking2 = receptionist.initiateBooking("Bertil", "20141010", "20141015", "B");
		
		//Add rooms to bookings
		receptionist.addRoomToBooking("single room", booking1);
		receptionist.addRoomToBooking("single room", booking2);
		receptionist.addRoomToBooking("single room", booking1);
		
		//Confirm all bookings
		receptionist.confirmBooking(booking1);
		receptionist.confirmBooking(booking2);
		
		//Checked in rooms
		receptionist.checkInRoomById(booking1, roomId1);
		receptionist.checkInRoomById(booking1, roomId2);
		receptionist.checkInRoomById(booking2, roomId3);
	
		//Check out rooms separately
		receptionist.initiateRoomCheckout(roomId1, booking1);
		boolean isPaidRoom = receptionist.payRoomDuringCheckout(roomId1, "00000000", "000", 11, 16, "Peter", "Pan");
		boolean isNotPaidWrongCard = receptionist.payRoomDuringCheckout(roomId1, "00001000", "000", 11, 16, "Peter", "Pan");
		assertEquals("Room 1 should be checked out -> RoomStatus roomId1 = FREE", 
				RoomStatus.FREE, roomHandler.roomcatalogue().getRoomById(roomId1).getRoomStatus());
		assertTrue("Payment succeeded", isPaidRoom);
		assertFalse("Payment failed due to wrong card number", isNotPaidWrongCard); 
		
		double cost = receptionist.initiateRoomCheckout(roomId1, booking2);
		boolean isNotPaidWrongInfo = receptionist.payRoomDuringCheckout(roomId1, "00000000", "000", 11, 16, "Peter", "Pan");
		assertEquals("Room 1 should not be checked out due to wrong bookingId ", 
				-100, cost, 0.001);
		assertFalse("Payment fails due to wrong bookingId", isNotPaidWrongInfo);
	
		receptionist.initiateRoomCheckout(roomId3, booking2);
		boolean isRoom3Paid = receptionist.payRoomDuringCheckout(roomId3, "11111111", "111", 11, 16, "Anna", "A");
		assertEquals("Room 3 should be checked out",
				RoomStatus.FREE, roomHandler.roomcatalogue().getRoomById(roomId3).getRoomStatus());
		assertTrue("Payment succeeded for room 3", isRoom3Paid);

		receptionist.initiateRoomCheckout(roomId3, booking2);
		boolean isRoom3AlreadyPaid = receptionist.payRoomDuringCheckout(roomId3, "11111111", "111", 11, 16, "Anna", "A");
		assertEquals("Room 3 is still checked out",
				RoomStatus.FREE, roomHandler.roomcatalogue().getRoomById(roomId3).getRoomStatus());
		assertFalse("Already paid for room 3", isRoom3AlreadyPaid);

		receptionist.initiateRoomCheckout(roomId2, booking1);
		boolean isRoom2Paid = receptionist.payRoomDuringCheckout(roomId2, "00000000", "000", 11, 16, "Peter", "Pan");
		assertEquals("Room 2 should be checked out",
				RoomStatus.FREE, roomHandler.roomcatalogue().getRoomById(roomId2).getRoomStatus());
		assertTrue("Payment succeeded for room 2", isRoom2Paid);
	}
	
	/*
	 *  UC 2.1.13
	 */
	@Test
	public void addExtrasTest() {
		this.admin.addRoomType(1, "single room", 300.0);
		int roomId = 1;
		this.admin.addRoom(roomId, "single room");
		
		int bookingId = receptionist.initiateBooking("Barnaf", "20141010", "20141011", "Vårteed");
		this.receptionist.addRoomToBooking("single room", bookingId);
		this.receptionist.confirmBooking(bookingId);
		
		this.receptionist.checkInRoomById(bookingId, roomId);
		this.bookingHandler.getBooking(bookingId).getRoomsAssociated().get(0).addExtra("Room service", 150.0f);
		double cost = this.receptionist.initiateCheckout(bookingId);
		
		assertEquals(450.00, cost, 0.001);
	}

} //Receptionist
