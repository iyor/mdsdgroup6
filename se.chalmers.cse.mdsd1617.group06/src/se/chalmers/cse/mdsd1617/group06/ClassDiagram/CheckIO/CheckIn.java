/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Check In</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getDate <em>Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getRoomIds <em>Room Ids</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIn()
 * @model
 * @generated
 */
public interface CheckIn extends EObject {

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIn_Date()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * Returns the value of the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Id</em>' attribute.
	 * @see #setBookingId(int)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIn_BookingId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getBookingId <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Id</em>' attribute.
	 * @see #getBookingId()
	 * @generated
	 */
	void setBookingId(int value);

	/**
	 * Returns the value of the '<em><b>Room Ids</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Ids</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Ids</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIn_RoomIds()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Integer> getRoomIds();
	
	void addToRoomIds(Integer roomId);
} // CheckIn
