/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage
 * @generated
 */
public interface CheckIOFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CheckIOFactory eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Catalogue</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Catalogue</em>'.
	 * @generated
	 */
	CheckIOCatalogue createCheckIOCatalogue();

	/**
	 * Returns a new object of class '<em>Check In</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Check In</em>'.
	 * @generated
	 */
	CheckIn createCheckIn();

	/**
	 * Returns a new object of class '<em>Check Out</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Check Out</em>'.
	 * @generated
	 */
	CheckOut createCheckOut();

	/**
	 * Returns a new object of class '<em>Room Id To Booking Id Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Room Id To Booking Id Entry</em>'.
	 * @generated
	 */
	RoomIdToBookingIdEntry createRoomIdToBookingIdEntry();

	/**
	 * Returns a new object of class '<em>Handler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Handler</em>'.
	 * @generated
	 */
	CheckIOHandler createCheckIOHandler();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CheckIOPackage getCheckIOPackage();

} //CheckIOFactory
