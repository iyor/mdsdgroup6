/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.ClassDiagramFactory
 * @model kind="package"
 * @generated
 */
public interface ClassDiagramPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ClassDiagram";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group06/ClassDiagram.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group06.ClassDiagram";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassDiagramPackage eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ClassDiagramPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.AdminImpl <em>Admin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.AdminImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ClassDiagramPackageImpl#getAdmin()
	 * @generated
	 */
	int ADMIN = 0;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN__ROOMHANDLER = 0;

	/**
	 * The feature id for the '<em><b>Startupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN__STARTUPPROVIDES = 1;

	/**
	 * The number of structural features of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Add Room Type Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM_TYPE_TEST = 0;

	/**
	 * The operation id for the '<em>Update Room Type Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___UPDATE_ROOM_TYPE_TEST = 1;

	/**
	 * The operation id for the '<em>Remove Room Type Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___REMOVE_ROOM_TYPE_TEST = 2;

	/**
	 * The operation id for the '<em>Test Block Unblock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___TEST_BLOCK_UNBLOCK = 3;

	/**
	 * The operation id for the '<em>Startup Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___STARTUP_TEST = 4;

	/**
	 * The operation id for the '<em>Add Room Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM_TEST = 5;

	/**
	 * The number of operations of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_OPERATION_COUNT = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ReceptionistImpl <em>Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ReceptionistImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ClassDiagramPackageImpl#getReceptionist()
	 * @generated
	 */
	int RECEPTIONIST = 1;

	/**
	 * The feature id for the '<em><b>Booking Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__BOOKING_HANDLER = 0;

	/**
	 * The feature id for the '<em><b>Hotelcustomerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__HOTELCUSTOMERPROVIDES = 1;

	/**
	 * The feature id for the '<em><b>Room Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__ROOM_HANDLER = 2;

	/**
	 * The feature id for the '<em><b>Start Up Provides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__START_UP_PROVIDES = 3;

	/**
	 * The feature id for the '<em><b>Check IO Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__CHECK_IO_HANDLER = 4;

	/**
	 * The number of structural features of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Initiate Booking Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_BOOKING_TEST = 0;

	/**
	 * The operation id for the '<em>Add Rooms To Booking Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_ROOMS_TO_BOOKING_TEST = 1;

	/**
	 * The operation id for the '<em>Confirm Booking Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CONFIRM_BOOKING_TEST = 2;

	/**
	 * The operation id for the '<em>Cancel Booking Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CANCEL_BOOKING_TEST = 3;

	/**
	 * The operation id for the '<em>Add Extra To Booking Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_EXTRA_TO_BOOKING_TEST = 4;

	/**
	 * The operation id for the '<em>Search For Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___SEARCH_FOR_FREE_ROOMS__INT_DATE_DATE = 5;

	/**
	 * The operation id for the '<em>Add Extra Cost To Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_EXTRA_COST_TO_ROOMS__INT_INT_STRING_FLOAT = 6;

	/**
	 * The operation id for the '<em>Add Too Many Rooms Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_TOO_MANY_ROOMS_TEST = 7;

	/**
	 * The operation id for the '<em>Check In Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_TEST = 8;

	/**
	 * The operation id for the '<em>Check Out Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_OUT_TEST = 9;

	/**
	 * The operation id for the '<em>List Check Ins By Day Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_INS_BY_DAY_TEST = 10;

	/**
	 * The operation id for the '<em>List Check Outs By Day Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_OUTS_BY_DAY_TEST = 11;

	/**
	 * The operation id for the '<em>Edit Booking Time Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___EDIT_BOOKING_TIME_TEST = 12;

	/**
	 * The operation id for the '<em>Edit Booking Rooms Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___EDIT_BOOKING_ROOMS_TEST = 13;

	/**
	 * The operation id for the '<em>Edit Booking Date And Room Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___EDIT_BOOKING_DATE_AND_ROOM_TEST = 14;
	
	/**
	 * The number of operations of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_OPERATION_COUNT = 15;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin <em>Admin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Admin</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin
	 * @generated
	 */
	EClass getAdmin();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#getRoomhandler()
	 * @see #getAdmin()
	 * @generated
	 */
	EReference getAdmin_Roomhandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#getStartupprovides <em>Startupprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Startupprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#getStartupprovides()
	 * @see #getAdmin()
	 * @generated
	 */
	EReference getAdmin_Startupprovides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#addRoomTypeTest() <em>Add Room Type Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#addRoomTypeTest()
	 * @generated
	 */
	EOperation getAdmin__AddRoomTypeTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#updateRoomTypeTest() <em>Update Room Type Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#updateRoomTypeTest()
	 * @generated
	 */
	EOperation getAdmin__UpdateRoomTypeTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#removeRoomTypeTest() <em>Remove Room Type Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#removeRoomTypeTest()
	 * @generated
	 */
	EOperation getAdmin__RemoveRoomTypeTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#testBlockUnblock() <em>Test Block Unblock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Test Block Unblock</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#testBlockUnblock()
	 * @generated
	 */
	EOperation getAdmin__TestBlockUnblock();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#startupTest() <em>Startup Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#startupTest()
	 * @generated
	 */
	EOperation getAdmin__StartupTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#addRoomTest() <em>Add Room Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Admin#addRoomTest()
	 * @generated
	 */
	EOperation getAdmin__AddRoomTest();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist <em>Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist
	 * @generated
	 */
	EClass getReceptionist();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getBookingHandler <em>Booking Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Booking Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getBookingHandler()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_BookingHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getHotelcustomerprovides <em>Hotelcustomerprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hotelcustomerprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getHotelcustomerprovides()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_Hotelcustomerprovides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getRoomHandler <em>Room Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getRoomHandler()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_RoomHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getStartUpProvides <em>Start Up Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start Up Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getStartUpProvides()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_StartUpProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getCheckIOHandler <em>Check IO Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Check IO Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#getCheckIOHandler()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_CheckIOHandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#initiateBookingTest() <em>Initiate Booking Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#initiateBookingTest()
	 * @generated
	 */
	EOperation getReceptionist__InitiateBookingTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addRoomsToBookingTest() <em>Add Rooms To Booking Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Rooms To Booking Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addRoomsToBookingTest()
	 * @generated
	 */
	EOperation getReceptionist__AddRoomsToBookingTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#confirmBookingTest() <em>Confirm Booking Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#confirmBookingTest()
	 * @generated
	 */
	EOperation getReceptionist__ConfirmBookingTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#cancelBookingTest() <em>Cancel Booking Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#cancelBookingTest()
	 * @generated
	 */
	EOperation getReceptionist__CancelBookingTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addExtraToBookingTest() <em>Add Extra To Booking Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra To Booking Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addExtraToBookingTest()
	 * @generated
	 */
	EOperation getReceptionist__AddExtraToBookingTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#searchForFreeRooms(int, java.util.Date, java.util.Date) <em>Search For Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search For Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#searchForFreeRooms(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__SearchForFreeRooms__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addExtraCostToRooms(int, int, java.lang.String, float) <em>Add Extra Cost To Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addExtraCostToRooms(int, int, java.lang.String, float)
	 * @generated
	 */
	EOperation getReceptionist__AddExtraCostToRooms__int_int_String_float();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addTooManyRoomsTest() <em>Add Too Many Rooms Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Too Many Rooms Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#addTooManyRoomsTest()
	 * @generated
	 */
	EOperation getReceptionist__AddTooManyRoomsTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#checkInTest() <em>Check In Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#checkInTest()
	 * @generated
	 */
	EOperation getReceptionist__CheckInTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#checkOutTest() <em>Check Out Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#checkOutTest()
	 * @generated
	 */
	EOperation getReceptionist__CheckOutTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#listCheckInsByDayTest() <em>List Check Ins By Day Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins By Day Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#listCheckInsByDayTest()
	 * @generated
	 */
	EOperation getReceptionist__ListCheckInsByDayTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#listCheckOutsByDayTest() <em>List Check Outs By Day Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs By Day Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#listCheckOutsByDayTest()
	 * @generated
	 */
	EOperation getReceptionist__ListCheckOutsByDayTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingTimeTest() <em>Edit Booking Time Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking Time Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingTimeTest()
	 * @generated
	 */
	EOperation getReceptionist__EditBookingTimeTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingRoomsTest() <em>Edit Booking Rooms Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking Rooms Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingRoomsTest()
	 * @generated
	 */
	EOperation getReceptionist__EditBookingRoomsTest();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingDateAndRoomTest() <em>Edit Booking Date And Room Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking Date And Room Test</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.Receptionist#editBookingDateAndRoomTest()
	 * @generated
	 */
	EOperation getReceptionist__EditBookingDateAndRoomTest();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassDiagramFactory getClassDiagramFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.AdminImpl <em>Admin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.AdminImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ClassDiagramPackageImpl#getAdmin()
		 * @generated
		 */
		EClass ADMIN = eINSTANCE.getAdmin();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN__ROOMHANDLER = eINSTANCE.getAdmin_Roomhandler();

		/**
		 * The meta object literal for the '<em><b>Startupprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN__STARTUPPROVIDES = eINSTANCE.getAdmin_Startupprovides();

		/**
		 * The meta object literal for the '<em><b>Add Room Type Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___ADD_ROOM_TYPE_TEST = eINSTANCE.getAdmin__AddRoomTypeTest();

		/**
		 * The meta object literal for the '<em><b>Update Room Type Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___UPDATE_ROOM_TYPE_TEST = eINSTANCE.getAdmin__UpdateRoomTypeTest();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___REMOVE_ROOM_TYPE_TEST = eINSTANCE.getAdmin__RemoveRoomTypeTest();

		/**
		 * The meta object literal for the '<em><b>Test Block Unblock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___TEST_BLOCK_UNBLOCK = eINSTANCE.getAdmin__TestBlockUnblock();

		/**
		 * The meta object literal for the '<em><b>Startup Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___STARTUP_TEST = eINSTANCE.getAdmin__StartupTest();

		/**
		 * The meta object literal for the '<em><b>Add Room Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___ADD_ROOM_TEST = eINSTANCE.getAdmin__AddRoomTest();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ReceptionistImpl <em>Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ReceptionistImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl.ClassDiagramPackageImpl#getReceptionist()
		 * @generated
		 */
		EClass RECEPTIONIST = eINSTANCE.getReceptionist();

		/**
		 * The meta object literal for the '<em><b>Booking Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__BOOKING_HANDLER = eINSTANCE.getReceptionist_BookingHandler();

		/**
		 * The meta object literal for the '<em><b>Hotelcustomerprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__HOTELCUSTOMERPROVIDES = eINSTANCE.getReceptionist_Hotelcustomerprovides();

		/**
		 * The meta object literal for the '<em><b>Room Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__ROOM_HANDLER = eINSTANCE.getReceptionist_RoomHandler();

		/**
		 * The meta object literal for the '<em><b>Start Up Provides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__START_UP_PROVIDES = eINSTANCE.getReceptionist_StartUpProvides();

		/**
		 * The meta object literal for the '<em><b>Check IO Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__CHECK_IO_HANDLER = eINSTANCE.getReceptionist_CheckIOHandler();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___INITIATE_BOOKING_TEST = eINSTANCE.getReceptionist__InitiateBookingTest();

		/**
		 * The meta object literal for the '<em><b>Add Rooms To Booking Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_ROOMS_TO_BOOKING_TEST = eINSTANCE.getReceptionist__AddRoomsToBookingTest();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CONFIRM_BOOKING_TEST = eINSTANCE.getReceptionist__ConfirmBookingTest();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CANCEL_BOOKING_TEST = eINSTANCE.getReceptionist__CancelBookingTest();

		/**
		 * The meta object literal for the '<em><b>Add Extra To Booking Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_EXTRA_TO_BOOKING_TEST = eINSTANCE.getReceptionist__AddExtraToBookingTest();

		/**
		 * The meta object literal for the '<em><b>Search For Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___SEARCH_FOR_FREE_ROOMS__INT_DATE_DATE = eINSTANCE.getReceptionist__SearchForFreeRooms__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_EXTRA_COST_TO_ROOMS__INT_INT_STRING_FLOAT = eINSTANCE.getReceptionist__AddExtraCostToRooms__int_int_String_float();

		/**
		 * The meta object literal for the '<em><b>Add Too Many Rooms Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_TOO_MANY_ROOMS_TEST = eINSTANCE.getReceptionist__AddTooManyRoomsTest();

		/**
		 * The meta object literal for the '<em><b>Check In Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_IN_TEST = eINSTANCE.getReceptionist__CheckInTest();

		/**
		 * The meta object literal for the '<em><b>Check Out Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_OUT_TEST = eINSTANCE.getReceptionist__CheckOutTest();

		/**
		 * The meta object literal for the '<em><b>List Check Ins By Day Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_INS_BY_DAY_TEST = eINSTANCE.getReceptionist__ListCheckInsByDayTest();

		/**
		 * The meta object literal for the '<em><b>List Check Outs By Day Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_OUTS_BY_DAY_TEST = eINSTANCE.getReceptionist__ListCheckOutsByDayTest();

		/**
		 * The meta object literal for the '<em><b>Edit Booking Time Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___EDIT_BOOKING_TIME_TEST = eINSTANCE.getReceptionist__EditBookingTimeTest();

		/**
		 * The meta object literal for the '<em><b>Edit Booking Rooms Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___EDIT_BOOKING_ROOMS_TEST = eINSTANCE.getReceptionist__EditBookingRoomsTest();

		/**
		 * The meta object literal for the '<em><b>Edit Booking Date And Room Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___EDIT_BOOKING_DATE_AND_ROOM_TEST = eINSTANCE.getReceptionist__EditBookingDateAndRoomTest();
	}

} //ClassDiagramPackage
