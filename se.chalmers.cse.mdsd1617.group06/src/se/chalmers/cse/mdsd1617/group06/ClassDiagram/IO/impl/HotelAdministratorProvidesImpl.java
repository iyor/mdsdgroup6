/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl#getCheckiohandler <em>Checkiohandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl#getBookinghandler <em>Bookinghandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelAdministratorProvidesImpl extends MinimalEObjectImpl.Container implements HotelAdministratorProvides {
	/**
	 * The cached value of the '{@link #getRoomhandler() <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomhandler()
	 * @generated
	 * @ordered
	 */
	protected RoomHandler roomhandler;
	/**
	 * The cached value of the '{@link #getCheckiohandler() <em>Checkiohandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckiohandler()
	 * @generated
	 * @ordered
	 */
	protected CheckIOHandler checkiohandler;

	/**
	 * The cached value of the '{@link #getBookinghandler() <em>Bookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookinghandler()
	 * @generated
	 * @ordered
	 */
	protected BookingHandler bookinghandler;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelAdministratorProvidesImpl() {
		super();
		this.roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
		this.checkiohandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		this.bookinghandler = BookingFactory.eINSTANCE.createBookingHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IOPackage.Literals.HOTEL_ADMINISTRATOR_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler getRoomhandler() {
		if (roomhandler != null && roomhandler.eIsProxy()) {
			InternalEObject oldRoomhandler = (InternalEObject)roomhandler;
			roomhandler = (RoomHandler)eResolveProxy(oldRoomhandler);
			if (roomhandler != oldRoomhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
			}
		}
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler basicGetRoomhandler() {
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomhandler(RoomHandler newRoomhandler) {
		RoomHandler oldRoomhandler = roomhandler;
		roomhandler = newRoomhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler getCheckiohandler() {
		if (checkiohandler != null && checkiohandler.eIsProxy()) {
			InternalEObject oldCheckiohandler = (InternalEObject)checkiohandler;
			checkiohandler = (CheckIOHandler)eResolveProxy(oldCheckiohandler);
			if (checkiohandler != oldCheckiohandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
			}
		}
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler basicGetCheckiohandler() {
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckiohandler(CheckIOHandler newCheckiohandler) {
		CheckIOHandler oldCheckiohandler = checkiohandler;
		checkiohandler = newCheckiohandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler getBookinghandler() {
		if (bookinghandler != null && bookinghandler.eIsProxy()) {
			InternalEObject oldBookinghandler = (InternalEObject)bookinghandler;
			bookinghandler = (BookingHandler)eResolveProxy(oldBookinghandler);
			if (bookinghandler != oldBookinghandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
			}
		}
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler basicGetBookinghandler() {
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookinghandler(BookingHandler newBookinghandler) {
		BookingHandler oldBookinghandler = bookinghandler;
		bookinghandler = newBookinghandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight) {
		return roomhandler.addRoomType(noOfBeds, roomTypeDescription, pricePerNight);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice) {
		return roomhandler.updateRoomType(oldDescription, newNoOfBeds, newDescription, newPrice);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String roomTypeDescription) {
		return roomhandler.removeRoomType(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomId) {
		roomhandler.blockRoom(roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomId) {
		roomhandler.unblockRoom(roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomId, String roomType) {
		return roomhandler.addRoom(roomId, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomId) {
		return roomhandler.removeRoom(roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeRoom(int roomId, String newRoomType) {
		return roomhandler.changeRoomTypeRoom(roomId, newRoomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		bookinghandler = BookingFactory.eINSTANCE.createBookingHandler();
		roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
		checkiohandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		checkiohandler.clearAllCheckIO();
		bookinghandler.clearAllBookings();
		roomhandler.clearAllRooms();

		roomhandler.startup(numRooms);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER:
				if (resolve) return getRoomhandler();
				return basicGetRoomhandler();
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER:
				if (resolve) return getCheckiohandler();
				return basicGetCheckiohandler();
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER:
				if (resolve) return getBookinghandler();
				return basicGetBookinghandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)newValue);
				return;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)newValue);
				return;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)null);
				return;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)null);
				return;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER:
				return roomhandler != null;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER:
				return checkiohandler != null;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER:
				return bookinghandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IHotelStartupProvides.class) {
			switch (baseOperationID) {
				case IOPackage.IHOTEL_STARTUP_PROVIDES___STARTUP__INT: return IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___CHANGE_ROOM_TYPE_ROOM__INT_STRING:
				return changeRoomTypeRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__INT_STRING_DOUBLE:
				return addRoomType((Integer)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE:
				return updateRoomType((String)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case IOPackage.HOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelAdministratorProvidesImpl
