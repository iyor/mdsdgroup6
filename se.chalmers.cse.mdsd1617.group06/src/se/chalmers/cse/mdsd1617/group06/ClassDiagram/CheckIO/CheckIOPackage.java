/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory
 * @model kind="package"
 * @generated
 */
public interface CheckIOPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CheckIO";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group06/ClassDiagram/CheckIO.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CheckIOPackage eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl <em>Catalogue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIOCatalogue()
	 * @generated
	 */
	int CHECK_IO_CATALOGUE = 0;

	/**
	 * The feature id for the '<em><b>Check Ins</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE__CHECK_INS = 0;

	/**
	 * The feature id for the '<em><b>Check Outs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE__CHECK_OUTS = 1;

	/**
	 * The number of structural features of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Clear All Check IO</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE___CLEAR_ALL_CHECK_IO = 0;

	/**
	 * The operation id for the '<em>Register Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE___REGISTER_CHECK_IN__CHECKIN = 1;

	/**
	 * The operation id for the '<em>Register Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE___REGISTER_CHECK_OUT__CHECKOUT = 2;

	/**
	 * The number of operations of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_CATALOGUE_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl <em>Check In</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIn()
	 * @generated
	 */
	int CHECK_IN = 1;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN__DATE = 0;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN__BOOKING_ID = 1;

	/**
	 * The feature id for the '<em><b>Room Ids</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN__ROOM_IDS = 2;

	/**
	 * The number of structural features of the '<em>Check In</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Check In</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckOutImpl <em>Check Out</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckOutImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckOut()
	 * @generated
	 */
	int CHECK_OUT = 2;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT__DATE = 0;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT__BOOKING_ID = 1;

	/**
	 * The feature id for the '<em><b>Room Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT__ROOM_ID = 2;

	/**
	 * The number of structural features of the '<em>Check Out</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Check Out</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl <em>Room Id To Booking Id Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getRoomIdToBookingIdEntry()
	 * @generated
	 */
	int ROOM_ID_TO_BOOKING_ID_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Room Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID = 0;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID = 1;

	/**
	 * The number of structural features of the '<em>Room Id To Booking Id Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_BOOKING_ID_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room Id To Booking Id Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_BOOKING_ID_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIOHandler()
	 * @generated
	 */
	int CHECK_IO_HANDLER = 4;

	/**
	 * The feature id for the '<em><b>Check IO Catalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__CHECK_IO_CATALOGUE = 0;

	/**
	 * The feature id for the '<em><b>Room Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__ROOM_HANDLER = 1;

	/**
	 * The feature id for the '<em><b>Booking Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__BOOKING_HANDLER = 2;

	/**
	 * The feature id for the '<em><b>Price For Initiated Checkout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT = 3;

	/**
	 * The feature id for the '<em><b>Booking Init Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT = 4;

	/**
	 * The feature id for the '<em><b>Price For Initiated Room Checkout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT = 5;

	/**
	 * The feature id for the '<em><b>Room Init Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT = 6;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___INITIATE_CHECK_IN__INT = 0;

	/**
	 * The operation id for the '<em>Check In Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_ID__INT_INT = 1;

	/**
	 * The operation id for the '<em>Check In Room By Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_TYPE__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 3;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 4;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___INITIATE_CHECKOUT__INT = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Clear All Check IO</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CLEAR_ALL_CHECK_IO = 7;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___LIST_OCCUPIED_ROOMS__DATE = 8;

	/**
	 * The operation id for the '<em>Check IO Catalogue</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CHECK_IO_CATALOGUE = 9;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___LIST_CHECK_INS__DATE = 10;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___LIST_CHECK_INS__DATE_DATE = 11;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE = 12;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE_DATE = 13;

	/**
	 * The operation id for the '<em>Check In Reg</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CHECK_IN_REG__ELIST_INT = 14;

	/**
	 * The operation id for the '<em>Check Out Reg</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___CHECK_OUT_REG__INT_INT = 15;

	/**
	 * The operation id for the '<em>Are On Same Day</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER___ARE_ON_SAME_DAY__DATE_DATE = 16;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IO_HANDLER_OPERATION_COUNT = 17;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue <em>Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Catalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue
	 * @generated
	 */
	EClass getCheckIOCatalogue();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckIns <em>Check Ins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Check Ins</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckIns()
	 * @see #getCheckIOCatalogue()
	 * @generated
	 */
	EReference getCheckIOCatalogue_CheckIns();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckOuts <em>Check Outs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Check Outs</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckOuts()
	 * @see #getCheckIOCatalogue()
	 * @generated
	 */
	EReference getCheckIOCatalogue_CheckOuts();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#clearAllCheckIO() <em>Clear All Check IO</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Check IO</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#clearAllCheckIO()
	 * @generated
	 */
	EOperation getCheckIOCatalogue__ClearAllCheckIO();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#registerCheckIn(se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn) <em>Register Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Register Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#registerCheckIn(se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn)
	 * @generated
	 */
	EOperation getCheckIOCatalogue__RegisterCheckIn__CheckIn();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#registerCheckOut(se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut) <em>Register Check Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Register Check Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#registerCheckOut(se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut)
	 * @generated
	 */
	EOperation getCheckIOCatalogue__RegisterCheckOut__CheckOut();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn <em>Check In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check In</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn
	 * @generated
	 */
	EClass getCheckIn();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getDate()
	 * @see #getCheckIn()
	 * @generated
	 */
	EAttribute getCheckIn_Date();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getBookingId()
	 * @see #getCheckIn()
	 * @generated
	 */
	EAttribute getCheckIn_BookingId();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getRoomIds <em>Room Ids</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Room Ids</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn#getRoomIds()
	 * @see #getCheckIn()
	 * @generated
	 */
	EAttribute getCheckIn_RoomIds();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut <em>Check Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut
	 * @generated
	 */
	EClass getCheckOut();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getDate()
	 * @see #getCheckOut()
	 * @generated
	 */
	EAttribute getCheckOut_Date();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getBookingId()
	 * @see #getCheckOut()
	 * @generated
	 */
	EAttribute getCheckOut_BookingId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getRoomId <em>Room Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut#getRoomId()
	 * @see #getCheckOut()
	 * @generated
	 */
	EAttribute getCheckOut_RoomId();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry <em>Room Id To Booking Id Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Id To Booking Id Entry</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry
	 * @generated
	 */
	EClass getRoomIdToBookingIdEntry();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry#getRoomId <em>Room Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry#getRoomId()
	 * @see #getRoomIdToBookingIdEntry()
	 * @generated
	 */
	EAttribute getRoomIdToBookingIdEntry_RoomId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry#getBookingId()
	 * @see #getRoomIdToBookingIdEntry()
	 * @generated
	 */
	EAttribute getRoomIdToBookingIdEntry_BookingId();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler
	 * @generated
	 */
	EClass getCheckIOHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getCheckIOCatalogue <em>Check IO Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Check IO Catalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getCheckIOCatalogue()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EReference getCheckIOHandler_CheckIOCatalogue();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomHandler <em>Room Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomHandler()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EReference getCheckIOHandler_RoomHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingHandler <em>Booking Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Booking Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingHandler()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EReference getCheckIOHandler_BookingHandler();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedCheckout <em>Price For Initiated Checkout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price For Initiated Checkout</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedCheckout()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EAttribute getCheckIOHandler_PriceForInitiatedCheckout();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingInitCheckout <em>Booking Init Checkout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Booking Init Checkout</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingInitCheckout()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EReference getCheckIOHandler_BookingInitCheckout();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedRoomCheckout <em>Price For Initiated Room Checkout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price For Initiated Room Checkout</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedRoomCheckout()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EAttribute getCheckIOHandler_PriceForInitiatedRoomCheckout();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomInitCheckout <em>Room Init Checkout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Init Checkout</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomInitCheckout()
	 * @see #getCheckIOHandler()
	 * @generated
	 */
	EReference getCheckIOHandler_RoomInitCheckout();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateCheckIn(int) <em>Initiate Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateCheckIn(int)
	 * @generated
	 */
	EOperation getCheckIOHandler__InitiateCheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInRoomById(int, int) <em>Check In Room By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room By Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInRoomById(int, int)
	 * @generated
	 */
	EOperation getCheckIOHandler__CheckInRoomById__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInRoomByType(int, java.lang.String) <em>Check In Room By Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room By Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInRoomByType(int, java.lang.String)
	 * @generated
	 */
	EOperation getCheckIOHandler__CheckInRoomByType__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getCheckIOHandler__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getCheckIOHandler__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateCheckout(int)
	 * @generated
	 */
	EOperation getCheckIOHandler__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getCheckIOHandler__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#clearAllCheckIO() <em>Clear All Check IO</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Check IO</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#clearAllCheckIO()
	 * @generated
	 */
	EOperation getCheckIOHandler__ClearAllCheckIO();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listOccupiedRooms(java.util.Date) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__ListOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkIOCatalogue() <em>Check IO Catalogue</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check IO Catalogue</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkIOCatalogue()
	 * @generated
	 */
	EOperation getCheckIOHandler__CheckIOCatalogue();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckIns(java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckIns(java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__ListCheckIns__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckOuts(java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckOuts(java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__ListCheckOuts__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInReg(org.eclipse.emf.common.util.EList, int) <em>Check In Reg</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Reg</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkInReg(org.eclipse.emf.common.util.EList, int)
	 * @generated
	 */
	EOperation getCheckIOHandler__CheckInReg__EList_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkOutReg(int, int) <em>Check Out Reg</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Reg</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#checkOutReg(int, int)
	 * @generated
	 */
	EOperation getCheckIOHandler__CheckOutReg__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#areOnSameDay(java.util.Date, java.util.Date) <em>Are On Same Day</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Are On Same Day</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#areOnSameDay(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getCheckIOHandler__AreOnSameDay__Date_Date();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CheckIOFactory getCheckIOFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl <em>Catalogue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOCatalogueImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIOCatalogue()
		 * @generated
		 */
		EClass CHECK_IO_CATALOGUE = eINSTANCE.getCheckIOCatalogue();
		/**
		 * The meta object literal for the '<em><b>Check Ins</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_CATALOGUE__CHECK_INS = eINSTANCE.getCheckIOCatalogue_CheckIns();
		/**
		 * The meta object literal for the '<em><b>Check Outs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_CATALOGUE__CHECK_OUTS = eINSTANCE.getCheckIOCatalogue_CheckOuts();
		/**
		 * The meta object literal for the '<em><b>Clear All Check IO</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_CATALOGUE___CLEAR_ALL_CHECK_IO = eINSTANCE.getCheckIOCatalogue__ClearAllCheckIO();
		/**
		 * The meta object literal for the '<em><b>Register Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_CATALOGUE___REGISTER_CHECK_IN__CHECKIN = eINSTANCE.getCheckIOCatalogue__RegisterCheckIn__CheckIn();
		/**
		 * The meta object literal for the '<em><b>Register Check Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_CATALOGUE___REGISTER_CHECK_OUT__CHECKOUT = eINSTANCE.getCheckIOCatalogue__RegisterCheckOut__CheckOut();
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl <em>Check In</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIn()
		 * @generated
		 */
		EClass CHECK_IN = eINSTANCE.getCheckIn();
		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN__DATE = eINSTANCE.getCheckIn_Date();
		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN__BOOKING_ID = eINSTANCE.getCheckIn_BookingId();
		/**
		 * The meta object literal for the '<em><b>Room Ids</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN__ROOM_IDS = eINSTANCE.getCheckIn_RoomIds();
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckOutImpl <em>Check Out</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckOutImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckOut()
		 * @generated
		 */
		EClass CHECK_OUT = eINSTANCE.getCheckOut();
		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT__DATE = eINSTANCE.getCheckOut_Date();
		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT__BOOKING_ID = eINSTANCE.getCheckOut_BookingId();
		/**
		 * The meta object literal for the '<em><b>Room Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT__ROOM_ID = eINSTANCE.getCheckOut_RoomId();
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl <em>Room Id To Booking Id Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getRoomIdToBookingIdEntry()
		 * @generated
		 */
		EClass ROOM_ID_TO_BOOKING_ID_ENTRY = eINSTANCE.getRoomIdToBookingIdEntry();
		/**
		 * The meta object literal for the '<em><b>Room Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID = eINSTANCE.getRoomIdToBookingIdEntry_RoomId();
		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID = eINSTANCE.getRoomIdToBookingIdEntry_BookingId();
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl#getCheckIOHandler()
		 * @generated
		 */
		EClass CHECK_IO_HANDLER = eINSTANCE.getCheckIOHandler();
		/**
		 * The meta object literal for the '<em><b>Check IO Catalogue</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_HANDLER__CHECK_IO_CATALOGUE = eINSTANCE.getCheckIOHandler_CheckIOCatalogue();
		/**
		 * The meta object literal for the '<em><b>Room Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_HANDLER__ROOM_HANDLER = eINSTANCE.getCheckIOHandler_RoomHandler();
		/**
		 * The meta object literal for the '<em><b>Booking Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_HANDLER__BOOKING_HANDLER = eINSTANCE.getCheckIOHandler_BookingHandler();
		/**
		 * The meta object literal for the '<em><b>Price For Initiated Checkout</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT = eINSTANCE.getCheckIOHandler_PriceForInitiatedCheckout();
		/**
		 * The meta object literal for the '<em><b>Booking Init Checkout</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT = eINSTANCE.getCheckIOHandler_BookingInitCheckout();
		/**
		 * The meta object literal for the '<em><b>Price For Initiated Room Checkout</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT = eINSTANCE.getCheckIOHandler_PriceForInitiatedRoomCheckout();
		/**
		 * The meta object literal for the '<em><b>Room Init Checkout</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT = eINSTANCE.getCheckIOHandler_RoomInitCheckout();
		/**
		 * The meta object literal for the '<em><b>Initiate Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___INITIATE_CHECK_IN__INT = eINSTANCE.getCheckIOHandler__InitiateCheckIn__int();
		/**
		 * The meta object literal for the '<em><b>Check In Room By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_ID__INT_INT = eINSTANCE.getCheckIOHandler__CheckInRoomById__int_int();
		/**
		 * The meta object literal for the '<em><b>Check In Room By Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_TYPE__INT_STRING = eINSTANCE.getCheckIOHandler__CheckInRoomByType__int_String();
		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getCheckIOHandler__PayRoomDuringCheckout__int_String_String_int_int_String_String();
		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getCheckIOHandler__PayDuringCheckout__String_String_int_int_String_String();
		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___INITIATE_CHECKOUT__INT = eINSTANCE.getCheckIOHandler__InitiateCheckout__int();
		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getCheckIOHandler__InitiateRoomCheckout__int_int();
		/**
		 * The meta object literal for the '<em><b>Clear All Check IO</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CLEAR_ALL_CHECK_IO = eINSTANCE.getCheckIOHandler__ClearAllCheckIO();
		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___LIST_OCCUPIED_ROOMS__DATE = eINSTANCE.getCheckIOHandler__ListOccupiedRooms__Date();
		/**
		 * The meta object literal for the '<em><b>Check IO Catalogue</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CHECK_IO_CATALOGUE = eINSTANCE.getCheckIOHandler__CheckIOCatalogue();
		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___LIST_CHECK_INS__DATE = eINSTANCE.getCheckIOHandler__ListCheckIns__Date();
		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getCheckIOHandler__ListCheckIns__Date_Date();
		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE = eINSTANCE.getCheckIOHandler__ListCheckOuts__Date();
		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getCheckIOHandler__ListCheckOuts__Date_Date();
		/**
		 * The meta object literal for the '<em><b>Check In Reg</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CHECK_IN_REG__ELIST_INT = eINSTANCE.getCheckIOHandler__CheckInReg__EList_int();
		/**
		 * The meta object literal for the '<em><b>Check Out Reg</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___CHECK_OUT_REG__INT_INT = eINSTANCE.getCheckIOHandler__CheckOutReg__int_int();
		/**
		 * The meta object literal for the '<em><b>Are On Same Day</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHECK_IO_HANDLER___ARE_ON_SAME_DAY__DATE_DATE = eINSTANCE.getCheckIOHandler__AreOnSameDay__Date_Date();

	}

} //CheckIOPackage
