/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Catalogue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckIns <em>Check Ins</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue#getCheckOuts <em>Check Outs</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOCatalogue()
 * @model
 * @generated
 */
public interface CheckIOCatalogue extends EObject {
	/**
	 * Returns the value of the '<em><b>Check Ins</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Ins</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Ins</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOCatalogue_CheckIns()
	 * @model ordered="false"
	 * @generated
	 */
	EList<CheckIn> getCheckIns();

	/**
	 * Returns the value of the '<em><b>Check Outs</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Outs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Outs</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOCatalogue_CheckOuts()
	 * @model ordered="false"
	 * @generated
	 */
	EList<CheckOut> getCheckOuts();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearAllCheckIO();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model checkInRequired="true" checkInOrdered="false"
	 * @generated
	 */
	void registerCheckIn(CheckIn checkIn);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model checkOutRequired="true" checkOutOrdered="false"
	 * @generated
	 */
	void registerCheckOut(CheckOut checkOut);

} // CheckIOCatalogue
