/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomHandlerImpl#getRoomcatalogue <em>Roomcatalogue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomHandlerImpl extends MinimalEObjectImpl.Container implements RoomHandler {
	/**
	 * The cached value of the '{@link #getRoomcatalogue() <em>Roomcatalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomcatalogue()
	 * @generated
	 * @ordered
	 */
	protected RoomCatalogue roomcatalogue;
	/**
	 * The cached value of the '{@link #getRoomCatalogue() <em>Room Catalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * Since we want the catalogue to be a Singleton, we fake this by making it static in the 
	 * only class that accesses it, the RoomHandler. In here, we make sure every access to 
	 * it is via the correct method.
	 * <!-- end-user-doc -->
	 * @see #getRoomCatalogue()
	 * @generated NOT
	 * @ordered
	 */
	protected static RoomCatalogue roomCatalogue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomHandlerImpl() {
		super();
		if(this.roomCatalogue==null)
			roomCatalogue = new RoomCatalogueImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomCatalogue getRoomcatalogue() {
		if (roomcatalogue != null && roomcatalogue.eIsProxy()) {
			InternalEObject oldRoomcatalogue = (InternalEObject)roomcatalogue;
			roomcatalogue = (RoomCatalogue)eResolveProxy(oldRoomcatalogue);
			if (roomcatalogue != oldRoomcatalogue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM_HANDLER__ROOMCATALOGUE, oldRoomcatalogue, roomcatalogue));
			}
		}
		return roomcatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomCatalogue basicGetRoomcatalogue() {
		return roomcatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomcatalogue(RoomCatalogue newRoomcatalogue) {
		RoomCatalogue oldRoomcatalogue = roomcatalogue;
		roomcatalogue = newRoomcatalogue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_HANDLER__ROOMCATALOGUE, oldRoomcatalogue, roomcatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight) {
		return roomcatalogue().addRoomType(noOfBeds, roomTypeDescription, pricePerNight);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice) {
		return roomcatalogue().updateRoomType(oldDescription, newNoOfBeds, newDescription, newPrice);
	}

	/**
	 * <!-- begin-user-doc -->
	 * By Hjort: Remove room type based on name only
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String roomTypeDescription) {
		return roomcatalogue().removeRoomType(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNbrOfRooms(String roomType) {
		return roomcatalogue().getRoomsOfType(roomType).size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomId) {
		this.blockUnblock(roomId, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomId) {
		this.blockUnblock(roomId, false);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomId, String roomType) {
		//The room number is incorrect or the room type is not specified
		if( roomId < 0 || roomType == null ){
			return false;
		}else{
			return this.roomcatalogue().addRoom(roomId, roomType);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getLstRoomType() {
		return roomcatalogue().getRoomTypeList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomId) {
		Room room = this.roomcatalogue().getRoomById(roomId);
		if(  room != null ){
			return this.roomcatalogue().removeRoom(room);
		}else{
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeRoom(int roomId, String newRoomType) {
		return this.roomcatalogue().changeRoomTypeRoom(newRoomType, roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Room getRoomById(int roomId) {
		return roomcatalogue().getRoomById(roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getRoomsOfType(String roomTypeDescription) {
		return roomcatalogue().getRoomsOfType(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockUnblock(int roomId, boolean block) {
		Room r = this.roomcatalogue().getRoomById(roomId);
		if (r != null) r.setBlocked(block);
		
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMCATALOGUE:
				if (resolve) return getRoomcatalogue();
				return basicGetRoomcatalogue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMCATALOGUE:
				setRoomcatalogue((RoomCatalogue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMCATALOGUE:
				setRoomcatalogue((RoomCatalogue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMCATALOGUE:
				return roomcatalogue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_HANDLER___ADD_ROOM_TYPE__INT_STRING_DOUBLE:
				return addRoomType((Integer)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2));
			case RoomPackage.ROOM_HANDLER___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE:
				return updateRoomType((String)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case RoomPackage.ROOM_HANDLER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___GET_NBR_OF_ROOMS__STRING:
				return getNbrOfRooms((String)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___CLEAR_ALL_ROOMS:
				clearAllRooms();
				return null;
			case RoomPackage.ROOM_HANDLER___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___ROOMCATALOGUE:
				return roomcatalogue();
			case RoomPackage.ROOM_HANDLER___ADD_EXTRA__INT_STRING_FLOAT:
				addExtra((Integer)arguments.get(0), (String)arguments.get(1), (Float)arguments.get(2));
				return null;
			case RoomPackage.ROOM_HANDLER___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT:
				return getAllRoomTypesWithMinNumberOfBeds((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackage.ROOM_HANDLER___GET_LST_ROOM_TYPE:
				return getLstRoomType();
			case RoomPackage.ROOM_HANDLER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___CHANGE_ROOM_TYPE_ROOM__INT_STRING:
				return changeRoomTypeRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackage.ROOM_HANDLER___GET_ROOM_BY_ID__INT:
				return getRoomById((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___GET_ROOMS_OF_TYPE__STRING:
				return getRoomsOfType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllRooms() {
		roomCatalogue.clearAllRooms();

	}


	/**
	 * <!-- begin-user-doc -->
	 * Creates new rooms (doesn't clean anything up) of the default type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		roomcatalogue().addRoomType(2, "default", 1000.0);
		for(int i = 0; i < numRooms; i++){
			roomcatalogue().addRoom(i+1, "default");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Pretend the room catalogue is a Singleton by always accessing it via this method.
	 * Keep in mind, roomCatalogue is static.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomCatalogue roomcatalogue() {
		if (roomCatalogue == null) {
			roomCatalogue = RoomFactory.eINSTANCE.createRoomCatalogue();
		}
		return roomCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(int roomNumber, String description, float price) {
		roomcatalogue().addExtra(roomNumber, description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public EList<RoomType> getAllRoomTypesWithMinNumberOfBeds(int minNumBeds) {
		return roomcatalogue().getAllRoomTypesWithMinNumberOfBeds(minNumBeds);
	}

} //RoomHandlerImpl
