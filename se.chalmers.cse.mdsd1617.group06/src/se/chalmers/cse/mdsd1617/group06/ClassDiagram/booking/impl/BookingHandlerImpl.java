/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Handler</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl#getBookingcatalogue <em>Bookingcatalogue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingHandlerImpl extends MinimalEObjectImpl.Container implements BookingHandler {
	/**
	 * The cached value of the '{@link #getRoomhandler() <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRoomhandler()
	 * @generated
	 * @ordered
	 */
	protected RoomHandler roomhandler;

	/**
	 * The cached value of the '{@link #getBookingcatalogue()
	 * <em>Bookingcatalogue</em>}' containment reference. <!-- begin-user-doc
	 * --> Singleton <!-- end-user-doc -->
	 * 
	 * @see #getBookingcatalogue()
	 * @generated NOT
	 * @ordered
	 */
	protected static BookingCatalogue bookingcatalogue;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected BookingHandlerImpl() {
		super();
		this.roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_HANDLER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public BookingCatalogue getBookingcatalogue() {
		if (bookingcatalogue != null && bookingcatalogue.eIsProxy()) {
			InternalEObject oldBookingcatalogue = (InternalEObject)bookingcatalogue;
			bookingcatalogue = (BookingCatalogue)eResolveProxy(oldBookingcatalogue);
			if (bookingcatalogue != oldBookingcatalogue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE, oldBookingcatalogue, bookingcatalogue));
			}
		}
		return bookingcatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingCatalogue basicGetBookingcatalogue() {
		return bookingcatalogue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingcatalogue(BookingCatalogue newBookingcatalogue) {
		BookingCatalogue oldBookingcatalogue = bookingcatalogue;
		bookingcatalogue = newBookingcatalogue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE, oldBookingcatalogue, bookingcatalogue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler getRoomhandler() {
		if (roomhandler != null && roomhandler.eIsProxy()) {
			InternalEObject oldRoomhandler = (InternalEObject)roomhandler;
			roomhandler = (RoomHandler)eResolveProxy(oldRoomhandler);
			if (roomhandler != oldRoomhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_HANDLER__ROOMHANDLER, oldRoomhandler, roomhandler));
			}
		}
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler basicGetRoomhandler() {
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomhandler(RoomHandler newRoomhandler) {
		RoomHandler oldRoomhandler = roomhandler;
		roomhandler = newRoomhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_HANDLER__ROOMHANDLER, oldRoomhandler, roomhandler));
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.1. (CH)
	 * This method is used to create a new booking. On success, it returns the bookingID, on failure, it will return -1.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, Date startDate, Date endDate) {
		// Check specified possible errors
		if (firstName == null || lastName == null || startDate == null || endDate == null || endDate.before(startDate))
			return -1;

		// Set given information for booking
		Booking bk = BookingFactoryImpl.init().createBooking();
		bk.setFirstName(firstName);
		bk.setLastName(lastName);
		bk.setStartDate(startDate);
		bk.setEndDate(endDate);
		bk.setBookingId(bookingcatalogue().getNewBookingId());
		bk.setBookingStatus(BookingStatus.UNCONFIRMED);
		bk.setIsPaid(false);

		// Register booking in bookingcatalogue
		bookingcatalogue().addBooking(bk);

		return bk.getBookingId();
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.1. (CH)
	 * This method will add a room to a specified booking if a room of the specified type is available during the booked time span.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking booking = bookingcatalogue().getBooking(bookingID);

		// Make sure booking exists
		if (booking == null || booking.getBookingStatus() != BookingStatus.UNCONFIRMED) {
			return false;
		}

		// Check that roomType exists and contains free rooms
		for (RoomType rT : roomhandler.getLstRoomType()) {
			if (rT.getRoomTypeDescription().equals(roomTypeDescription)) {
				
				// Make sure there is a room available during booked period
				int nbrRooms = getFreeRoomTypesForPeriod(roomTypeDescription, booking.getStartDate(),
						booking.getEndDate());
				if (nbrRooms > 0) {
					booking.addRoomsOfRoomType(rT, 1);
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.1. (CH)
	 * This method confirms a booking. The method will make sure that the bookingID exists and the booking has at least one room associated with it.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		// Make sure bookingID is valid
		if (bookingID > bookingcatalogue.getCurrentBookingId() || bookingID < 1) {
			return false;
		}
		
		// Make sure booking exists and is unconfirmed
		Booking bk = bookingcatalogue.getBooking(bookingID);
		if (bk == null || bk.getBookingStatus() != BookingStatus.UNCONFIRMED) {
			return false;
		}
		
		// Check if rooms are added
		int roomsAdded = 0;
		for (RoomTypeToNumberEntry entry : bk.getRoomMap()) {
			roomsAdded = +(int) entry.getValue();
		}
		if (roomsAdded > 0) {
			bk.setBookingStatus(BookingStatus.CONFIRMED);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void addExtra(int bookingId, int roomNumber, String description, float price) {
		EList<Room> roomsAssociated = bookingcatalogue().getBooking(bookingId).getRoomsAssociated();

		for (Room roomInList : roomsAssociated) {
			if (roomInList.getId() == roomNumber) {
				roomhandler.addExtra(roomNumber, description, price);
				return;
			}
		}
	}

	/**
	 * Returns a new list of bookings that are confirmed
	 * 
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		BasicEList<Booking> temp = new BasicEList<Booking>();

		for (Booking b : getBookingcatalogue().getBookings()) {
			if (b.getBookingStatus() == BookingStatus.CONFIRMED)
				temp.add(b);
		}

		return temp;
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.5. (CH)
	 * This method changes the types of rooms and the number of rooms associated with a booking. It makes sure that there is enough rooms available for the selected RoomTypes. In case an edit is not possible, the current state will be kept.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, EList<RoomTypeToNumberEntry> roomTypesandCount) {
		// Make sure booking exists and booking is not yet checked in
		Booking bk = this.bookingcatalogue().getBooking(bookingID);
		if (bk == null || !(bk.getBookingStatus() == BookingStatus.CONFIRMED
				|| bk.getBookingStatus() == BookingStatus.UNCONFIRMED)) {
			return false;
		}
		
		// Save current booking state in case there is not enough free rooms
		EList<RoomTypeToNumberEntry> oldRooms = bk.getRoomMap();
		
		// Reset Rooms associated
		bk.replaceRoomMap(new BasicEList<RoomTypeToNumberEntry>());
		boolean noFailure = true;
		
		// Iterate through RoomTypes and try to generate rooms
		for (int i = 0; i < oldRooms.size(); i++) {
			RoomTypeToNumberEntry entry = roomTypesandCount.get(i);
			for (int j = 0; j < entry.getValue(); j++) {
				// Break loop and stop method if not enough rooms are available
				if (!addRoomToBooking(entry.getKey().getRoomTypeDescription(), bookingID)) {
					noFailure = false;
					break;
				}
			}
		}
		if (noFailure) {
			// Reset status to unconfirmed if no room is included anymore
			int roomsAdded = 0;
			for (RoomTypeToNumberEntry entry : bk.getRoomMap()) {
				roomsAdded = +(int) entry.getValue();
			}
			if (roomsAdded < 1) {
				bk.setBookingStatus(BookingStatus.UNCONFIRMED);
			}
			return true;
		} else {
			// Restore former state if edit is impossible
			bk.replaceRoomMap(oldRooms);
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.5. (CH)
	 * This method changes the booking dates. It makes sure that there is enough rooms available for the selected RoomTypes during the specified time period. In case an edit is not possible, the current state will be kept.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, Date startDate, Date endDate) {
		// Make sure booking exists and booking is not yet checked in
		Booking bk = bookingcatalogue.getBooking(bookingID);
		if (bk == null || !(bk.getBookingStatus() == BookingStatus.CONFIRMED
				|| bk.getBookingStatus() == BookingStatus.UNCONFIRMED)) {
			return false;
		}
		// Check if new dates are contained within old ones and change booking if it is the case
		if (bk.getStartDate().compareTo(startDate) <= 0 && bk.getEndDate().compareTo(endDate) >= 0) {
			bk.setStartDate(startDate);
			bk.setEndDate(endDate);
			return true;
		}
		
		// Save current booking state in case there is not enough free rooms
		EList<RoomTypeToNumberEntry> oldRooms = bk.getRoomMap();
		Date oldStartDate = bk.getStartDate();
		Date oldEndDate = bk.getEndDate();
		
		// Reset Rooms associated and booking dates
		bk.replaceRoomMap(new BasicEList<RoomTypeToNumberEntry>());
		bk.setStartDate(startDate);
		bk.setEndDate(endDate);
		boolean noFailure = true;
		
		// Iterate through RoomTypes and try to associate rooms
		for (int i = 0; i < oldRooms.size(); i++) {
			RoomTypeToNumberEntry entry = oldRooms.get(i);
			// Break loop and stop method if not enough rooms are available
			for (int j = 0; j < entry.getValue(); j++) {
				if (!addRoomToBooking(entry.getKey().getRoomTypeDescription(), bookingID)) {
					noFailure = false;
					break;
				}
			}
		}
		if (noFailure) {
			return true;
		} else {
			// Restore former state if edit is impossible
			Booking book = bookingcatalogue.getBooking(bookingID);
			book.replaceRoomMap(oldRooms);
			book.setStartDate(oldStartDate);
			book.setEndDate(oldEndDate);
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * UC 2.1.5. (CH)
	 * This method changes the types of rooms and the number of rooms associated with a booking and its dates. It makes sure that there is enough rooms available for the selected RoomTypes during the specified time period. In case an edit is not possible, the current state will be kept.
	 *  <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, Date startDate, Date endDate,
			EList<RoomTypeToNumberEntry> roomTypesandCount) {
		// Make sure booking exists and booking is not yet checked in
		Booking bk = bookingcatalogue.getBooking(bookingID);
		if (bk == null || !(bk.getBookingStatus() == BookingStatus.CONFIRMED
				|| bk.getBookingStatus() == BookingStatus.UNCONFIRMED) || endDate.before(startDate) || endDate.equals(startDate)) {
			return false;
		}
		
		// Save current booking state in case there is not enough free rooms
		EList<RoomTypeToNumberEntry> oldRooms = bk.getRoomMap();
		Date oldStartDate = bk.getStartDate();
		Date oldEndDate = bk.getEndDate();
		
		// Reset Rooms associated and booking dates
		bk.replaceRoomMap(new BasicEList<RoomTypeToNumberEntry>());
		bk.setStartDate(startDate);
		bk.setEndDate(endDate);
		
		// Iterate through RoomTypes and try to associate rooms
		boolean noFailure = true;
		for (int i = 0; i < roomTypesandCount.size(); i++) {
			RoomTypeToNumberEntry entry = roomTypesandCount.get(i);
			// Break loop and stop method if not enough rooms are available
			for (int j = 0; j < entry.getValue(); j++) {
				if (!addRoomToBooking(entry.getKey().getRoomTypeDescription(), bookingID)) {
					noFailure = false;
					break;
				}
			}
		}
		if (noFailure) {
			return true;
		} else {
			// Restore former state if edit is impossible
			Booking book = bookingcatalogue.getBooking(bookingID);
			book.replaceRoomMap(oldRooms);
			book.setStartDate(oldStartDate);
			book.setEndDate(oldEndDate);
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.6. (CH)
	 * This method cancels confirmed and unconfirmed bookings and sets all associated rooms free.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingId) {
		// Make sure booking exists and is confirmed or unconfirmed
		Booking bk = bookingcatalogue.getBooking(bookingId);
		if (bk != null && (bk.getBookingStatus() == BookingStatus.CONFIRMED
				|| bk.getBookingStatus() == BookingStatus.UNCONFIRMED)) {
			// Clear associated rooms and set new bookingstatus
			bk.getRoomMap().clear();
			bk.setBookingStatus(BookingStatus.CANCELED);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.2.9. (CH)
	 * This method forwards a reset call in order to clear all bookings and create a clean startup
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void clearAllBookings() {
		bookingcatalogue().clearAllBookings();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public BookingCatalogue bookingcatalogue() {
		if (bookingcatalogue == null) {
			bookingcatalogue = BookingFactory.eINSTANCE.createBookingCatalogue();
		}
		return bookingcatalogue;
	}

	/**
	 * Returns a list of room types that fulfills the requirements <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, Date startDate, Date endDate) {
		EList<RoomType> roomTypesList = roomhandler.getAllRoomTypesWithMinNumberOfBeds(numBeds);
		Map<RoomType, Integer> nbrRoomsinRTMap = new HashMap<>();

		for (RoomType rt : roomTypesList) {
			nbrRoomsinRTMap.put(rt, roomhandler.getNbrOfRooms(rt.getRoomTypeDescription()));
		}

		EList<Booking> allBookingsPeriod = bookingcatalogue().getBookingsForPeriod(startDate, endDate);
		for (Booking booking : allBookingsPeriod) {
			for (RoomTypeToNumberEntry rtnEntry : booking.getRoomMap()) {
				RoomType roomtype = rtnEntry.getKey();
				if (nbrRoomsinRTMap.containsKey(roomtype)) {
					int nbrRooms = nbrRoomsinRTMap.get(roomtype);
					int nbrBookedRooms = rtnEntry.getValue();
					nbrRoomsinRTMap.replace(roomtype, nbrRooms - nbrBookedRooms);
				}
			}
		}

		EList<FreeRoomTypesDTO> frtList = new BasicEList<FreeRoomTypesDTO>();
		for (Map.Entry<RoomType, Integer> entry : nbrRoomsinRTMap.entrySet()) {
			if(entry.getValue()>0){
				FreeRoomTypesDTO frtElement = IOFactory.eINSTANCE.createFreeRoomTypesDTO();
				RoomType rtInMap = entry.getKey();
				frtElement.setNumBeds(rtInMap.getNoOfBeds());
				frtElement.setNumFreeRooms(entry.getValue());
				frtElement.setPricePerNight(rtInMap.getPricePerNight());
				frtElement.setRoomTypeDescription(rtInMap.getRoomTypeDescription());
				frtList.add(frtElement);
			}
		}

		return frtList;
	}

	/**
	 * <!-- begin-user-doc --> 
	 * UC 2.1.1
	 * This method returns the number of rooms of a specified roomType that are available during the specified time span.
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getFreeRoomTypesForPeriod(String roomType, Date startDate, Date endDate) {
		int total = this.roomhandler.getNbrOfRooms(roomType);
		for (Booking b : bookingcatalogue().getBookings()) {
			// Make sure booking is not canceled or checked out
			if (b.getBookingStatus() == BookingStatus.CANCELED || b.getBookingStatus() == BookingStatus.CHECKED_OUT)
				continue;

			// Check if the date overlaps with the specified one, and skip this
			// booking if it doesn't.
			if (b.getEndDate().before(startDate) || b.getEndDate().equals(startDate) || b.getStartDate().after(endDate) || b.getStartDate().equals(endDate))
				continue;

			// Find if this booking books any room of the same type, and deduct
			// the number of rooms the have booked.
			for (RoomTypeToNumberEntry e : b.getRoomMap()) {
				if (e.getKey().getRoomTypeDescription().equals(roomType)) {
					total -= e.getValue();
				}
			}
		}
		return total;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBooking(int bookingId) {
		return bookingcatalogue().getBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__ROOMHANDLER:
				if (resolve) return getRoomhandler();
				return basicGetRoomhandler();
			case BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE:
				if (resolve) return getBookingcatalogue();
				return basicGetBookingcatalogue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__ROOMHANDLER:
				setRoomhandler((RoomHandler)newValue);
				return;
			case BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE:
				setBookingcatalogue((BookingCatalogue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__ROOMHANDLER:
				setRoomhandler((RoomHandler)null);
				return;
			case BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE:
				setBookingcatalogue((BookingCatalogue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__ROOMHANDLER:
				return roomhandler != null;
			case BookingPackage.BOOKING_HANDLER__BOOKINGCATALOGUE:
				return bookingcatalogue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_DATE_DATE:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3));
			case BookingPackage.BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___ADD_EXTRA__INT_INT_STRING_FLOAT:
				addExtra((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Float)arguments.get(3));
				return null;
			case BookingPackage.BOOKING_HANDLER___LIST_BOOKINGS:
				return listBookings();
			case BookingPackage.BOOKING_HANDLER___EDIT_BOOKING__INT_ELIST:
				return editBooking((Integer)arguments.get(0), (EList<RoomTypeToNumberEntry>)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE:
				return editBooking((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case BookingPackage.BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE_ELIST:
				return editBooking((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2), (EList<RoomTypeToNumberEntry>)arguments.get(3));
			case BookingPackage.BOOKING_HANDLER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___CLEAR_ALL_BOOKINGS:
				clearAllBookings();
				return null;
			case BookingPackage.BOOKING_HANDLER___BOOKINGCATALOGUE:
				return bookingcatalogue();
			case BookingPackage.BOOKING_HANDLER___GET_FREE_ROOMS__INT_DATE_DATE:
				return getFreeRooms((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case BookingPackage.BOOKING_HANDLER___GET_FREE_ROOM_TYPES_FOR_PERIOD__STRING_DATE_DATE:
				return getFreeRoomTypesForPeriod((String)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case BookingPackage.BOOKING_HANDLER___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} // BookingHandlerImpl
