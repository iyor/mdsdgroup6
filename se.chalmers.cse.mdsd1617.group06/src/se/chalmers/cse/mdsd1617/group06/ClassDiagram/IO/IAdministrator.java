/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IAdministrator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getIAdministrator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IAdministrator extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" noOfBedsRequired="true" noOfBedsOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" pricePerNightRequired="true" pricePerNightOrdered="false"
	 * @generated
	 */
	boolean addRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldDescriptionRequired="true" oldDescriptionOrdered="false" newNoOfBedsRequired="true" newNoOfBedsOrdered="false" newDescriptionRequired="true" newDescriptionOrdered="false" newPriceRequired="true" newPriceOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	boolean addRoom(int roomId, String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeRoom(int roomId, String newRoomType);
} // IAdministrator
