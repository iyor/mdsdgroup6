/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CheckIOFactoryImpl extends EFactoryImpl implements CheckIOFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CheckIOFactory init() {
		try {
			CheckIOFactory theCheckIOFactory = (CheckIOFactory)EPackage.Registry.INSTANCE.getEFactory(CheckIOPackage.eNS_URI);
			if (theCheckIOFactory != null) {
				return theCheckIOFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CheckIOFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CheckIOPackage.CHECK_IO_CATALOGUE: return createCheckIOCatalogue();
			case CheckIOPackage.CHECK_IN: return createCheckIn();
			case CheckIOPackage.CHECK_OUT: return createCheckOut();
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY: return createRoomIdToBookingIdEntry();
			case CheckIOPackage.CHECK_IO_HANDLER: return createCheckIOHandler();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOCatalogue createCheckIOCatalogue() {
		CheckIOCatalogueImpl checkIOCatalogue = new CheckIOCatalogueImpl();
		return checkIOCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIn createCheckIn() {
		CheckInImpl checkIn = new CheckInImpl();
		return checkIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckOut createCheckOut() {
		CheckOutImpl checkOut = new CheckOutImpl();
		return checkOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomIdToBookingIdEntry createRoomIdToBookingIdEntry() {
		RoomIdToBookingIdEntryImpl roomIdToBookingIdEntry = new RoomIdToBookingIdEntryImpl();
		return roomIdToBookingIdEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler createCheckIOHandler() {
		CheckIOHandlerImpl checkIOHandler = new CheckIOHandlerImpl();
		return checkIOHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOPackage getCheckIOPackage() {
		return (CheckIOPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CheckIOPackage getPackage() {
		return CheckIOPackage.eINSTANCE;
	}

} //CheckIOFactoryImpl
