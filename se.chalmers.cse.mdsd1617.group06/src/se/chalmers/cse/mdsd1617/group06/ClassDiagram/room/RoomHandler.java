/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomcatalogue <em>Roomcatalogue</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomHandler()
 * @model
 * @generated
 */
public interface RoomHandler extends EObject {
	/**
	 * Returns the value of the '<em><b>Roomcatalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roomcatalogue</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roomcatalogue</em>' reference.
	 * @see #setRoomcatalogue(RoomCatalogue)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomHandler_Roomcatalogue()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomCatalogue getRoomcatalogue();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomcatalogue <em>Roomcatalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roomcatalogue</em>' reference.
	 * @see #getRoomcatalogue()
	 * @generated
	 */
	void setRoomcatalogue(RoomCatalogue value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" noOfBedsRequired="true" noOfBedsOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" pricePerNightRequired="true" pricePerNightOrdered="false"
	 * @generated
	 */
	boolean addRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldDescriptionRequired="true" oldDescriptionOrdered="false" newNoOfBedsRequired="true" newNoOfBedsOrdered="false" newDescriptionRequired="true" newDescriptionOrdered="false" newPriceRequired="true" newPriceOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int getNbrOfRooms(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearAllRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model numRoomsRequired="true" numRoomsOrdered="false"
	 * @generated
	 */
	void startup(int numRooms);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomCatalogue roomcatalogue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false" descriptionRequired="true" descriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	void addExtra(int roomNumber, String description, float price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" minNumBedsRequired="true" minNumBedsOrdered="false"
	 * @generated
	 */
	EList<RoomType> getAllRoomTypesWithMinNumberOfBeds(int minNumBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	boolean addRoom(int roomId, String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomType> getLstRoomType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeRoom(int roomId, String newRoomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	Room getRoomById(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	EList<Room> getRoomsOfType(String roomTypeDescription);

} // RoomHandler
