/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getCheckIOCatalogue <em>Check IO Catalogue</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getRoomHandler <em>Room Handler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getBookingHandler <em>Booking Handler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getPriceForInitiatedCheckout <em>Price For Initiated Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getBookingInitCheckout <em>Booking Init Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getPriceForInitiatedRoomCheckout <em>Price For Initiated Room Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOHandlerImpl#getRoomInitCheckout <em>Room Init Checkout</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CheckIOHandlerImpl extends MinimalEObjectImpl.Container implements CheckIOHandler {
	/**
	 * The cached value of the '{@link #getCheckiocatalogue() <em>Checkiocatalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * Singleton.
	 * <!-- end-user-doc -->
	 * @see #getCheckiocatalogue()
	 * @generated NOT
	 * @ordered
	 */
	protected static CheckIOCatalogue checkIOCatalogue;
	/**
	 * The cached value of the '{@link #getRoomHandler() <em>Room Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomHandler()
	 * @generated
	 * @ordered
	 */
	protected RoomHandler roomHandler;
	/**
	 * The cached value of the '{@link #getBookingHandler() <em>Booking Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingHandler()
	 * @generated
	 * @ordered
	 */
	protected BookingHandler bookingHandler;
	/**
	 * The default value of the '{@link #getPriceForInitiatedCheckout() <em>Price For Initiated Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriceForInitiatedCheckout()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_FOR_INITIATED_CHECKOUT_EDEFAULT = 0.0;
	/**
	 * The cached value of the '{@link #getPriceForInitiatedCheckout() <em>Price For Initiated Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriceForInitiatedCheckout()
	 * @generated
	 * @ordered
	 */
	protected double priceForInitiatedCheckout = PRICE_FOR_INITIATED_CHECKOUT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getBookingInitCheckout() <em>Booking Init Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingInitCheckout()
	 * @generated
	 * @ordered
	 */
	protected Booking bookingInitCheckout;
	/**
	 * The default value of the '{@link #getPriceForInitiatedRoomCheckout() <em>Price For Initiated Room Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriceForInitiatedRoomCheckout()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_FOR_INITIATED_ROOM_CHECKOUT_EDEFAULT = 0.0;
	/**
	 * The cached value of the '{@link #getPriceForInitiatedRoomCheckout() <em>Price For Initiated Room Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriceForInitiatedRoomCheckout()
	 * @generated
	 * @ordered
	 */
	protected double priceForInitiatedRoomCheckout = PRICE_FOR_INITIATED_ROOM_CHECKOUT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getRoomInitCheckout() <em>Room Init Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomInitCheckout()
	 * @generated
	 * @ordered
	 */
	protected Room roomInitCheckout;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected CheckIOHandlerImpl() {
		super();
		this.bookingHandler = BookingFactory.eINSTANCE.createBookingHandler();
		this.roomHandler = RoomFactory.eINSTANCE.createRoomHandler();
		roomInitCheckout = null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CheckIOPackage.Literals.CHECK_IO_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOCatalogue getCheckIOCatalogue() {
		if (checkIOCatalogue != null && checkIOCatalogue.eIsProxy()) {
			InternalEObject oldCheckIOCatalogue = (InternalEObject)checkIOCatalogue;
			checkIOCatalogue = (CheckIOCatalogue)eResolveProxy(oldCheckIOCatalogue);
			if (checkIOCatalogue != oldCheckIOCatalogue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE, oldCheckIOCatalogue, checkIOCatalogue));
			}
		}
		return checkIOCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOCatalogue basicGetCheckIOCatalogue() {
		return checkIOCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckIOCatalogue(CheckIOCatalogue newCheckIOCatalogue) {
		CheckIOCatalogue oldCheckIOCatalogue = checkIOCatalogue;
		checkIOCatalogue = newCheckIOCatalogue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE, oldCheckIOCatalogue, checkIOCatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler getRoomHandler() {
		if (roomHandler != null && roomHandler.eIsProxy()) {
			InternalEObject oldRoomHandler = (InternalEObject)roomHandler;
			roomHandler = (RoomHandler)eResolveProxy(oldRoomHandler);
			if (roomHandler != oldRoomHandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER, oldRoomHandler, roomHandler));
			}
		}
		return roomHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler basicGetRoomHandler() {
		return roomHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomHandler(RoomHandler newRoomHandler) {
		RoomHandler oldRoomHandler = roomHandler;
		roomHandler = newRoomHandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER, oldRoomHandler, roomHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler getBookingHandler() {
		if (bookingHandler != null && bookingHandler.eIsProxy()) {
			InternalEObject oldBookingHandler = (InternalEObject)bookingHandler;
			bookingHandler = (BookingHandler)eResolveProxy(oldBookingHandler);
			if (bookingHandler != oldBookingHandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER, oldBookingHandler, bookingHandler));
			}
		}
		return bookingHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler basicGetBookingHandler() {
		return bookingHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingHandler(BookingHandler newBookingHandler) {
		BookingHandler oldBookingHandler = bookingHandler;
		bookingHandler = newBookingHandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER, oldBookingHandler, bookingHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPriceForInitiatedCheckout() {
		return priceForInitiatedCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriceForInitiatedCheckout(double newPriceForInitiatedCheckout) {
		double oldPriceForInitiatedCheckout = priceForInitiatedCheckout;
		priceForInitiatedCheckout = newPriceForInitiatedCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT, oldPriceForInitiatedCheckout, priceForInitiatedCheckout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking getBookingInitCheckout() {
		if (bookingInitCheckout != null && bookingInitCheckout.eIsProxy()) {
			InternalEObject oldBookingInitCheckout = (InternalEObject)bookingInitCheckout;
			bookingInitCheckout = (Booking)eResolveProxy(oldBookingInitCheckout);
			if (bookingInitCheckout != oldBookingInitCheckout) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT, oldBookingInitCheckout, bookingInitCheckout));
			}
		}
		return bookingInitCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking basicGetBookingInitCheckout() {
		return bookingInitCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingInitCheckout(Booking newBookingInitCheckout) {
		Booking oldBookingInitCheckout = bookingInitCheckout;
		bookingInitCheckout = newBookingInitCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT, oldBookingInitCheckout, bookingInitCheckout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPriceForInitiatedRoomCheckout() {
		return priceForInitiatedRoomCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriceForInitiatedRoomCheckout(double newPriceForInitiatedRoomCheckout) {
		double oldPriceForInitiatedRoomCheckout = priceForInitiatedRoomCheckout;
		priceForInitiatedRoomCheckout = newPriceForInitiatedRoomCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT, oldPriceForInitiatedRoomCheckout, priceForInitiatedRoomCheckout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoomInitCheckout() {
		if (roomInitCheckout != null && roomInitCheckout.eIsProxy()) {
			InternalEObject oldRoomInitCheckout = (InternalEObject)roomInitCheckout;
			roomInitCheckout = (Room)eResolveProxy(oldRoomInitCheckout);
			if (roomInitCheckout != oldRoomInitCheckout) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT, oldRoomInitCheckout, roomInitCheckout));
			}
		}
		return roomInitCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetRoomInitCheckout() {
		return roomInitCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomInitCheckout(Room newRoomInitCheckout) {
		Room oldRoomInitCheckout = roomInitCheckout;
		roomInitCheckout = newRoomInitCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT, oldRoomInitCheckout, roomInitCheckout));
	}

	/**
	 * Takes a booking ID and returns a list of available rooms corresponding to the 
	 * booked rooms types.
	 * 
	 * @generated NOT
	 */
	public EList<Room> initiateCheckIn(int bookingId) {
		Booking booking = bookingHandler.getBooking(bookingId);
		
		if(booking == null) return null;
		if(booking.getBookingStatus() != BookingStatus.CONFIRMED) return null;
		
		EList<RoomType> roomTypeList= new BasicEList<RoomType>();
		EList<Room> roomList= new BasicEList<Room>();
		EList<Room> availableRoomList= new BasicEList<Room>();
		
		//Adding the room types asked from the booking
		for(RoomTypeToNumberEntry r : booking.getRoomMap()){
			roomTypeList.add(r.getKey());
		}
		
		//Adding all the rooms of the room types asked from the booking
		for(RoomType roomType : roomTypeList){
			roomList.addAll(roomHandler.getRoomsOfType(roomType.getRoomTypeDescription()));
		}
		
		//Adding the available rooms 
		for(Room room : roomList){
			if(room.getRoomStatus() == RoomStatus.FREE) availableRoomList.add(room);
		}
		
		return availableRoomList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInRoomById(int bookingId, int roomId) {
		Booking booking = bookingHandler.getBooking(bookingId);
		Room room = roomHandler.getRoomById(roomId);
		
		if(booking == null) return false;
		if(room == null) return false;
		
		if(booking.getBookingStatus() != BookingStatus.CONFIRMED) return false;
		if(room.getRoomStatus() != RoomStatus.FREE) return false;
		
		//The booking does not contain any rooms of the given type
		boolean failedToFind = true;
		for(RoomTypeToNumberEntry entry : booking.getRoomMap()){
			if(entry.getKey() == room.getRoomType()) failedToFind = false;
		}
		if(failedToFind) return false;
		
		//The booking already have the request amount of rooms of the given type
		for(RoomTypeToNumberEntry entry : booking.getRoomMap()){
			int roomCounter = 0;
			for(Room r : booking.getRoomsAssociated()){
				if(r.getRoomType() == room.getRoomType()) roomCounter++;
			}
			if(roomCounter >= entry.getValue()) return false;
		}
		
		//If all checks passed, book the room
		booking.getRoomsAssociated().add(room);
		room.setRoomStatus(RoomStatus.OCCUPIED);
		if(areAllRoomsCheckedIn(bookingId)) booking.setBookingStatus(BookingStatus.CHECKED_IN);
		
		//Register the check in
		EList<Integer> roomIds = new BasicEList<Integer>();
		roomIds.add(roomId);
		checkInReg(roomIds, bookingId);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoomByType(int bookingId, String roomTypeDescription) {
				
				
				Booking booking = bookingHandler.getBooking(bookingId);
				
				
				if(booking != null && booking.getBookingStatus() == BookingStatus.CONFIRMED ){
					
					int roomOfTypeCounter = 0;
					EList<Room> roomList = initiateCheckIn(bookingId);
					
					for(Room room : roomList){
						if(room.getRoomType().getRoomTypeDescription().equals(roomTypeDescription) && room.getRoomStatus() == RoomStatus.FREE ){
							for(RoomTypeToNumberEntry rttne : booking.getRoomMap()){
								
								if(rttne.getKey().equals(room.getRoomType())){
									
									for(Room r : booking.getRoomsAssociated()){
										
										if(room.getRoomType().equals(r.getRoomType())){
											roomOfTypeCounter++;
										}
										
									}
									if(rttne.getValue() > roomOfTypeCounter){
										
										roomHandler.getRoomById(room.getId()).setRoomStatus(RoomStatus.OCCUPIED);
										bookingHandler.getBooking(bookingId).getRoomsAssociated().add(room);
											
										if(areAllRoomsCheckedIn(bookingId)){
											bookingHandler.getBooking(bookingId).setBookingStatus(BookingStatus.CHECKED_IN);	
										}
										
										EList<Integer> roomIds = new BasicEList<Integer>();
										roomIds.add(room.getId());
										checkInReg(roomIds, bookingId);
										
										return room.getId();
									}
									
								}
							
							}
						}
					}
				}
				
				return -1;
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private boolean areAllRoomsCheckedIn(int bookingId){
		
		
		Booking booking = bookingHandler.getBooking(bookingId);
		
		int roomCounter = 0;
		RoomTypeToNumberEntry rttne = null;
		
		EList<Room> bookingRoomList = booking.getRoomsAssociated();
		
		for(int i = 0; i < booking.getRoomMap().size(); i++){
			
			rttne = booking.getRoomMap().get(i);
			
			roomCounter += rttne.getValue();
				
		}
		
		return roomCounter == bookingRoomList.size();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int _expiryMonth, int expiryYear, String firstName, String lastName) {
 		if(bookingInitCheckout == null || priceForInitiatedCheckout == -1){
 			return false;
 		}
 		try {
 			
 			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires banking = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
 			if(banking.isCreditCardValid(ccNumber, ccv, _expiryMonth, expiryYear, firstName, lastName)){
 				if(priceForInitiatedCheckout > 0.0){
 					if(banking.makePayment(ccNumber, ccv, _expiryMonth, expiryYear, firstName, lastName, priceForInitiatedCheckout)){
 						
 						priceForInitiatedCheckout = -1;
 						
 						for(int i = 0; i < bookingInitCheckout.getRoomsAssociated().size(); i++){
 							if(bookingInitCheckout.getRoomsAssociated().get(i).getId() == roomNumber){
 								bookingInitCheckout.getRoomsAssociated().remove(i);
 							}
 						}
							
 						
 						if(bookingInitCheckout.getRoomsAssociated().isEmpty()){
 							bookingInitCheckout.setBookingStatus(BookingStatus.CHECKED_OUT);
 							bookingInitCheckout.getRoomsAssociated().clear();
 						}
 						
 						bookingInitCheckout = null;
 						
 						return true;
 					}
 				}
 			}
 			
 		} catch (SOAPException e) {
 			
 			e.printStackTrace();
 		} 
 		return false;
 	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */

 	public boolean payDuringCheckout(String ccNumber, String ccv, int _expiryMonth, int expiryYear, String firstName, String lastName) {
 				if(bookingInitCheckout == null || priceForInitiatedCheckout == -1){
 					return false;
 				}

 				try {
 					
 					se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires banking = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
 					if(banking.isCreditCardValid(ccNumber, ccv, _expiryMonth, expiryYear, firstName, lastName)){
 						if(priceForInitiatedCheckout > 0.0){
 							if(banking.makePayment(ccNumber, ccv, _expiryMonth, expiryYear, firstName, lastName, priceForInitiatedCheckout)){
 								priceForInitiatedCheckout = -1;
 								
 								bookingInitCheckout.getRoomsAssociated().clear();
 								bookingInitCheckout.setBookingStatus(BookingStatus.CHECKED_OUT);
 								
 								bookingInitCheckout = null;
 								return true;
 							}
 						}
 					}
 					
 				} catch (SOAPException e) {
 					
 					
 				} 
 				return false;
 	}
	


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
public double initiateCheckout(int bookingId) {
 		
 		BookingHandler bh = bookingHandler;
 		Booking booking = bh.getBooking(bookingId);
 		
 		priceForInitiatedCheckout = 0.0;
 		bookingInitCheckout = null;
 		
 		if(booking == null){
 			return -1;
 		}
 		
 		if(booking.getBookingStatus() == BookingStatus.CHECKED_IN){
 			EList<Room> roomList = booking.getRoomsAssociated();
 			double price=0.0;
	 			
	 		for(int i = 0; i < roomList.size(); i++){
	 			priceForInitiatedCheckout = initiateRoomCheckout(roomList.get(i).getId(), bookingId);
	 			if(priceForInitiatedCheckout < 0){
 					return -1;
 				}
 				price += priceForInitiatedCheckout;
	 		}
 		
 			 if(price <= 0.0 ){
 				 priceForInitiatedCheckout = -1;
 			 }else{
 				 bookingInitCheckout = booking;
 			 }
 				 return priceForInitiatedCheckout;
 			 

 		}
 		return -1;
 	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
public double initiateRoomCheckout(int roomId, int bookingId) {
		double pricePerNight = 0.0;
		double extraCosts = 0.0;
		priceForInitiatedCheckout = 0.0;
		bookingInitCheckout = null;

		Booking booking = this.bookingHandler.getBooking(bookingId);
		Room room = this.roomHandler.getRoomById(roomId);

		if(booking == null || booking.getBookingStatus() != BookingStatus.CHECKED_IN){
			return -1;
		}
		
		EList<Room> roomsInBooking = booking.getRoomsAssociated();
		if(!roomsInBooking.contains(room)){
			return -100;
		}
		
		Date sDate = booking.getStartDate();
		Date eDate = booking.getEndDate();
		long diff = eDate.getTime() - sDate.getTime();
	    
		int nights = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

		pricePerNight = room.getRoomType().getPricePerNight();
		double totalCostForNights = nights*pricePerNight;
		
		for(Extras extra : room.getExtras()){
			extraCosts += extra.getPrice();
		}
		priceForInitiatedCheckout = totalCostForNights + extraCosts;
		bookingInitCheckout = booking;
		
		checkOutReg(roomId, bookingId);
		
		for(int i = 0; i < booking.getRoomsAssociated().size(); i++){
			if(booking.getRoomsAssociated().get(i).getId() == roomId) booking.getRoomsAssociated().remove(i);
		}
		room.setRoomStatus(RoomStatus.FREE);
		room.getExtras().clear();
		
		return priceForInitiatedCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearAllCheckIO() {
		checkIOCatalogue().clearAllCheckIO();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomIdToBookingIdEntry> listOccupiedRooms(Date date) {
		EList<RoomIdToBookingIdEntry> temp = new BasicEList<RoomIdToBookingIdEntry>();

		// Check that it is not invalid: null or in the future.
		if(date == null || new Date(System.currentTimeMillis()).before(date)) return temp;

		List<CheckIn> checkIns = new ArrayList<CheckIn>();

		for(CheckIn c : checkIOCatalogue.getCheckIns()){
			if(areOnSameDay(c.getDate(), date) || c.getDate().before(date)) {
				checkIns.add(c);
			}
		}

		for(CheckIn ci : checkIns){
			for (int i : ci.getRoomIds()) {

				boolean shouldAdd = true;

				// Search through checkouts to see if the room was checked out
				// before our given date.
				for(CheckOut co : checkIOCatalogue.getCheckOuts()){
					if(ci.getBookingId() == co.getBookingId()){
						if(i == co.getRoomId() 
								&& !areOnSameDay(co.getDate(), date) 
								&& co.getDate().before(date)) {
							shouldAdd = false;
						}
					}
				}
				if (shouldAdd) {
					// Create an entry.
					RoomIdToBookingIdEntry rb = CheckIOFactory.eINSTANCE.createRoomIdToBookingIdEntry();
					rb.setBookingId(ci.getBookingId());
					rb.setRoomId(i);
					temp.add(rb);
				}
			}
		}

		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Singleton method. Call this instead of accessing the variable checkiocatalogue 
	 * variable directly to ensure the instance for the catolgue is shared among all 
	 * instances of the handler.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CheckIOCatalogue checkIOCatalogue() {
		if (checkIOCatalogue == null) {
			checkIOCatalogue = CheckIOFactory.eINSTANCE.createCheckIOCatalogue();
		}
		return checkIOCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public EList<CheckIn> listCheckIns(Date date) {
		if(date == null || new Date().before(date)) return null;
		
		EList<CheckIn> temp = new BasicEList<CheckIn>();

		for(CheckIn c : checkIOCatalogue.getCheckIns()){
			if(areOnSameDay(c.getDate(), date)) temp.add(c);
		}
		
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckIn> listCheckIns(Date startDate, Date endDate) {
		if(startDate == null || new Date().before(startDate)) return null;
		if(endDate == null || startDate.after(endDate)) return null;
		
		EList<CheckIn> temp = new BasicEList<CheckIn>();
		
		for(CheckIn c : checkIOCatalogue.getCheckIns()){
			if(c.getDate().after(startDate) && c.getDate().before(endDate)) temp.add(c);
		}
		
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOut> listCheckOuts(Date date) {
		if(date == null || new Date().before(date)) return null;
		
		EList<CheckOut> temp = new BasicEList<CheckOut>();
		
		for(CheckOut c : checkIOCatalogue.getCheckOuts()){
			if(areOnSameDay(c.getDate(), date)) 
			   temp.add(c);
		}
		
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOut> listCheckOuts(Date startDate, Date endDate) {
		if(startDate == null || new Date().before(startDate)) return null;
		if(endDate == null || startDate.after(endDate)) return null;
		
		EList<CheckOut> temp = new BasicEList<CheckOut>();
		
		for(CheckOut c : checkIOCatalogue.getCheckOuts()){
			if(c.getDate().after(startDate) && c.getDate().before(endDate)) temp.add(c);
		}
		
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInReg(EList<Integer> roomIds, int bookingId) {
		CheckIn checkIn = CheckIOFactory.eINSTANCE.createCheckIn();
		checkIn.setDate(new Date());
		checkIn.setBookingId(bookingId);
		for(int i : roomIds){
			checkIn.addToRoomIds(i);
		}
		checkIOCatalogue().registerCheckIn(checkIn);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkOutReg(int roomId, int bookingId) {
		CheckOut checkOut = CheckIOFactory.eINSTANCE.createCheckOut();
		checkOut.setDate(new Date());
		checkOut.setBookingId(bookingId);
		checkOut.setRoomId(roomId);
		checkIOCatalogue().registerCheckOut(checkOut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Helper method: check that two dates are on the same day.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean areOnSameDay(Date d1, Date d2) {
		return d1.getDate() == d2.getDate() && d1.getMonth() == d2.getMonth() && d1.getYear() == d2.getYear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE:
				if (resolve) return getCheckIOCatalogue();
				return basicGetCheckIOCatalogue();
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER:
				if (resolve) return getRoomHandler();
				return basicGetRoomHandler();
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER:
				if (resolve) return getBookingHandler();
				return basicGetBookingHandler();
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT:
				return getPriceForInitiatedCheckout();
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT:
				if (resolve) return getBookingInitCheckout();
				return basicGetBookingInitCheckout();
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT:
				return getPriceForInitiatedRoomCheckout();
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT:
				if (resolve) return getRoomInitCheckout();
				return basicGetRoomInitCheckout();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE:
				setCheckIOCatalogue((CheckIOCatalogue)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER:
				setRoomHandler((RoomHandler)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER:
				setBookingHandler((BookingHandler)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT:
				setPriceForInitiatedCheckout((Double)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT:
				setBookingInitCheckout((Booking)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT:
				setPriceForInitiatedRoomCheckout((Double)newValue);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT:
				setRoomInitCheckout((Room)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE:
				setCheckIOCatalogue((CheckIOCatalogue)null);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER:
				setRoomHandler((RoomHandler)null);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER:
				setBookingHandler((BookingHandler)null);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT:
				setPriceForInitiatedCheckout(PRICE_FOR_INITIATED_CHECKOUT_EDEFAULT);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT:
				setBookingInitCheckout((Booking)null);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT:
				setPriceForInitiatedRoomCheckout(PRICE_FOR_INITIATED_ROOM_CHECKOUT_EDEFAULT);
				return;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT:
				setRoomInitCheckout((Room)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IO_HANDLER__CHECK_IO_CATALOGUE:
				return checkIOCatalogue != null;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_HANDLER:
				return roomHandler != null;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_HANDLER:
				return bookingHandler != null;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_CHECKOUT:
				return priceForInitiatedCheckout != PRICE_FOR_INITIATED_CHECKOUT_EDEFAULT;
			case CheckIOPackage.CHECK_IO_HANDLER__BOOKING_INIT_CHECKOUT:
				return bookingInitCheckout != null;
			case CheckIOPackage.CHECK_IO_HANDLER__PRICE_FOR_INITIATED_ROOM_CHECKOUT:
				return priceForInitiatedRoomCheckout != PRICE_FOR_INITIATED_ROOM_CHECKOUT_EDEFAULT;
			case CheckIOPackage.CHECK_IO_HANDLER__ROOM_INIT_CHECKOUT:
				return roomInitCheckout != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CheckIOPackage.CHECK_IO_HANDLER___INITIATE_CHECK_IN__INT:
				return initiateCheckIn((Integer)arguments.get(0));
			case CheckIOPackage.CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_ID__INT_INT:
				return checkInRoomById((Integer)arguments.get(0), (Integer)arguments.get(1));
			case CheckIOPackage.CHECK_IO_HANDLER___CHECK_IN_ROOM_BY_TYPE__INT_STRING:
				return checkInRoomByType((Integer)arguments.get(0), (String)arguments.get(1));
			case CheckIOPackage.CHECK_IO_HANDLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case CheckIOPackage.CHECK_IO_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case CheckIOPackage.CHECK_IO_HANDLER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case CheckIOPackage.CHECK_IO_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case CheckIOPackage.CHECK_IO_HANDLER___CLEAR_ALL_CHECK_IO:
				clearAllCheckIO();
				return null;
			case CheckIOPackage.CHECK_IO_HANDLER___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case CheckIOPackage.CHECK_IO_HANDLER___CHECK_IO_CATALOGUE:
				return checkIOCatalogue();
			case CheckIOPackage.CHECK_IO_HANDLER___LIST_CHECK_INS__DATE:
				return listCheckIns((Date)arguments.get(0));
			case CheckIOPackage.CHECK_IO_HANDLER___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case CheckIOPackage.CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE:
				return listCheckOuts((Date)arguments.get(0));
			case CheckIOPackage.CHECK_IO_HANDLER___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case CheckIOPackage.CHECK_IO_HANDLER___CHECK_IN_REG__ELIST_INT:
				checkInReg((EList<Integer>)arguments.get(0), (Integer)arguments.get(1));
				return null;
			case CheckIOPackage.CHECK_IO_HANDLER___CHECK_OUT_REG__INT_INT:
				checkOutReg((Integer)arguments.get(0), (Integer)arguments.get(1));
				return null;
			case CheckIOPackage.CHECK_IO_HANDLER___ARE_ON_SAME_DAY__DATE_DATE:
				return areOnSameDay((Date)arguments.get(0), (Date)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (priceForInitiatedCheckout: ");
		result.append(priceForInitiatedCheckout);
		result.append(", priceForInitiatedRoomCheckout: ");
		result.append(priceForInitiatedRoomCheckout);
		result.append(')');
		return result.toString();
	}

} //CheckIOHandlerImpl
