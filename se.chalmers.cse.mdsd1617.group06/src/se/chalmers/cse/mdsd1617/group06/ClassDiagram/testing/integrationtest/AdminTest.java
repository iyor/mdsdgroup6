/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.integrationtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl;

/**
 * 
 */
public class AdminTest {

	protected RoomHandler roomHandler;
	
	protected HotelAdministratorProvides admin;

	public AdminTest() {
		super();
		this.roomHandler = RoomFactory.eINSTANCE.createRoomHandler();
		this.admin = IOFactory.eINSTANCE.createHotelAdministratorProvides();
	}

	@Before
	public void init(){
		this.roomHandler.clearAllRooms();
		this.roomHandler.roomcatalogue().clearAllRoomTypes();;
	}
	
	/**
	 * UC 2.2.1
	 */
	@Test
	public void addRoomTypeTest() {
		assertTrue(this.admin.addRoomType(1, "single room", 300.0));
		assertFalse(this.admin.addRoomType(1, "single room", 300.0)); //Duplicate room types are not permitted
		assertTrue(this.admin.addRoomType(2, "double room", 500.0));
	}

	/**
	 *  UC 2.2.2
	 */
	@Test
	public void updateRoomTypeTest() {
		this.admin.addRoomType(1, "single room", 30000.0);
		//Incorrect specification of existing room not allowed
		assertFalse(this.admin.updateRoomType("signle rum", 1, "single room", 300.0)); 						
		assertTrue(this.admin.updateRoomType("single room", 1, "single room", 300.0));
	}

	/**
	 * UC 2.2.3
	 */
	@Test
	public void removeRoomTypeTest() {
		this.admin.addRoomType(1, "single room", 300.0);
		//Removal of non-existent room type
		assertFalse(this.admin.removeRoomType("double room"));
		assertTrue(this.admin.removeRoomType("single room"));
	}
	
	/**
	 * UC 2.2.4
	 */
	@Test
	public void addRoomTest() {
		this.admin.addRoomType(1, "single room", 30000.0);
		assertTrue(this.admin.addRoom(10, "single room"));
		assertEquals(1, this.roomHandler.getNbrOfRooms("single room"));
	}
	
	/**
	 * UC 2.2.5
	 */
	@Test
	public void changeRoomTypeRoomTest() {
		this.admin.addRoomType(1, "single room", 30000.0);
		this.admin.addRoom(10, "single room");
		assertEquals(1, this.roomHandler.getNbrOfRooms("single room"));
		
		// update room type of room
		this.admin.addRoomType(2, "double room", 60000.0);
		this.admin.changeRoomTypeRoom(10, "double room");
		assertEquals(1, this.roomHandler.getNbrOfRooms("double room"));
	}
	
	/**
	 * UC 2.2.6
	 */
	@Test
	public void removeRoomTest() {
		this.admin.addRoomType(1, "single room", 30000.0);
		this.admin.addRoom(10, "single room");
		assertEquals(1, this.roomHandler.getNbrOfRooms("single room"));
		
		this.admin.removeRoom(10);
		assertEquals(0, this.roomHandler.getNbrOfRooms("single room"));
	}
	
	/**
	 * UC 2.2.7/8
	 */
	@Test
	public void testBlockUnblock() {
		this.admin.startup(1);
		this.admin.blockRoom(1);
		assertTrue(this.roomHandler.roomcatalogue().getRoomById(1).isBlocked());
		this.admin.unblockRoom(1);
		assertFalse(this.roomHandler.roomcatalogue().getRoomById(1).isBlocked());
	}
	
	/**
	 * UC 2.2.9
	 */
	@Test
	public void startupTest() {
		this.admin.startup(2);
		assertEquals(1, roomHandler.getAllRoomTypesWithMinNumberOfBeds(2).size());
		assertEquals(2, roomHandler.getNbrOfRooms("default"));
		this.admin.startup(100);
		assertEquals(1, roomHandler.getAllRoomTypesWithMinNumberOfBeds(2).size());
		assertEquals(100, roomHandler.getNbrOfRooms("default"));
	}

} //AdminImpl