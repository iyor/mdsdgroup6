/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group06/ClassDiagram/room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group06.ClassDiagram.room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl <em>Catalogue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomCatalogue()
	 * @generated
	 */
	int ROOM_CATALOGUE = 0;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE__ROOMS = 0;

	/**
	 * The feature id for the '<em><b>Room Type List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE__ROOM_TYPE_LIST = 1;

	/**
	 * The number of structural features of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___REMOVE_ROOM_TYPE__STRING = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___ADD_ROOM_TYPE__INT_STRING_DOUBLE = 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___ADD_ROOM__INT_STRING = 3;

	/**
	 * The operation id for the '<em>Clear All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___CLEAR_ALL_ROOMS = 4;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___ADD_EXTRA__INT_STRING_FLOAT = 5;

	/**
	 * The operation id for the '<em>Get All Room Types With Min Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT = 6;

	/**
	 * The operation id for the '<em>Get Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___GET_ROOM_BY_ID__INT = 7;

	/**
	 * The operation id for the '<em>Get Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___GET_ROOMS_OF_TYPE__STRING = 8;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___REMOVE_ROOM__ROOM = 9;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___GET_ROOM_TYPE__STRING = 10;

	/**
	 * The operation id for the '<em>Clear All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___CLEAR_ALL_ROOM_TYPES = 11;

	/**
	 * The operation id for the '<em>Change Room Type Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE___CHANGE_ROOM_TYPE_ROOM__STRING_INT = 12;

	/**
	 * The number of operations of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CATALOGUE_OPERATION_COUNT = 13;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ID = 0;

	/**
	 * The feature id for the '<em><b>Room Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_STATUS = 1;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Extras</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__EXTRAS = 3;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___IS_BLOCKED = 0;

	/**
	 * The operation id for the '<em>Set Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___SET_BLOCKED__BOOLEAN = 1;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___ADD_EXTRA__STRING_FLOAT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>No Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NO_OF_BEDS = 0;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__ROOM_TYPE_DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE_PER_NIGHT = 2;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___UPDATE_ROOM_TYPE__INT_STRING_DOUBLE = 0;

	/**
	 * The operation id for the '<em>Equals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___EQUALS__OBJECT = 1;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.ExtrasImpl <em>Extras</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.ExtrasImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getExtras()
	 * @generated
	 */
	int EXTRAS = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAS__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAS__PRICE = 1;

	/**
	 * The number of structural features of the '<em>Extras</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extras</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomHandler()
	 * @generated
	 */
	int ROOM_HANDLER = 4;

	/**
	 * The feature id for the '<em><b>Roomcatalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER__ROOMCATALOGUE = 0;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___ADD_ROOM_TYPE__INT_STRING_DOUBLE = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Get Nbr Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_NBR_OF_ROOMS__STRING = 3;

	/**
	 * The operation id for the '<em>Clear All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CLEAR_ALL_ROOMS = 4;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___STARTUP__INT = 5;

	/**
	 * The operation id for the '<em>Roomcatalogue</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___ROOMCATALOGUE = 6;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___ADD_EXTRA__INT_STRING_FLOAT = 7;

	/**
	 * The operation id for the '<em>Get All Room Types With Min Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT = 8;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___BLOCK_ROOM__INT = 9;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___UNBLOCK_ROOM__INT = 10;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___ADD_ROOM__INT_STRING = 11;

	/**
	 * The operation id for the '<em>Get Lst Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_LST_ROOM_TYPE = 12;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___REMOVE_ROOM__INT = 13;

	/**
	 * The operation id for the '<em>Change Room Type Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CHANGE_ROOM_TYPE_ROOM__INT_STRING = 14;

	/**
	 * The operation id for the '<em>Get Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ROOM_BY_ID__INT = 15;

	/**
	 * The operation id for the '<em>Get Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ROOMS_OF_TYPE__STRING = 16;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER_OPERATION_COUNT = 17;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomStatus()
	 * @generated
	 */
	int ROOM_STATUS = 5;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue <em>Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Catalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue
	 * @generated
	 */
	EClass getRoomCatalogue();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRooms()
	 * @see #getRoomCatalogue()
	 * @generated
	 */
	EReference getRoomCatalogue_Rooms();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomTypeList <em>Room Type List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Type List</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomTypeList()
	 * @see #getRoomCatalogue()
	 * @generated
	 */
	EReference getRoomCatalogue_RoomTypeList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomCatalogue__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#updateRoomType(java.lang.String, int, java.lang.String, double) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#updateRoomType(java.lang.String, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomCatalogue__UpdateRoomType__String_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addRoomType(int, java.lang.String, double) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addRoomType(int, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomCatalogue__AddRoomType__int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomCatalogue__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#clearAllRooms() <em>Clear All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#clearAllRooms()
	 * @generated
	 */
	EOperation getRoomCatalogue__ClearAllRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addExtra(int, java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#addExtra(int, java.lang.String, float)
	 * @generated
	 */
	EOperation getRoomCatalogue__AddExtra__int_String_float();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getAllRoomTypesWithMinNumberOfBeds(int) <em>Get All Room Types With Min Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Room Types With Min Number Of Beds</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getAllRoomTypesWithMinNumberOfBeds(int)
	 * @generated
	 */
	EOperation getRoomCatalogue__GetAllRoomTypesWithMinNumberOfBeds__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomById(int) <em>Get Room By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room By Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomById(int)
	 * @generated
	 */
	EOperation getRoomCatalogue__GetRoomById__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomsOfType(java.lang.String) <em>Get Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomCatalogue__GetRoomsOfType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#removeRoom(se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#removeRoom(se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room)
	 * @generated
	 */
	EOperation getRoomCatalogue__RemoveRoom__Room();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomType(java.lang.String) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomCatalogue__GetRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#clearAllRoomTypes() <em>Clear All Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#clearAllRoomTypes()
	 * @generated
	 */
	EOperation getRoomCatalogue__ClearAllRoomTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#changeRoomTypeRoom(java.lang.String, int) <em>Change Room Type Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#changeRoomTypeRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getRoomCatalogue__ChangeRoomTypeRoom__String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getNoOfBeds <em>No Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getNoOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NoOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getRoomTypeDescription()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#getPricePerNight()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_PricePerNight();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#updateRoomType(int, java.lang.String, double) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#updateRoomType(int, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomType__UpdateRoomType__int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#equals(java.lang.Object) <em>Equals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Equals</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType#equals(java.lang.Object)
	 * @generated
	 */
	EOperation getRoomType__Equals__Object();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_RoomType();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getExtras <em>Extras</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extras</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getExtras()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Extras();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#isBlocked() <em>Is Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blocked</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#isBlocked()
	 * @generated
	 */
	EOperation getRoom__IsBlocked();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#setBlocked(boolean) <em>Set Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Blocked</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#setBlocked(boolean)
	 * @generated
	 */
	EOperation getRoom__SetBlocked__boolean();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#addExtra(java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#addExtra(java.lang.String, float)
	 * @generated
	 */
	EOperation getRoom__AddExtra__String_float();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getId()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomStatus <em>Room Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room#getRoomStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomStatus();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras <em>Extras</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extras</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras
	 * @generated
	 */
	EClass getExtras();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras#getDescription()
	 * @see #getExtras()
	 * @generated
	 */
	EAttribute getExtras_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras#getPrice()
	 * @see #getExtras()
	 * @generated
	 */
	EAttribute getExtras_Price();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler
	 * @generated
	 */
	EClass getRoomHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomcatalogue <em>Roomcatalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomcatalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomcatalogue()
	 * @see #getRoomHandler()
	 * @generated
	 */
	EReference getRoomHandler_Roomcatalogue();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addRoomType(int, java.lang.String, double) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addRoomType(int, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomHandler__AddRoomType__int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#updateRoomType(java.lang.String, int, java.lang.String, double) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#updateRoomType(java.lang.String, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomHandler__UpdateRoomType__String_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomHandler__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getNbrOfRooms(java.lang.String) <em>Get Nbr Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Nbr Of Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getNbrOfRooms(java.lang.String)
	 * @generated
	 */
	EOperation getRoomHandler__GetNbrOfRooms__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#clearAllRooms() <em>Clear All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#clearAllRooms()
	 * @generated
	 */
	EOperation getRoomHandler__ClearAllRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#startup(int)
	 * @generated
	 */
	EOperation getRoomHandler__Startup__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#roomcatalogue() <em>Roomcatalogue</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Roomcatalogue</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#roomcatalogue()
	 * @generated
	 */
	EOperation getRoomHandler__Roomcatalogue();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addExtra(int, java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addExtra(int, java.lang.String, float)
	 * @generated
	 */
	EOperation getRoomHandler__AddExtra__int_String_float();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getAllRoomTypesWithMinNumberOfBeds(int) <em>Get All Room Types With Min Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Room Types With Min Number Of Beds</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getAllRoomTypesWithMinNumberOfBeds(int)
	 * @generated
	 */
	EOperation getRoomHandler__GetAllRoomTypesWithMinNumberOfBeds__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#blockRoom(int)
	 * @generated
	 */
	EOperation getRoomHandler__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#unblockRoom(int)
	 * @generated
	 */
	EOperation getRoomHandler__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomHandler__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getLstRoomType() <em>Get Lst Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Lst Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getLstRoomType()
	 * @generated
	 */
	EOperation getRoomHandler__GetLstRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#removeRoom(int)
	 * @generated
	 */
	EOperation getRoomHandler__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#changeRoomTypeRoom(int, java.lang.String) <em>Change Room Type Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#changeRoomTypeRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomHandler__ChangeRoomTypeRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomById(int) <em>Get Room By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room By Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomById(int)
	 * @generated
	 */
	EOperation getRoomHandler__GetRoomById__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomsOfType(java.lang.String) <em>Get Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler#getRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomHandler__GetRoomsOfType__String();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus
	 * @generated
	 */
	EEnum getRoomStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl <em>Catalogue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomCatalogueImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomCatalogue()
		 * @generated
		 */
		EClass ROOM_CATALOGUE = eINSTANCE.getRoomCatalogue();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_CATALOGUE__ROOMS = eINSTANCE.getRoomCatalogue_Rooms();

		/**
		 * The meta object literal for the '<em><b>Room Type List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_CATALOGUE__ROOM_TYPE_LIST = eINSTANCE.getRoomCatalogue_RoomTypeList();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getRoomCatalogue__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = eINSTANCE.getRoomCatalogue__UpdateRoomType__String_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___ADD_ROOM_TYPE__INT_STRING_DOUBLE = eINSTANCE.getRoomCatalogue__AddRoomType__int_String_double();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___ADD_ROOM__INT_STRING = eINSTANCE.getRoomCatalogue__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Clear All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___CLEAR_ALL_ROOMS = eINSTANCE.getRoomCatalogue__ClearAllRooms();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___ADD_EXTRA__INT_STRING_FLOAT = eINSTANCE.getRoomCatalogue__AddExtra__int_String_float();

		/**
		 * The meta object literal for the '<em><b>Get All Room Types With Min Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT = eINSTANCE.getRoomCatalogue__GetAllRoomTypesWithMinNumberOfBeds__int();

		/**
		 * The meta object literal for the '<em><b>Get Room By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___GET_ROOM_BY_ID__INT = eINSTANCE.getRoomCatalogue__GetRoomById__int();

		/**
		 * The meta object literal for the '<em><b>Get Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___GET_ROOMS_OF_TYPE__STRING = eINSTANCE.getRoomCatalogue__GetRoomsOfType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___REMOVE_ROOM__ROOM = eINSTANCE.getRoomCatalogue__RemoveRoom__Room();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___GET_ROOM_TYPE__STRING = eINSTANCE.getRoomCatalogue__GetRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Clear All Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___CLEAR_ALL_ROOM_TYPES = eINSTANCE.getRoomCatalogue__ClearAllRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CATALOGUE___CHANGE_ROOM_TYPE_ROOM__STRING_INT = eINSTANCE.getRoomCatalogue__ChangeRoomTypeRoom__String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>No Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NO_OF_BEDS = eINSTANCE.getRoomType_NoOfBeds();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__ROOM_TYPE_DESCRIPTION = eINSTANCE.getRoomType_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE_PER_NIGHT = eINSTANCE.getRoomType_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE___UPDATE_ROOM_TYPE__INT_STRING_DOUBLE = eINSTANCE.getRoomType__UpdateRoomType__int_String_double();

		/**
		 * The meta object literal for the '<em><b>Equals</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE___EQUALS__OBJECT = eINSTANCE.getRoomType__Equals__Object();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOM_TYPE = eINSTANCE.getRoom_RoomType();

		/**
		 * The meta object literal for the '<em><b>Extras</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__EXTRAS = eINSTANCE.getRoom_Extras();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM___IS_BLOCKED = eINSTANCE.getRoom__IsBlocked();

		/**
		 * The meta object literal for the '<em><b>Set Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM___SET_BLOCKED__BOOLEAN = eINSTANCE.getRoom__SetBlocked__boolean();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM___ADD_EXTRA__STRING_FLOAT = eINSTANCE.getRoom__AddExtra__String_float();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ID = eINSTANCE.getRoom_Id();

		/**
		 * The meta object literal for the '<em><b>Room Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_STATUS = eINSTANCE.getRoom_RoomStatus();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.ExtrasImpl <em>Extras</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.ExtrasImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getExtras()
		 * @generated
		 */
		EClass EXTRAS = eINSTANCE.getExtras();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRAS__DESCRIPTION = eINSTANCE.getExtras_Description();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRAS__PRICE = eINSTANCE.getExtras_Price();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomHandler()
		 * @generated
		 */
		EClass ROOM_HANDLER = eINSTANCE.getRoomHandler();

		/**
		 * The meta object literal for the '<em><b>Roomcatalogue</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_HANDLER__ROOMCATALOGUE = eINSTANCE.getRoomHandler_Roomcatalogue();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___ADD_ROOM_TYPE__INT_STRING_DOUBLE = eINSTANCE.getRoomHandler__AddRoomType__int_String_double();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = eINSTANCE.getRoomHandler__UpdateRoomType__String_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getRoomHandler__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Get Nbr Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___GET_NBR_OF_ROOMS__STRING = eINSTANCE.getRoomHandler__GetNbrOfRooms__String();

		/**
		 * The meta object literal for the '<em><b>Clear All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___CLEAR_ALL_ROOMS = eINSTANCE.getRoomHandler__ClearAllRooms();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___STARTUP__INT = eINSTANCE.getRoomHandler__Startup__int();

		/**
		 * The meta object literal for the '<em><b>Roomcatalogue</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___ROOMCATALOGUE = eINSTANCE.getRoomHandler__Roomcatalogue();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___ADD_EXTRA__INT_STRING_FLOAT = eINSTANCE.getRoomHandler__AddExtra__int_String_float();

		/**
		 * The meta object literal for the '<em><b>Get All Room Types With Min Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___GET_ALL_ROOM_TYPES_WITH_MIN_NUMBER_OF_BEDS__INT = eINSTANCE.getRoomHandler__GetAllRoomTypesWithMinNumberOfBeds__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___BLOCK_ROOM__INT = eINSTANCE.getRoomHandler__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___UNBLOCK_ROOM__INT = eINSTANCE.getRoomHandler__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___ADD_ROOM__INT_STRING = eINSTANCE.getRoomHandler__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Get Lst Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___GET_LST_ROOM_TYPE = eINSTANCE.getRoomHandler__GetLstRoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___REMOVE_ROOM__INT = eINSTANCE.getRoomHandler__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___CHANGE_ROOM_TYPE_ROOM__INT_STRING = eINSTANCE.getRoomHandler__ChangeRoomTypeRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Get Room By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___GET_ROOM_BY_ID__INT = eINSTANCE.getRoomHandler__GetRoomById__int();

		/**
		 * The meta object literal for the '<em><b>Get Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_HANDLER___GET_ROOMS_OF_TYPE__STRING = eINSTANCE.getRoomHandler__GetRoomsOfType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl#getRoomStatus()
		 * @generated
		 */
		EEnum ROOM_STATUS = eINSTANCE.getRoomStatus();

	}

} //RoomPackage
