/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Id To Booking Id Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl#getRoomId <em>Room Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.RoomIdToBookingIdEntryImpl#getBookingId <em>Booking Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomIdToBookingIdEntryImpl extends MinimalEObjectImpl.Container implements RoomIdToBookingIdEntry {
	/**
	 * The default value of the '{@link #getRoomId() <em>Room Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomId()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoomId() <em>Room Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomId()
	 * @generated
	 * @ordered
	 */
	protected int roomId = ROOM_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected int bookingId = BOOKING_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomIdToBookingIdEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CheckIOPackage.Literals.ROOM_ID_TO_BOOKING_ID_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomId() {
		return roomId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomId(int newRoomId) {
		int oldRoomId = roomId;
		roomId = newRoomId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID, oldRoomId, roomId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingId() {
		return bookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingId(int newBookingId) {
		int oldBookingId = bookingId;
		bookingId = newBookingId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID, oldBookingId, bookingId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID:
				return getRoomId();
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID:
				return getBookingId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID:
				setRoomId((Integer)newValue);
				return;
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID:
				setBookingId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID:
				setRoomId(ROOM_ID_EDEFAULT);
				return;
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID:
				setBookingId(BOOKING_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__ROOM_ID:
				return roomId != ROOM_ID_EDEFAULT;
			case CheckIOPackage.ROOM_ID_TO_BOOKING_ID_ENTRY__BOOKING_ID:
				return bookingId != BOOKING_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (roomId: ");
		result.append(roomId);
		result.append(", bookingId: ");
		result.append(bookingId);
		result.append(')');
		return result.toString();
	}

} //RoomIdToBookingIdEntryImpl
