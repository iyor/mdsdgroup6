/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl;

import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Check In</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl#getDate <em>Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckInImpl#getRoomIds <em>Room Ids</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CheckInImpl extends MinimalEObjectImpl.Container implements CheckIn {
	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected Date date = DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected int bookingId = BOOKING_ID_EDEFAULT;
	/**
	 * The cached value of the '{@link #getRoomIds() <em>Room Ids</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * All the rooms that are checked in by this check in action.
	 * <!-- end-user-doc -->
	 * @see #getRoomIds()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> roomIds;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected CheckInImpl() {
		super();
		roomIds = new BasicEList<Integer>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CheckIOPackage.Literals.CHECK_IN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(Date newDate) {
		Date oldDate = date;
		date = newDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IN__DATE, oldDate, date));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingId() {
		return bookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingId(int newBookingId) {
		int oldBookingId = bookingId;
		bookingId = newBookingId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CheckIOPackage.CHECK_IN__BOOKING_ID, oldBookingId, bookingId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getRoomIds() {
		if (roomIds == null) {
			roomIds = new EDataTypeUniqueEList<Integer>(Integer.class, this, CheckIOPackage.CHECK_IN__ROOM_IDS);
		}
		return roomIds;
	}
	
	public void addToRoomIds(Integer roomId){
		roomIds.add(roomId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IN__DATE:
				return getDate();
			case CheckIOPackage.CHECK_IN__BOOKING_ID:
				return getBookingId();
			case CheckIOPackage.CHECK_IN__ROOM_IDS:
				return getRoomIds();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IN__DATE:
				setDate((Date)newValue);
				return;
			case CheckIOPackage.CHECK_IN__BOOKING_ID:
				setBookingId((Integer)newValue);
				return;
			case CheckIOPackage.CHECK_IN__ROOM_IDS:
				getRoomIds().clear();
				getRoomIds().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IN__DATE:
				setDate(DATE_EDEFAULT);
				return;
			case CheckIOPackage.CHECK_IN__BOOKING_ID:
				setBookingId(BOOKING_ID_EDEFAULT);
				return;
			case CheckIOPackage.CHECK_IN__ROOM_IDS:
				getRoomIds().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CheckIOPackage.CHECK_IN__DATE:
				return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
			case CheckIOPackage.CHECK_IN__BOOKING_ID:
				return bookingId != BOOKING_ID_EDEFAULT;
			case CheckIOPackage.CHECK_IN__ROOM_IDS:
				return roomIds != null && !roomIds.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (date: ");
		result.append(date);
		result.append(", bookingId: ");
		result.append(bookingId);
		result.append(", roomIds: ");
		result.append(roomIds);
		result.append(')');
		return result.toString();
	}

} //CheckInImpl
