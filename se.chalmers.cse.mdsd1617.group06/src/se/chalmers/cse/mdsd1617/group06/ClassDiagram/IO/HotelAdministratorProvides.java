/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getCheckiohandler <em>Checkiohandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getBookinghandler <em>Bookinghandler</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelAdministratorProvides()
 * @model
 * @generated
 */
public interface HotelAdministratorProvides extends IAdministrator, IHotelStartupProvides {

	/**
	 * Returns the value of the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roomhandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roomhandler</em>' reference.
	 * @see #setRoomhandler(RoomHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelAdministratorProvides_Roomhandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomHandler getRoomhandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getRoomhandler <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roomhandler</em>' reference.
	 * @see #getRoomhandler()
	 * @generated
	 */
	void setRoomhandler(RoomHandler value);

	/**
	 * Returns the value of the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checkiohandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checkiohandler</em>' reference.
	 * @see #setCheckiohandler(CheckIOHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelAdministratorProvides_Checkiohandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CheckIOHandler getCheckiohandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getCheckiohandler <em>Checkiohandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checkiohandler</em>' reference.
	 * @see #getCheckiohandler()
	 * @generated
	 */
	void setCheckiohandler(CheckIOHandler value);

	/**
	 * Returns the value of the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookinghandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookinghandler</em>' reference.
	 * @see #setBookinghandler(BookingHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelAdministratorProvides_Bookinghandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingHandler getBookinghandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getBookinghandler <em>Bookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookinghandler</em>' reference.
	 * @see #getBookinghandler()
	 * @generated
	 */
	void setBookinghandler(BookingHandler value);
} // HotelAdministratorProvides
