package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.unittest;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

public class UnitTestsIO {

	protected IHotelStartupProvides startupProvides;
	protected IHotelCustomerProvides customerProvides;
	protected BookingHandler bookinghandler;
	protected RoomHandler roomhandler;
	protected CheckIOHandler checkiohandler;
	
	@Before 
	public void initializeHandlers(){
		startupProvides = IOFactory.eINSTANCE.createHotelStartupProvides();
		customerProvides = IOFactory.eINSTANCE.createHotelCustomerProvides();
		bookinghandler = BookingFactory.eINSTANCE.createBookingHandler();
		roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
		checkiohandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
	}
	
	/**
	 * 
	 * Test if the handlers correctly clear respective catalogues.
	 */

	@Test
	public void clearSystemTest() {
		bookinghandler.clearAllBookings();
		assertEquals(bookinghandler.getBookingcatalogue().getBookings().size(), 0);
		roomhandler.clearAllRooms();
		assertEquals(roomhandler.roomcatalogue().getRooms().size(),0);
		checkiohandler.clearAllCheckIO();
		assertEquals(checkiohandler.checkIOCatalogue().getCheckIns().size(),0);
		assertEquals(checkiohandler.checkIOCatalogue().getCheckOuts().size(),0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Test
	public void startupTest() {
		this.roomhandler.clearAllRooms();
		this.roomhandler.startup(10);
		assertEquals(10, this.roomhandler.getNbrOfRooms("default"));
		assertEquals(10, this.roomhandler.roomcatalogue().getRooms().size());
		
		// Make sure previous changes are retained properly.
		this.roomhandler.startup(5);
		assertEquals(10, this.roomhandler.getNbrOfRooms("default"));
		assertEquals(10, this.roomhandler.roomcatalogue().getRooms().size());		
	}
	
	@Test
	public void convertDateTest() {
		HotelCustomerProvidesImpl tmpCustomerProvides = (HotelCustomerProvidesImpl) this.customerProvides;
		Date test = tmpCustomerProvides.convertDate("20170201");
		// Zero indexing on days and months.
		Date lastJan = new Date(117, 0, 31);
		Date secondFeb = new Date(117, 1, 2);
		assertTrue(test.after(lastJan));
		assertTrue(test.before(secondFeb));
	}
	
}
