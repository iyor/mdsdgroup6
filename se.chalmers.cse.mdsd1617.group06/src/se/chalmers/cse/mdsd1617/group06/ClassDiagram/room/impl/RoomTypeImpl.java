/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl#getNoOfBeds <em>No Of Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl#getRoomTypeDescription <em>Room Type Description</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomTypeImpl#getPricePerNight <em>Price Per Night</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeImpl extends MinimalEObjectImpl.Container implements RoomType {
	/**
	 * The default value of the '{@link #getNoOfBeds() <em>No Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoOfBeds()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_OF_BEDS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNoOfBeds() <em>No Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoOfBeds()
	 * @generated
	 * @ordered
	 */
	protected int noOfBeds = NO_OF_BEDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomTypeDescription() <em>Room Type Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOM_TYPE_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRoomTypeDescription() <em>Room Type Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeDescription()
	 * @generated
	 * @ordered
	 */
	protected String roomTypeDescription = ROOM_TYPE_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPricePerNight() <em>Price Per Night</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPricePerNight()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_PER_NIGHT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPricePerNight() <em>Price Per Night</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPricePerNight()
	 * @generated
	 * @ordered
	 */
	protected double pricePerNight = PRICE_PER_NIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	protected RoomTypeImpl(int noOfBeds, String roomDescription, double pricePerNight) {
		super();
		this.noOfBeds = noOfBeds;
		this.roomTypeDescription = roomDescription;
		this.pricePerNight = pricePerNight;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoOfBeds() {
		return noOfBeds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoOfBeds(int newNoOfBeds) {
		int oldNoOfBeds = noOfBeds;
		noOfBeds = newNoOfBeds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_TYPE__NO_OF_BEDS, oldNoOfBeds, noOfBeds));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRoomTypeDescription() {
		return roomTypeDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeDescription(String newRoomTypeDescription) {
		String oldRoomTypeDescription = roomTypeDescription;
		roomTypeDescription = newRoomTypeDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_TYPE__ROOM_TYPE_DESCRIPTION, oldRoomTypeDescription, roomTypeDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPricePerNight() {
		return pricePerNight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPricePerNight(double newPricePerNight) {
		double oldPricePerNight = pricePerNight;
		pricePerNight = newPricePerNight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_TYPE__PRICE_PER_NIGHT, oldPricePerNight, pricePerNight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateRoomType(int noOfBeds, String roomTypeDescription, double pricePerNight) {
		this.noOfBeds = noOfBeds;
		this.roomTypeDescription = roomTypeDescription;
		this.pricePerNight = pricePerNight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(obj instanceof RoomTypeImpl)
			if(((RoomTypeImpl) obj).getNoOfBeds()==this.noOfBeds &&
					((RoomTypeImpl) obj).getRoomTypeDescription()==this.roomTypeDescription &&
					((RoomTypeImpl) obj).getPricePerNight() == this.pricePerNight )
				return true;
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE__NO_OF_BEDS:
				return getNoOfBeds();
			case RoomPackage.ROOM_TYPE__ROOM_TYPE_DESCRIPTION:
				return getRoomTypeDescription();
			case RoomPackage.ROOM_TYPE__PRICE_PER_NIGHT:
				return getPricePerNight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE__NO_OF_BEDS:
				setNoOfBeds((Integer)newValue);
				return;
			case RoomPackage.ROOM_TYPE__ROOM_TYPE_DESCRIPTION:
				setRoomTypeDescription((String)newValue);
				return;
			case RoomPackage.ROOM_TYPE__PRICE_PER_NIGHT:
				setPricePerNight((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE__NO_OF_BEDS:
				setNoOfBeds(NO_OF_BEDS_EDEFAULT);
				return;
			case RoomPackage.ROOM_TYPE__ROOM_TYPE_DESCRIPTION:
				setRoomTypeDescription(ROOM_TYPE_DESCRIPTION_EDEFAULT);
				return;
			case RoomPackage.ROOM_TYPE__PRICE_PER_NIGHT:
				setPricePerNight(PRICE_PER_NIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE__NO_OF_BEDS:
				return noOfBeds != NO_OF_BEDS_EDEFAULT;
			case RoomPackage.ROOM_TYPE__ROOM_TYPE_DESCRIPTION:
				return ROOM_TYPE_DESCRIPTION_EDEFAULT == null ? roomTypeDescription != null : !ROOM_TYPE_DESCRIPTION_EDEFAULT.equals(roomTypeDescription);
			case RoomPackage.ROOM_TYPE__PRICE_PER_NIGHT:
				return pricePerNight != PRICE_PER_NIGHT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_TYPE___UPDATE_ROOM_TYPE__INT_STRING_DOUBLE:
				updateRoomType((Integer)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2));
				return null;
			case RoomPackage.ROOM_TYPE___EQUALS__OBJECT:
				return equals(arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (noOfBeds: ");
		result.append(noOfBeds);
		result.append(", roomTypeDescription: ");
		result.append(roomTypeDescription);
		result.append(", pricePerNight: ");
		result.append(pricePerNight);
		result.append(')');
		return result.toString();
	}

} //RoomTypeImpl