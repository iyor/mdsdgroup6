/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIn;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckOut;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.RoomIdToBookingIdEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl#getCheckiohandler <em>Checkiohandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl#getBookinghandler <em>Bookinghandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelReceptionistProvidesImpl extends MinimalEObjectImpl.Container implements HotelReceptionistProvides {
	/**
	 * The cached value of the '{@link #getRoomhandler() <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomhandler()
	 * @generated
	 * @ordered
	 */
	protected RoomHandler roomhandler;
	/**
	 * The cached value of the '{@link #getCheckiohandler() <em>Checkiohandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckiohandler()
	 * @generated
	 * @ordered
	 */
	protected CheckIOHandler checkiohandler;
	/**
	 * The cached value of the '{@link #getBookinghandler() <em>Bookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookinghandler()
	 * @generated
	 * @ordered
	 */
	protected BookingHandler bookinghandler;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelReceptionistProvidesImpl() {
		super();
		this.roomhandler = RoomFactory.eINSTANCE.createRoomHandler();
		this.checkiohandler = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		this.bookinghandler = BookingFactory.eINSTANCE.createBookingHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IOPackage.Literals.HOTEL_RECEPTIONIST_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler getRoomhandler() {
		if (roomhandler != null && roomhandler.eIsProxy()) {
			InternalEObject oldRoomhandler = (InternalEObject)roomhandler;
			roomhandler = (RoomHandler)eResolveProxy(oldRoomhandler);
			if (roomhandler != oldRoomhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
			}
		}
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomHandler basicGetRoomhandler() {
		return roomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomhandler(RoomHandler newRoomhandler) {
		RoomHandler oldRoomhandler = roomhandler;
		roomhandler = newRoomhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER, oldRoomhandler, roomhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler getCheckiohandler() {
		if (checkiohandler != null && checkiohandler.eIsProxy()) {
			InternalEObject oldCheckiohandler = (InternalEObject)checkiohandler;
			checkiohandler = (CheckIOHandler)eResolveProxy(oldCheckiohandler);
			if (checkiohandler != oldCheckiohandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
			}
		}
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckIOHandler basicGetCheckiohandler() {
		return checkiohandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckiohandler(CheckIOHandler newCheckiohandler) {
		CheckIOHandler oldCheckiohandler = checkiohandler;
		checkiohandler = newCheckiohandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER, oldCheckiohandler, checkiohandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler getBookinghandler() {
		if (bookinghandler != null && bookinghandler.eIsProxy()) {
			InternalEObject oldBookinghandler = (InternalEObject)bookinghandler;
			bookinghandler = (BookingHandler)eResolveProxy(oldBookinghandler);
			if (bookinghandler != oldBookinghandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
			}
		}
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingHandler basicGetBookinghandler() {
		return bookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookinghandler(BookingHandler newBookinghandler) {
		BookingHandler oldBookinghandler = bookinghandler;
		bookinghandler = newBookinghandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER, oldBookinghandler, bookinghandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		Date sD = convertDate(startDate);
		Date eD = convertDate(endDate);
		if (eD == null || sD == null || eD.before(sD))
			return new BasicEList<FreeRoomTypesDTO>();
		return this.bookinghandler.getFreeRooms(numBeds, sD, eD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		if (firstName == null || lastName == null || startDate == null || endDate == null) 
			return -1;
		
		Date sD = convertDate(startDate);
		Date eD = convertDate(endDate);
		
		if (sD == null || eD == null) return -1;
		
		if (eD.before(sD))
			return -1;
		
		return bookinghandler.initiateBooking(firstName,lastName, sD, eD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return bookinghandler.addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return bookinghandler.confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		return checkiohandler.initiateCheckout(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return checkiohandler.payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return checkiohandler.checkInRoomByType(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		return checkiohandler.initiateRoomCheckout(roomNumber, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return checkiohandler.payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, String startDate, String endDate, EList<RoomTypeToNumberEntry> roomTypesandCount) {
		return bookinghandler.editBooking(bookingID, convertDate(startDate), convertDate(endDate), roomTypesandCount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, EList<RoomTypeToNumberEntry> roomTypesandCount) {
		return bookinghandler.editBooking(bookingID, roomTypesandCount); 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, String startDate, String endDate) {
		return bookinghandler.editBooking(bookingID, convertDate(startDate), convertDate(endDate)); 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingId) {
		return bookinghandler.cancelBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(int roomNumber, String description, float price) {
		roomhandler.addExtra(roomNumber, description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> initiateCheckIn(int bookingId) {
		return checkiohandler.initiateCheckIn(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInRoomById(int bookingId, int roomId) {
		return checkiohandler.checkInRoomById(bookingId, roomId);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomIdToBookingIdEntry> listOccupiedRooms(Date date) {
		return checkiohandler.listOccupiedRooms(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckIn> listCheckIns(Date date) {
		return checkiohandler.listCheckIns(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckIn> listCheckIns(Date startDate, Date endDate) {
		return checkiohandler.listCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOut> listCheckOuts(Date date) {
		return checkiohandler.listCheckOuts(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOut> listCheckOuts(Date startDate, Date endDate) {
		return checkiohandler.listCheckOuts(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(int bookingId, int roomNumber, String description, float price) {
		bookinghandler.addExtra(bookingId, roomNumber, description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date convertDate(String date) {
		try {
			int year = Integer.parseInt(date.substring(0, 4));
			int month = Integer.parseInt(date.substring(4, 6));
			int day = Integer.parseInt(date.substring(6, 8));
			return new Date(year - 1900,month-1,day);
		} catch (NullPointerException | StringIndexOutOfBoundsException | NumberFormatException e) {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER:
				if (resolve) return getRoomhandler();
				return basicGetRoomhandler();
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER:
				if (resolve) return getCheckiohandler();
				return basicGetCheckiohandler();
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER:
				if (resolve) return getBookinghandler();
				return basicGetBookinghandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)newValue);
				return;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)newValue);
				return;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER:
				setRoomhandler((RoomHandler)null);
				return;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER:
				setCheckiohandler((CheckIOHandler)null);
				return;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER:
				setBookinghandler((BookingHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER:
				return roomhandler != null;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER:
				return checkiohandler != null;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER:
				return bookinghandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IReceptionist.class) {
			switch (baseOperationID) {
				case IOPackage.IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING_ELIST: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING_ELIST;
				case IOPackage.IRECEPTIONIST___EDIT_BOOKING__INT_ELIST: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_ELIST;
				case IOPackage.IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING;
				case IOPackage.IRECEPTIONIST___CANCEL_BOOKING__INT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT;
				case IOPackage.IRECEPTIONIST___ADD_EXTRA__INT_STRING_FLOAT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_STRING_FLOAT;
				case IOPackage.IRECEPTIONIST___INITIATE_CHECK_IN__INT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT;
				case IOPackage.IRECEPTIONIST___CHECK_IN_ROOM_BY_ID__INT_INT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM_BY_ID__INT_INT;
				case IOPackage.IRECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING_1;
				case IOPackage.IRECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_1;
				case IOPackage.IRECEPTIONIST___INITIATE_CHECKOUT__INT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT_1;
				case IOPackage.IRECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT_1;
				case IOPackage.IRECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE;
				case IOPackage.IRECEPTIONIST___LIST_CHECK_INS__DATE: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE;
				case IOPackage.IRECEPTIONIST___LIST_CHECK_INS__DATE_DATE: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE;
				case IOPackage.IRECEPTIONIST___LIST_CHECK_OUTS__DATE: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE;
				case IOPackage.IRECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE;
				case IOPackage.IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_FLOAT: return IOPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_INT_STRING_FLOAT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT_1:
				return initiateCheckout((Integer)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_1:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT_1:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING_1:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING_ELIST:
				return editBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (EList<RoomTypeToNumberEntry>)arguments.get(3));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_ELIST:
				return editBooking((Integer)arguments.get(0), (EList<RoomTypeToNumberEntry>)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING:
				return editBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_STRING_FLOAT:
				addExtra((Integer)arguments.get(0), (String)arguments.get(1), (Float)arguments.get(2));
				return null;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT:
				return initiateCheckIn((Integer)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM_BY_ID__INT_INT:
				return checkInRoomById((Integer)arguments.get(0), (Integer)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE:
				return listCheckIns((Date)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE:
				return listCheckOuts((Date)arguments.get(0));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_INT_STRING_FLOAT:
				addExtra((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Float)arguments.get(3));
				return null;
			case IOPackage.HOTEL_RECEPTIONIST_PROVIDES___CONVERT_DATE__STRING:
				return convertDate((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelReceptionistProvidesImpl
