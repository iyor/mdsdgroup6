/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.ClassDiagramFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.ClassDiagramPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassDiagramPackageImpl extends EPackageImpl implements ClassDiagramPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adminEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receptionistEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.ClassDiagramPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClassDiagramPackageImpl() {
		super(eNS_URI, ClassDiagramFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ClassDiagramPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClassDiagramPackage init() {
		if (isInited) return (ClassDiagramPackage)EPackage.Registry.INSTANCE.getEPackage(ClassDiagramPackage.eNS_URI);

		// Obtain or create and register package
		ClassDiagramPackageImpl theClassDiagramPackage = (ClassDiagramPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ClassDiagramPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ClassDiagramPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		IOPackageImpl theIOPackage = (IOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) instanceof IOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) : IOPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		CheckIOPackageImpl theCheckIOPackage = (CheckIOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) instanceof CheckIOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) : CheckIOPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);

		// Create package meta-data objects
		theClassDiagramPackage.createPackageContents();
		theIOPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theCheckIOPackage.createPackageContents();
		theBookingPackage.createPackageContents();

		// Initialize created meta-data
		theClassDiagramPackage.initializePackageContents();
		theIOPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theCheckIOPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassDiagramPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassDiagramPackage.eNS_URI, theClassDiagramPackage);
		return theClassDiagramPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdmin() {
		return adminEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAdmin_Roomhandler() {
		return (EReference)adminEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAdmin_Startupprovides() {
		return (EReference)adminEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__AddRoomTypeTest() {
		return adminEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__UpdateRoomTypeTest() {
		return adminEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__RemoveRoomTypeTest() {
		return adminEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__TestBlockUnblock() {
		return adminEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__StartupTest() {
		return adminEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdmin__AddRoomTest() {
		return adminEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceptionist() {
		return receptionistEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_BookingHandler() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_Hotelcustomerprovides() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_RoomHandler() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_StartUpProvides() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_CheckIOHandler() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__InitiateBookingTest() {
		return receptionistEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__AddRoomsToBookingTest() {
		return receptionistEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ConfirmBookingTest() {
		return receptionistEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CancelBookingTest() {
		return receptionistEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__AddExtraToBookingTest() {
		return receptionistEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__SearchForFreeRooms__int_Date_Date() {
		return receptionistEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__AddExtraCostToRooms__int_int_String_float() {
		return receptionistEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__AddTooManyRoomsTest() {
		return receptionistEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckInTest() {
		return receptionistEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckOutTest() {
		return receptionistEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListCheckInsByDayTest() {
		return receptionistEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListCheckOutsByDayTest() {
		return receptionistEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__EditBookingTimeTest() {
		return receptionistEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__EditBookingRoomsTest() {
		return receptionistEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__EditBookingDateAndRoomTest() {
		return receptionistEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagramFactory getClassDiagramFactory() {
		return (ClassDiagramFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		adminEClass = createEClass(ADMIN);
		createEReference(adminEClass, ADMIN__ROOMHANDLER);
		createEReference(adminEClass, ADMIN__STARTUPPROVIDES);
		createEOperation(adminEClass, ADMIN___ADD_ROOM_TYPE_TEST);
		createEOperation(adminEClass, ADMIN___UPDATE_ROOM_TYPE_TEST);
		createEOperation(adminEClass, ADMIN___REMOVE_ROOM_TYPE_TEST);
		createEOperation(adminEClass, ADMIN___TEST_BLOCK_UNBLOCK);
		createEOperation(adminEClass, ADMIN___STARTUP_TEST);
		createEOperation(adminEClass, ADMIN___ADD_ROOM_TEST);

		receptionistEClass = createEClass(RECEPTIONIST);
		createEReference(receptionistEClass, RECEPTIONIST__BOOKING_HANDLER);
		createEReference(receptionistEClass, RECEPTIONIST__HOTELCUSTOMERPROVIDES);
		createEReference(receptionistEClass, RECEPTIONIST__ROOM_HANDLER);
		createEReference(receptionistEClass, RECEPTIONIST__START_UP_PROVIDES);
		createEReference(receptionistEClass, RECEPTIONIST__CHECK_IO_HANDLER);
		createEOperation(receptionistEClass, RECEPTIONIST___INITIATE_BOOKING_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___ADD_ROOMS_TO_BOOKING_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___CONFIRM_BOOKING_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___CANCEL_BOOKING_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___ADD_EXTRA_TO_BOOKING_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___SEARCH_FOR_FREE_ROOMS__INT_DATE_DATE);
		createEOperation(receptionistEClass, RECEPTIONIST___ADD_EXTRA_COST_TO_ROOMS__INT_INT_STRING_FLOAT);
		createEOperation(receptionistEClass, RECEPTIONIST___ADD_TOO_MANY_ROOMS_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_IN_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_OUT_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_CHECK_INS_BY_DAY_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_CHECK_OUTS_BY_DAY_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___EDIT_BOOKING_TIME_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___EDIT_BOOKING_ROOMS_TEST);
		createEOperation(receptionistEClass, RECEPTIONIST___EDIT_BOOKING_DATE_AND_ROOM_TEST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IOPackage theIOPackage = (IOPackage)EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI);
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		CheckIOPackage theCheckIOPackage = (CheckIOPackage)EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI);
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theIOPackage);
		getESubpackages().add(theRoomPackage);
		getESubpackages().add(theCheckIOPackage);
		getESubpackages().add(theBookingPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEOperation(getAdmin__AddRoomTypeTest(), null, "addRoomTypeTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdmin__UpdateRoomTypeTest(), null, "updateRoomTypeTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdmin__RemoveRoomTypeTest(), null, "removeRoomTypeTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdmin__TestBlockUnblock(), null, "testBlockUnblock", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdmin__StartupTest(), null, "startupTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdmin__AddRoomTest(), null, "addRoomTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__InitiateBookingTest(), null, "initiateBookingTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__AddRoomsToBookingTest(), null, "addRoomsToBookingTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__ConfirmBookingTest(), null, "confirmBookingTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__CancelBookingTest(), null, "cancelBookingTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__AddExtraToBookingTest(), null, "addExtraToBookingTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		EOperation op = initEOperation(getReceptionist__SearchForFreeRooms__int_Date_Date(), theIOPackage.getFreeRoomTypesDTO(), "searchForFreeRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__AddExtraCostToRooms__int_int_String_float(), null, "addExtraCostToRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__AddTooManyRoomsTest(), null, "addTooManyRoomsTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__CheckInTest(), null, "checkInTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__CheckOutTest(), null, "checkOutTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__ListCheckInsByDayTest(), null, "listCheckInsByDayTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__ListCheckOutsByDayTest(), null, "listCheckOutsByDayTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__EditBookingTimeTest(), null, "editBookingTimeTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__EditBookingRoomsTest(), null, "editBookingRoomsTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__EditBookingDateAndRoomTest(), null, "editBookingDateAndRoomTest", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ClassDiagramPackageImpl
