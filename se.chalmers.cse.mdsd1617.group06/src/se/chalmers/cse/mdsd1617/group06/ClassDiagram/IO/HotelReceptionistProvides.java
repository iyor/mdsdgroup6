/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO;

import java.util.Date;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getRoomhandler <em>Roomhandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getCheckiohandler <em>Checkiohandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getBookinghandler <em>Bookinghandler</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelReceptionistProvides()
 * @model
 * @generated
 */
public interface HotelReceptionistProvides extends IHotelCustomerProvides, IReceptionist {

	/**
	 * Returns the value of the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roomhandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roomhandler</em>' reference.
	 * @see #setRoomhandler(RoomHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelReceptionistProvides_Roomhandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomHandler getRoomhandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getRoomhandler <em>Roomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roomhandler</em>' reference.
	 * @see #getRoomhandler()
	 * @generated
	 */
	void setRoomhandler(RoomHandler value);

	/**
	 * Returns the value of the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checkiohandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checkiohandler</em>' reference.
	 * @see #setCheckiohandler(CheckIOHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelReceptionistProvides_Checkiohandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CheckIOHandler getCheckiohandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getCheckiohandler <em>Checkiohandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checkiohandler</em>' reference.
	 * @see #getCheckiohandler()
	 * @generated
	 */
	void setCheckiohandler(CheckIOHandler value);

	/**
	 * Returns the value of the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookinghandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookinghandler</em>' reference.
	 * @see #setBookinghandler(BookingHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage#getHotelReceptionistProvides_Bookinghandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingHandler getBookinghandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getBookinghandler <em>Bookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookinghandler</em>' reference.
	 * @see #getBookinghandler()
	 * @generated
	 */
	void setBookinghandler(BookingHandler value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	Date convertDate(String date);
} // HotelReceptionistProvides
