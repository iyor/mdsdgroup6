package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.unittest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

public class UnitTestsCheckIO {
	private BookingHandler bh;
	private RoomHandler rh;
	private CheckIOHandler ch;
	private IHotelStartupProvides sp;
	
	@Before 
	public void init(){
		this.bh = BookingFactory.eINSTANCE.createBookingHandler();
		this.rh = RoomFactory.eINSTANCE.createRoomHandler();
		this.ch = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		this.sp = IOFactory.eINSTANCE.createHotelStartupProvides();
	}

	@Test
	public void testCheckIOCatalogueSingleton() {
		CheckIOHandler ch1, ch2;
		ch1 = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		ch2 = CheckIOFactory.eINSTANCE.createCheckIOHandler();
		// Make sure they are not the same object, by some ECore Voodoo
		assertNotEquals(ch1, ch2);
		// ... but make sure they have the same catalogue
		assertEquals(ch1.checkIOCatalogue(), ch2.checkIOCatalogue());
	}
	
	
	@Test
	public void testSeparateCheckIns(){
		bh.clearAllBookings();
		rh.clearAllRooms();
		ch.clearAllCheckIO();
		
		int roomId1 = 1;
		int roomId2 = 2;
		int roomId3 = 3;
		
		rh.roomcatalogue().addRoomType(1, "singleBed", 300.0);
		rh.roomcatalogue().addRoom(roomId1, "singleBed");
		rh.roomcatalogue().addRoom(roomId2, "singleBed");
		rh.roomcatalogue().addRoom(roomId3, "singleBed");
		
		int bookingId = bh.initiateBooking("Beng", "Éntrålar", new Date(Long.MIN_VALUE), new Date(Long.MAX_VALUE));
		bh.addRoomToBooking("singleBed", bookingId);
		bh.addRoomToBooking("singleBed", bookingId);
		bh.addRoomToBooking("singleBed", bookingId);
		
		bh.confirmBooking(bookingId);
		
		ch.checkInRoomById(bookingId, roomId1);
		assertEquals("Only one room should be checked in",
				1, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
		ch.checkInRoomById(bookingId, roomId2);
		assertEquals("Only two rooms should be checked in",
				2, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
		ch.checkInRoomById(bookingId, roomId3);
		assertEquals("All three rooms should be checkd in",
				3, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
	}
	
	@Test
	public void testSeparateCheckOuts(){
		bh.clearAllBookings();
		rh.clearAllRooms();
		ch.clearAllCheckIO();
		rh.roomcatalogue().clearAllRoomTypes();
		
		int roomId1 = 1;
		int roomId2 = 2;
		int roomId3 = 3;
		
		rh.roomcatalogue().addRoomType(1, "singleBed", 300.0);
		rh.roomcatalogue().addRoom(roomId1, "singleBed");
		rh.roomcatalogue().addRoom(roomId2, "singleBed");
		rh.roomcatalogue().addRoom(roomId3, "singleBed");
		
		int bookingId = bh.initiateBooking("Beng", "Éntrålar", new Date(2014,10,10), new Date(2014,10,12));
		bh.addRoomToBooking("singleBed", bookingId);
		bh.addRoomToBooking("singleBed", bookingId);
		bh.addRoomToBooking("singleBed", bookingId);
		
		bh.confirmBooking(bookingId);
		
		ch.checkInRoomById(bookingId, roomId1);
		ch.checkInRoomById(bookingId, roomId2);
		ch.checkInRoomById(bookingId, roomId3);
		
		assertEquals("All three rooms should be associated with the booking",
				3, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
		
		double cost = ch.initiateRoomCheckout(roomId1, bookingId);
		assertEquals(600.0, cost, 0.001);
		assertEquals("Room should only be associated with two rooms",
				2, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
		
		ch.initiateRoomCheckout(roomId2, bookingId);		
		assertEquals("Room should only be associated with one room",
				1, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
		
		ch.initiateRoomCheckout(roomId3, bookingId);
		assertEquals("Room should only be associated with two rooms",
				0, bh.getBookingcatalogue().getBooking(bookingId).getRoomsAssociated().size());
	}
}
