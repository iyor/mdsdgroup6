/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getCheckIOCatalogue <em>Check IO Catalogue</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomHandler <em>Room Handler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingHandler <em>Booking Handler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedCheckout <em>Price For Initiated Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingInitCheckout <em>Booking Init Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedRoomCheckout <em>Price For Initiated Room Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomInitCheckout <em>Room Init Checkout</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler()
 * @model
 * @generated
 */
public interface CheckIOHandler extends EObject {

	/**
	 * Returns the value of the '<em><b>Check IO Catalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check IO Catalogue</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check IO Catalogue</em>' reference.
	 * @see #setCheckIOCatalogue(CheckIOCatalogue)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_CheckIOCatalogue()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CheckIOCatalogue getCheckIOCatalogue();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getCheckIOCatalogue <em>Check IO Catalogue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check IO Catalogue</em>' reference.
	 * @see #getCheckIOCatalogue()
	 * @generated
	 */
	void setCheckIOCatalogue(CheckIOCatalogue value);

	/**
	 * Returns the value of the '<em><b>Room Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Handler</em>' reference.
	 * @see #setRoomHandler(RoomHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_RoomHandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomHandler getRoomHandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomHandler <em>Room Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Handler</em>' reference.
	 * @see #getRoomHandler()
	 * @generated
	 */
	void setRoomHandler(RoomHandler value);

	/**
	 * Returns the value of the '<em><b>Booking Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Handler</em>' reference.
	 * @see #setBookingHandler(BookingHandler)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_BookingHandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingHandler getBookingHandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingHandler <em>Booking Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Handler</em>' reference.
	 * @see #getBookingHandler()
	 * @generated
	 */
	void setBookingHandler(BookingHandler value);

	/**
	 * Returns the value of the '<em><b>Price For Initiated Checkout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price For Initiated Checkout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price For Initiated Checkout</em>' attribute.
	 * @see #setPriceForInitiatedCheckout(double)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_PriceForInitiatedCheckout()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPriceForInitiatedCheckout();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedCheckout <em>Price For Initiated Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price For Initiated Checkout</em>' attribute.
	 * @see #getPriceForInitiatedCheckout()
	 * @generated
	 */
	void setPriceForInitiatedCheckout(double value);

	/**
	 * Returns the value of the '<em><b>Booking Init Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Init Checkout</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Init Checkout</em>' reference.
	 * @see #setBookingInitCheckout(Booking)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_BookingInitCheckout()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Booking getBookingInitCheckout();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getBookingInitCheckout <em>Booking Init Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Init Checkout</em>' reference.
	 * @see #getBookingInitCheckout()
	 * @generated
	 */
	void setBookingInitCheckout(Booking value);

	/**
	 * Returns the value of the '<em><b>Price For Initiated Room Checkout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price For Initiated Room Checkout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price For Initiated Room Checkout</em>' attribute.
	 * @see #setPriceForInitiatedRoomCheckout(double)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_PriceForInitiatedRoomCheckout()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPriceForInitiatedRoomCheckout();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getPriceForInitiatedRoomCheckout <em>Price For Initiated Room Checkout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price For Initiated Room Checkout</em>' attribute.
	 * @see #getPriceForInitiatedRoomCheckout()
	 * @generated
	 */
	void setPriceForInitiatedRoomCheckout(double value);

	/**
	 * Returns the value of the '<em><b>Room Init Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Init Checkout</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Init Checkout</em>' reference.
	 * @see #setRoomInitCheckout(Room)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage#getCheckIOHandler_RoomInitCheckout()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Room getRoomInitCheckout();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOHandler#getRoomInitCheckout <em>Room Init Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Init Checkout</em>' reference.
	 * @see #getRoomInitCheckout()
	 * @generated
	 */
	void setRoomInitCheckout(Room value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	EList<Room> initiateCheckIn(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	boolean checkInRoomById(int bookingId, int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	int checkInRoomByType(int bookingId, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" ccNumberRequired="true" ccNumberOrdered="false" ccvRequired="true" ccvOrdered="false" _expiryMonthRequired="true" _expiryMonthOrdered="false"
	 *        _expiryMonthAnnotation="http://www.eclipse.org/uml2/2.0.0/UML originalName=' expiryMonth'" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int _expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccNumberRequired="true" ccNumberOrdered="false" ccvRequired="true" ccvOrdered="false" _expiryMonthRequired="true" _expiryMonthOrdered="false"
	 *        _expiryMonthAnnotation="http://www.eclipse.org/uml2/2.0.0/UML originalName=' expiryMonth'" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payDuringCheckout(String ccNumber, String ccv, int _expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	double initiateCheckout(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(int roomId, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearAllCheckIO();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<RoomIdToBookingIdEntry> listOccupiedRooms(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CheckIOCatalogue checkIOCatalogue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<CheckIn> listCheckIns(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<CheckIn> listCheckIns(Date startDate, Date endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<CheckOut> listCheckOuts(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<CheckOut> listCheckOuts(Date startDate, Date endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdsMany="true" roomIdsOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	void checkInReg(EList<Integer> roomIds, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIdRequired="true" roomIdOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	void checkOutReg(int roomId, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" d1Required="true" d1Ordered="false" d2Required="true" d2Ordered="false"
	 * @generated
	 */
	boolean areOnSameDay(Date d1, Date d2);
} // CheckIOHandler
