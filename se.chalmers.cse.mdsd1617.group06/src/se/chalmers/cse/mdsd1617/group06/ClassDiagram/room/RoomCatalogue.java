/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Catalogue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue#getRoomTypeList <em>Room Type List</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomCatalogue()
 * @model
 * @generated
 */
public interface RoomCatalogue extends EObject {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(RoomCatalogue)
	 * @see ErikRootElement.room.RoomPackage#getRoomCatalogue_Instance()
	 * @model required="true" ordered="false"
	 * @generated NOT
	 */
	//The getInstance-method may need to be deleted //ERIK
	//RoomCatalogue getInstance();

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomCatalogue_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Room Type List</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type List</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage#getRoomCatalogue_RoomTypeList()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypeList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldDescriptionRequired="true" oldDescriptionOrdered="false" newNoOfBedsRequired="true" newNoOfBedsOrdered="false" newDescriptionRequired="true" newDescriptionOrdered="false" newPriceRequired="true" newPriceOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String oldDescription, int newNoOfBeds, String newDescription, double newPrice);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" noOfBedsRequired="true" noOfBedsOrdered="false" roomDescriptionRequired="true" roomDescriptionOrdered="false" pricePerNightRequired="true" pricePerNightOrdered="false"
	 * @generated
	 */
	boolean addRoomType(int noOfBeds, String roomDescription, double pricePerNight);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	boolean addRoom(int roomId, String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearAllRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false" descriptionRequired="true" descriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	void addExtra(int roomNumber, String description, float price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" minNumBedsRequired="true" minNumBedsOrdered="false"
	 * @generated
	 */
	EList<RoomType> getAllRoomTypesWithMinNumberOfBeds(int minNumBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	Room getRoomById(int roomId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	EList<Room> getRoomsOfType(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomRequired="true" roomOrdered="false"
	 * @generated
	 */
	boolean removeRoom(Room room);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	RoomType getRoomType(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearAllRoomTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeRoom(String roomType, int roomId);

} // RoomCatalogue
