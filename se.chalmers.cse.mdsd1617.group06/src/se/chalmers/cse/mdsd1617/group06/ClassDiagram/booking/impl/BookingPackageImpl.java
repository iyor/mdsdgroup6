/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.CheckIOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.CheckIO.impl.CheckIOPackageImpl;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BookingPackageImpl extends EPackageImpl implements BookingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingCatalogueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeToNumberEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum bookingStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BookingPackageImpl() {
		super(eNS_URI, BookingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BookingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BookingPackage init() {
		if (isInited) return (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Obtain or create and register package
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BookingPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		IOPackageImpl theIOPackage = (IOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) instanceof IOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI) : IOPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		CheckIOPackageImpl theCheckIOPackage = (CheckIOPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) instanceof CheckIOPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CheckIOPackage.eNS_URI) : CheckIOPackage.eINSTANCE);

		// Create package meta-data objects
		theBookingPackage.createPackageContents();
		theIOPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theCheckIOPackage.createPackageContents();

		// Initialize created meta-data
		theBookingPackage.initializePackageContents();
		theIOPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theCheckIOPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBookingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BookingPackage.eNS_URI, theBookingPackage);
		return theBookingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingCatalogue() {
		return bookingCatalogueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingCatalogue_Bookings() {
		return (EReference)bookingCatalogueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBookingCatalogue_CurrentBookingId() {
		return (EAttribute)bookingCatalogueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingCatalogue__GetBooking__int() {
		return bookingCatalogueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingCatalogue__AddBooking__Booking() {
		return bookingCatalogueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingCatalogue__GetNewBookingId() {
		return bookingCatalogueEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingCatalogue__ClearAllBookings() {
		return bookingCatalogueEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingCatalogue__GetBookingsForPeriod__Date_Date() {
		return bookingCatalogueEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooking() {
		return bookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_FirstName() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_LastName() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_StartDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_EndDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_BookingId() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_RoomMap() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_IsPaid() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_RoomsAssociated() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_BookingStatus() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__AddRoomsOfRoomType__RoomType_int() {
		return bookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__ReplaceRoomMap__EList() {
		return bookingEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeToNumberEntry() {
		return roomTypeToNumberEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeToNumberEntry_Key() {
		return (EReference)roomTypeToNumberEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomTypeToNumberEntry_Value() {
		return (EAttribute)roomTypeToNumberEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingHandler() {
		return bookingHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingHandler_Bookingcatalogue() {
		return (EReference)bookingHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingHandler_Roomhandler() {
		return (EReference)bookingHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__InitiateBooking__String_String_Date_Date() {
		return bookingHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__AddRoomToBooking__String_int() {
		return bookingHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__ConfirmBooking__int() {
		return bookingHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__AddExtra__int_int_String_float() {
		return bookingHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__ListBookings() {
		return bookingHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__EditBooking__int_EList() {
		return bookingHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__EditBooking__int_Date_Date() {
		return bookingHandlerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__EditBooking__int_Date_Date_EList() {
		return bookingHandlerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__CancelBooking__int() {
		return bookingHandlerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__ClearAllBookings() {
		return bookingHandlerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__Bookingcatalogue() {
		return bookingHandlerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__GetFreeRooms__int_Date_Date() {
		return bookingHandlerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__GetFreeRoomTypesForPeriod__String_Date_Date() {
		return bookingHandlerEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookingHandler__GetBooking__int() {
		return bookingHandlerEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBookingStatus() {
		return bookingStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingFactory getBookingFactory() {
		return (BookingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		bookingCatalogueEClass = createEClass(BOOKING_CATALOGUE);
		createEReference(bookingCatalogueEClass, BOOKING_CATALOGUE__BOOKINGS);
		createEAttribute(bookingCatalogueEClass, BOOKING_CATALOGUE__CURRENT_BOOKING_ID);
		createEOperation(bookingCatalogueEClass, BOOKING_CATALOGUE___GET_BOOKING__INT);
		createEOperation(bookingCatalogueEClass, BOOKING_CATALOGUE___ADD_BOOKING__BOOKING);
		createEOperation(bookingCatalogueEClass, BOOKING_CATALOGUE___GET_NEW_BOOKING_ID);
		createEOperation(bookingCatalogueEClass, BOOKING_CATALOGUE___CLEAR_ALL_BOOKINGS);
		createEOperation(bookingCatalogueEClass, BOOKING_CATALOGUE___GET_BOOKINGS_FOR_PERIOD__DATE_DATE);

		bookingEClass = createEClass(BOOKING);
		createEAttribute(bookingEClass, BOOKING__FIRST_NAME);
		createEAttribute(bookingEClass, BOOKING__LAST_NAME);
		createEAttribute(bookingEClass, BOOKING__START_DATE);
		createEAttribute(bookingEClass, BOOKING__END_DATE);
		createEAttribute(bookingEClass, BOOKING__BOOKING_ID);
		createEReference(bookingEClass, BOOKING__ROOM_MAP);
		createEAttribute(bookingEClass, BOOKING__IS_PAID);
		createEReference(bookingEClass, BOOKING__ROOMS_ASSOCIATED);
		createEAttribute(bookingEClass, BOOKING__BOOKING_STATUS);
		createEOperation(bookingEClass, BOOKING___ADD_ROOMS_OF_ROOM_TYPE__ROOMTYPE_INT);
		createEOperation(bookingEClass, BOOKING___REPLACE_ROOM_MAP__ELIST);

		roomTypeToNumberEntryEClass = createEClass(ROOM_TYPE_TO_NUMBER_ENTRY);
		createEReference(roomTypeToNumberEntryEClass, ROOM_TYPE_TO_NUMBER_ENTRY__KEY);
		createEAttribute(roomTypeToNumberEntryEClass, ROOM_TYPE_TO_NUMBER_ENTRY__VALUE);

		bookingHandlerEClass = createEClass(BOOKING_HANDLER);
		createEReference(bookingHandlerEClass, BOOKING_HANDLER__ROOMHANDLER);
		createEReference(bookingHandlerEClass, BOOKING_HANDLER__BOOKINGCATALOGUE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_DATE_DATE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___CONFIRM_BOOKING__INT);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___ADD_EXTRA__INT_INT_STRING_FLOAT);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___LIST_BOOKINGS);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___EDIT_BOOKING__INT_ELIST);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE_ELIST);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___CANCEL_BOOKING__INT);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___CLEAR_ALL_BOOKINGS);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___BOOKINGCATALOGUE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___GET_FREE_ROOMS__INT_DATE_DATE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___GET_FREE_ROOM_TYPES_FOR_PERIOD__STRING_DATE_DATE);
		createEOperation(bookingHandlerEClass, BOOKING_HANDLER___GET_BOOKING__INT);

		// Create enums
		bookingStatusEEnum = createEEnum(BOOKING_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		IOPackage theIOPackage = (IOPackage)EPackage.Registry.INSTANCE.getEPackage(IOPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(bookingCatalogueEClass, BookingCatalogue.class, "BookingCatalogue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingCatalogue_Bookings(), this.getBooking(), null, "bookings", null, 0, -1, BookingCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBookingCatalogue_CurrentBookingId(), ecorePackage.getEInt(), "currentBookingId", "1", 1, 1, BookingCatalogue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = initEOperation(getBookingCatalogue__GetBooking__int(), this.getBooking(), "getBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingCatalogue__AddBooking__Booking(), null, "addBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getBooking(), "Booking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookingCatalogue__GetNewBookingId(), ecorePackage.getEInt(), "getNewBookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookingCatalogue__ClearAllBookings(), null, "clearAllBookings", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingCatalogue__GetBookingsForPeriod__Date_Date(), this.getBooking(), "getBookingsForPeriod", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(bookingEClass, Booking.class, "Booking", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooking_FirstName(), ecorePackage.getEString(), "firstName", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_LastName(), ecorePackage.getEString(), "lastName", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_StartDate(), ecorePackage.getEDate(), "startDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_EndDate(), ecorePackage.getEDate(), "endDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBooking_RoomMap(), this.getRoomTypeToNumberEntry(), null, "roomMap", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_IsPaid(), ecorePackage.getEBoolean(), "isPaid", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBooking_RoomsAssociated(), theRoomPackage.getRoom(), null, "roomsAssociated", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_BookingStatus(), this.getBookingStatus(), "bookingStatus", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getBooking__AddRoomsOfRoomType__RoomType_int(), null, "addRoomsOfRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "count", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__ReplaceRoomMap__EList(), null, "replaceRoomMap", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomTypeToNumberEntry(), "roomMap", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomTypeToNumberEntryEClass, RoomTypeToNumberEntry.class, "RoomTypeToNumberEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeToNumberEntry_Key(), theRoomPackage.getRoomType(), null, "key", null, 1, 1, RoomTypeToNumberEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomTypeToNumberEntry_Value(), ecorePackage.getEIntegerObject(), "value", null, 1, 1, RoomTypeToNumberEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingHandlerEClass, BookingHandler.class, "BookingHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingHandler_Roomhandler(), theRoomPackage.getRoomHandler(), null, "roomhandler", null, 1, 1, BookingHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookingHandler_Bookingcatalogue(), this.getBookingCatalogue(), null, "bookingcatalogue", null, 1, 1, BookingHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getBookingHandler__InitiateBooking__String_String_Date_Date(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__AddRoomToBooking__String_int(), ecorePackage.getEBoolean(), "addRoomToBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__ConfirmBooking__int(), ecorePackage.getEBoolean(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__AddExtra__int_int_String_float(), null, "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookingHandler__ListBookings(), this.getBooking(), "listBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__EditBooking__int_EList(), ecorePackage.getEBoolean(), "editBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomTypeToNumberEntry(), "roomTypesandCount", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__EditBooking__int_Date_Date(), ecorePackage.getEBoolean(), "editBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__EditBooking__int_Date_Date_EList(), ecorePackage.getEBoolean(), "editBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomTypeToNumberEntry(), "roomTypesandCount", 1, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__CancelBooking__int(), ecorePackage.getEBoolean(), "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookingHandler__ClearAllBookings(), null, "clearAllBookings", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookingHandler__Bookingcatalogue(), this.getBookingCatalogue(), "bookingcatalogue", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__GetFreeRooms__int_Date_Date(), theIOPackage.getFreeRoomTypesDTO(), "getFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__GetFreeRoomTypesForPeriod__String_Date_Date(), ecorePackage.getEInt(), "getFreeRoomTypesForPeriod", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBookingHandler__GetBooking__int(), this.getBooking(), "getBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(bookingStatusEEnum, BookingStatus.class, "BookingStatus");
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.UNCONFIRMED);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CONFIRMED);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CHECKED_IN);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CHECKED_OUT);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CANCELED);

		// Create resource
		createResource(eNS_URI);
	}

} //BookingPackageImpl
