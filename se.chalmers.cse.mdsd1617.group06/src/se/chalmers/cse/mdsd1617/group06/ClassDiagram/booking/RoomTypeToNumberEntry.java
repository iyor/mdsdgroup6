/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>room Type To Number Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getKey <em>Key</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage#getRoomTypeToNumberEntry()
 * @model
 * @generated
 */
public interface RoomTypeToNumberEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage#getRoomTypeToNumberEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getKey();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(RoomType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Integer)
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingPackage#getRoomTypeToNumberEntry_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Integer getValue();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Integer value);

} // roomTypeToNumberEntry
