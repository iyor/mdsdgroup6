package se.chalmers.cse.mdsd1617.group06.ClassDiagram.testing.unittest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomCatalogue;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomHandler;

public class UnitTestsRoom {

	RoomHandler rh;
	
	@Before
	public void init() {
		rh = RoomFactory.eINSTANCE.createRoomHandler();
		
	}
	
	@Test
	public void testRoomCatalogueSingleton() {
		RoomHandler rh1, rh2;
		rh1 = RoomFactory.eINSTANCE.createRoomHandler();
		rh2 = RoomFactory.eINSTANCE.createRoomHandler();
		// Make sure they are not the same object, by some ECore Voodoo
		assertNotEquals(rh1, rh2);
		// ... but make sure they have the same catalogue
		assertEquals(rh1.roomcatalogue(), rh2.roomcatalogue());
	}
	
	@Test
	public void testGetRoomsOfType() {
		rh.clearAllRooms();
		rh.startup(10);
		assertEquals(10, rh.roomcatalogue().getRoomsOfType("default").size());
	}
	
	@Test
	public void testAddDuplicateRooms(){
		rh.clearAllRooms();
		rh.roomcatalogue().clearAllRoomTypes();
		rh.addRoomType(1, "single room", 300.0);
		
		int roomid = 1;
		rh.addRoom(roomid, "single room");
		assertEquals("There should now be one room in the hotel", 
				1, rh.roomcatalogue().getRooms().size());
		//Attempting to add room with the same id
		rh.addRoom(roomid, "single room");
		assertEquals("There should still only be one room",
				1, rh.roomcatalogue().getRooms().size());
	}
	
	@Test
	public void testDuplicateRoomTypes(){
		rh.clearAllRooms();
		rh.roomcatalogue().clearAllRoomTypes();
		
		rh.addRoomType(1, "single bed", 300.0);
		assertEquals(1, rh.roomcatalogue().getRoomTypeList().size());
		
		rh.addRoomType(1, "single bed", 300.0);
		assertEquals(1, rh.roomcatalogue().getRoomTypeList().size());
	}

}
