/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Extras;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.Room;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomFactory;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomPackage;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomStatus;
import se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl#getRoomStatus <em>Room Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.impl.RoomImpl#getExtras <em>Extras</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends MinimalEObjectImpl.Container implements Room {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomStatus() <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomStatus()
	 * @generated
	 * @ordered
	 */
	protected static final RoomStatus ROOM_STATUS_EDEFAULT = RoomStatus.FREE;

	/**
	 * The cached value of the '{@link #getRoomStatus() <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomStatus()
	 * @generated
	 * @ordered
	 */
	protected RoomStatus roomStatus = ROOM_STATUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomType() <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomType()
	 * @generated
	 * @ordered
	 */
	protected RoomType roomType;

	/**
	 * The cached value of the '{@link #getExtras() <em>Extras</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtras()
	 * @generated
	 * @ordered
	 */
	protected EList<Extras> extras;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomImpl() {
		super();
		this.extras = new BasicEList<Extras>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType getRoomType() {
		if (roomType != null && roomType.eIsProxy()) {
			InternalEObject oldRoomType = (InternalEObject)roomType;
			roomType = (RoomType)eResolveProxy(oldRoomType);
			if (roomType != oldRoomType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM__ROOM_TYPE, oldRoomType, roomType));
			}
		}
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType basicGetRoomType() {
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomType(RoomType newRoomType) {
		RoomType oldRoomType = roomType;
		roomType = newRoomType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_TYPE, oldRoomType, roomType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extras> getExtras() {
		if (extras == null) {
			extras = new EObjectResolvingEList<Extras>(Extras.class, this, RoomPackage.ROOM__EXTRAS);
		}
		return extras;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isBlocked() {
		return this.roomStatus == RoomStatus.BLOCKED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setBlocked(boolean block) {
		this.roomStatus = block ? RoomStatus.BLOCKED : RoomStatus.FREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(String description, float price) {
		Extras newExtra = RoomFactory.eINSTANCE.createExtras();
		newExtra.setDescription(description);
		newExtra.setPrice(price);
		this.extras.add(newExtra);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomStatus getRoomStatus() {
		return roomStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomStatus(RoomStatus newRoomStatus) {
		RoomStatus oldRoomStatus = roomStatus;
		roomStatus = newRoomStatus == null ? ROOM_STATUS_EDEFAULT : newRoomStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_STATUS, oldRoomStatus, roomStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM__ID:
				return getId();
			case RoomPackage.ROOM__ROOM_STATUS:
				return getRoomStatus();
			case RoomPackage.ROOM__ROOM_TYPE:
				if (resolve) return getRoomType();
				return basicGetRoomType();
			case RoomPackage.ROOM__EXTRAS:
				return getExtras();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM__ID:
				setId((Integer)newValue);
				return;
			case RoomPackage.ROOM__ROOM_STATUS:
				setRoomStatus((RoomStatus)newValue);
				return;
			case RoomPackage.ROOM__ROOM_TYPE:
				setRoomType((RoomType)newValue);
				return;
			case RoomPackage.ROOM__EXTRAS:
				getExtras().clear();
				getExtras().addAll((Collection<? extends Extras>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__ID:
				setId(ID_EDEFAULT);
				return;
			case RoomPackage.ROOM__ROOM_STATUS:
				setRoomStatus(ROOM_STATUS_EDEFAULT);
				return;
			case RoomPackage.ROOM__ROOM_TYPE:
				setRoomType((RoomType)null);
				return;
			case RoomPackage.ROOM__EXTRAS:
				getExtras().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__ID:
				return id != ID_EDEFAULT;
			case RoomPackage.ROOM__ROOM_STATUS:
				return roomStatus != ROOM_STATUS_EDEFAULT;
			case RoomPackage.ROOM__ROOM_TYPE:
				return roomType != null;
			case RoomPackage.ROOM__EXTRAS:
				return extras != null && !extras.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM___IS_BLOCKED:
				return isBlocked();
			case RoomPackage.ROOM___SET_BLOCKED__BOOLEAN:
				setBlocked((Boolean)arguments.get(0));
				return null;
			case RoomPackage.ROOM___ADD_EXTRA__STRING_FLOAT:
				addExtra((String)arguments.get(0), (Float)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", roomStatus: ");
		result.append(roomStatus);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
