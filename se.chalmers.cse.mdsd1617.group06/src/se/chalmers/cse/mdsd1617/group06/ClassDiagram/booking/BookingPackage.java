/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group06/ClassDiagram/booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl <em>Catalogue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingCatalogue()
	 * @generated
	 */
	int BOOKING_CATALOGUE = 0;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE__BOOKINGS = 0;

	/**
	 * The feature id for the '<em><b>Current Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE__CURRENT_BOOKING_ID = 1;

	/**
	 * The number of structural features of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE___GET_BOOKING__INT = 0;

	/**
	 * The operation id for the '<em>Add Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE___ADD_BOOKING__BOOKING = 1;

	/**
	 * The operation id for the '<em>Get New Booking Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE___GET_NEW_BOOKING_ID = 2;

	/**
	 * The operation id for the '<em>Clear All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE___CLEAR_ALL_BOOKINGS = 3;

	/**
	 * The operation id for the '<em>Get Bookings For Period</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE___GET_BOOKINGS_FOR_PERIOD__DATE_DATE = 4;

	/**
	 * The number of operations of the '<em>Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CATALOGUE_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 1;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__FIRST_NAME = 0;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__LAST_NAME = 1;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__START_DATE = 2;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__END_DATE = 3;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_ID = 4;

	/**
	 * The feature id for the '<em><b>Room Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOM_MAP = 5;

	/**
	 * The feature id for the '<em><b>Is Paid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IS_PAID = 6;

	/**
	 * The feature id for the '<em><b>Rooms Associated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOMS_ASSOCIATED = 7;

	/**
	 * The feature id for the '<em><b>Booking Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_STATUS = 8;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 9;

	/**
	 * The operation id for the '<em>Add Rooms Of Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOMS_OF_ROOM_TYPE__ROOMTYPE_INT = 0;

	/**
	 * The operation id for the '<em>Replace Room Map</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___REPLACE_ROOM_MAP__ELIST = 1;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.RoomTypeToNumberEntryImpl <em>Room Type To Number Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.RoomTypeToNumberEntryImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getRoomTypeToNumberEntry()
	 * @generated
	 */
	int ROOM_TYPE_TO_NUMBER_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_NUMBER_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_NUMBER_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Room Type To Number Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_NUMBER_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room Type To Number Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_NUMBER_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingHandler()
	 * @generated
	 */
	int BOOKING_HANDLER = 3;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER__ROOMHANDLER = 0;

	/**
	 * The feature id for the '<em><b>Bookingcatalogue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER__BOOKINGCATALOGUE = 1;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_DATE_DATE = 0;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = 1;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CONFIRM_BOOKING__INT = 2;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___ADD_EXTRA__INT_INT_STRING_FLOAT = 3;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___LIST_BOOKINGS = 4;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___EDIT_BOOKING__INT_ELIST = 5;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE = 6;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE_ELIST = 7;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CANCEL_BOOKING__INT = 8;

	/**
	 * The operation id for the '<em>Clear All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CLEAR_ALL_BOOKINGS = 9;

	/**
	 * The operation id for the '<em>Bookingcatalogue</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___BOOKINGCATALOGUE = 10;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___GET_FREE_ROOMS__INT_DATE_DATE = 11;

	/**
	 * The operation id for the '<em>Get Free Room Types For Period</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___GET_FREE_ROOM_TYPES_FOR_PERIOD__STRING_DATE_DATE = 12;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___GET_BOOKING__INT = 13;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER_OPERATION_COUNT = 14;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingStatus()
	 * @generated
	 */
	int BOOKING_STATUS = 4;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue <em>Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Catalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue
	 * @generated
	 */
	EClass getBookingCatalogue();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bookings</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBookings()
	 * @see #getBookingCatalogue()
	 * @generated
	 */
	EReference getBookingCatalogue_Bookings();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getCurrentBookingId <em>Current Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getCurrentBookingId()
	 * @see #getBookingCatalogue()
	 * @generated
	 */
	EAttribute getBookingCatalogue_CurrentBookingId();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBooking(int)
	 * @generated
	 */
	EOperation getBookingCatalogue__GetBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#addBooking(se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking) <em>Add Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#addBooking(se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking)
	 * @generated
	 */
	EOperation getBookingCatalogue__AddBooking__Booking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getNewBookingId() <em>Get New Booking Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get New Booking Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getNewBookingId()
	 * @generated
	 */
	EOperation getBookingCatalogue__GetNewBookingId();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#clearAllBookings() <em>Clear All Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#clearAllBookings()
	 * @generated
	 */
	EOperation getBookingCatalogue__ClearAllBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBookingsForPeriod(java.util.Date, java.util.Date) <em>Get Bookings For Period</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Bookings For Period</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingCatalogue#getBookingsForPeriod(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingCatalogue__GetBookingsForPeriod__Date_Date();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getFirstName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getLastName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_LastName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getStartDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_StartDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getEndDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_EndDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getBookingId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingId();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getRoomMap <em>Room Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Room Map</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getRoomMap()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_RoomMap();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#isPaid <em>Is Paid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Paid</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#isPaid()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_IsPaid();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getRoomsAssociated <em>Rooms Associated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms Associated</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getRoomsAssociated()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_RoomsAssociated();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getBookingStatus <em>Booking Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#getBookingStatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingStatus();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#addRoomsOfRoomType(se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType, int) <em>Add Rooms Of Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Rooms Of Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#addRoomsOfRoomType(se.chalmers.cse.mdsd1617.group06.ClassDiagram.room.RoomType, int)
	 * @generated
	 */
	EOperation getBooking__AddRoomsOfRoomType__RoomType_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#replaceRoomMap(org.eclipse.emf.common.util.EList) <em>Replace Room Map</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Replace Room Map</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.Booking#replaceRoomMap(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBooking__ReplaceRoomMap__EList();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry <em>Room Type To Number Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type To Number Entry</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry
	 * @generated
	 */
	EClass getRoomTypeToNumberEntry();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getKey()
	 * @see #getRoomTypeToNumberEntry()
	 * @generated
	 */
	EReference getRoomTypeToNumberEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.RoomTypeToNumberEntry#getValue()
	 * @see #getRoomTypeToNumberEntry()
	 * @generated
	 */
	EAttribute getRoomTypeToNumberEntry_Value();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler
	 * @generated
	 */
	EClass getBookingHandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getBookingcatalogue <em>Bookingcatalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookingcatalogue</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getBookingcatalogue()
	 * @see #getBookingHandler()
	 * @generated
	 */
	EReference getBookingHandler_Bookingcatalogue();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getRoomhandler()
	 * @see #getBookingHandler()
	 * @generated
	 */
	EReference getBookingHandler_Roomhandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#initiateBooking(java.lang.String, java.lang.String, java.util.Date, java.util.Date) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#initiateBooking(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingHandler__InitiateBooking__String_String_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getBookingHandler__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#confirmBooking(int)
	 * @generated
	 */
	EOperation getBookingHandler__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#addExtra(int, int, java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#addExtra(int, int, java.lang.String, float)
	 * @generated
	 */
	EOperation getBookingHandler__AddExtra__int_int_String_float();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#listBookings()
	 * @generated
	 */
	EOperation getBookingHandler__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, org.eclipse.emf.common.util.EList) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBookingHandler__EditBooking__int_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, java.util.Date, java.util.Date) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingHandler__EditBooking__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, java.util.Date, java.util.Date, org.eclipse.emf.common.util.EList) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#editBooking(int, java.util.Date, java.util.Date, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBookingHandler__EditBooking__int_Date_Date_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#cancelBooking(int)
	 * @generated
	 */
	EOperation getBookingHandler__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#clearAllBookings() <em>Clear All Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear All Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#clearAllBookings()
	 * @generated
	 */
	EOperation getBookingHandler__ClearAllBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#bookingcatalogue() <em>Bookingcatalogue</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Bookingcatalogue</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#bookingcatalogue()
	 * @generated
	 */
	EOperation getBookingHandler__Bookingcatalogue();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getFreeRooms(int, java.util.Date, java.util.Date) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getFreeRooms(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingHandler__GetFreeRooms__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getFreeRoomTypesForPeriod(java.lang.String, java.util.Date, java.util.Date) <em>Get Free Room Types For Period</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Room Types For Period</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getFreeRoomTypesForPeriod(java.lang.String, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingHandler__GetFreeRoomTypesForPeriod__String_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingHandler#getBooking(int)
	 * @generated
	 */
	EOperation getBookingHandler__GetBooking__int();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus
	 * @generated
	 */
	EEnum getBookingStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl <em>Catalogue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingCatalogueImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingCatalogue()
		 * @generated
		 */
		EClass BOOKING_CATALOGUE = eINSTANCE.getBookingCatalogue();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_CATALOGUE__BOOKINGS = eINSTANCE.getBookingCatalogue_Bookings();

		/**
		 * The meta object literal for the '<em><b>Current Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_CATALOGUE__CURRENT_BOOKING_ID = eINSTANCE.getBookingCatalogue_CurrentBookingId();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CATALOGUE___GET_BOOKING__INT = eINSTANCE.getBookingCatalogue__GetBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CATALOGUE___ADD_BOOKING__BOOKING = eINSTANCE.getBookingCatalogue__AddBooking__Booking();

		/**
		 * The meta object literal for the '<em><b>Get New Booking Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CATALOGUE___GET_NEW_BOOKING_ID = eINSTANCE.getBookingCatalogue__GetNewBookingId();

		/**
		 * The meta object literal for the '<em><b>Clear All Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CATALOGUE___CLEAR_ALL_BOOKINGS = eINSTANCE.getBookingCatalogue__ClearAllBookings();

		/**
		 * The meta object literal for the '<em><b>Get Bookings For Period</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CATALOGUE___GET_BOOKINGS_FOR_PERIOD__DATE_DATE = eINSTANCE.getBookingCatalogue__GetBookingsForPeriod__Date_Date();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__FIRST_NAME = eINSTANCE.getBooking_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__LAST_NAME = eINSTANCE.getBooking_LastName();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__START_DATE = eINSTANCE.getBooking_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__END_DATE = eINSTANCE.getBooking_EndDate();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_ID = eINSTANCE.getBooking_BookingId();

		/**
		 * The meta object literal for the '<em><b>Room Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOM_MAP = eINSTANCE.getBooking_RoomMap();

		/**
		 * The meta object literal for the '<em><b>Is Paid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__IS_PAID = eINSTANCE.getBooking_IsPaid();

		/**
		 * The meta object literal for the '<em><b>Rooms Associated</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOMS_ASSOCIATED = eINSTANCE.getBooking_RoomsAssociated();

		/**
		 * The meta object literal for the '<em><b>Booking Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_STATUS = eINSTANCE.getBooking_BookingStatus();

		/**
		 * The meta object literal for the '<em><b>Add Rooms Of Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_ROOMS_OF_ROOM_TYPE__ROOMTYPE_INT = eINSTANCE.getBooking__AddRoomsOfRoomType__RoomType_int();

		/**
		 * The meta object literal for the '<em><b>Replace Room Map</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___REPLACE_ROOM_MAP__ELIST = eINSTANCE.getBooking__ReplaceRoomMap__EList();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.RoomTypeToNumberEntryImpl <em>Room Type To Number Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.RoomTypeToNumberEntryImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getRoomTypeToNumberEntry()
		 * @generated
		 */
		EClass ROOM_TYPE_TO_NUMBER_ENTRY = eINSTANCE.getRoomTypeToNumberEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_TO_NUMBER_ENTRY__KEY = eINSTANCE.getRoomTypeToNumberEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE_TO_NUMBER_ENTRY__VALUE = eINSTANCE.getRoomTypeToNumberEntry_Value();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingHandler()
		 * @generated
		 */
		EClass BOOKING_HANDLER = eINSTANCE.getBookingHandler();

		/**
		 * The meta object literal for the '<em><b>Bookingcatalogue</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_HANDLER__BOOKINGCATALOGUE = eINSTANCE.getBookingHandler_Bookingcatalogue();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_HANDLER__ROOMHANDLER = eINSTANCE.getBookingHandler_Roomhandler();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_DATE_DATE = eINSTANCE.getBookingHandler__InitiateBooking__String_String_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getBookingHandler__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___CONFIRM_BOOKING__INT = eINSTANCE.getBookingHandler__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___ADD_EXTRA__INT_INT_STRING_FLOAT = eINSTANCE.getBookingHandler__AddExtra__int_int_String_float();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___LIST_BOOKINGS = eINSTANCE.getBookingHandler__ListBookings();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___EDIT_BOOKING__INT_ELIST = eINSTANCE.getBookingHandler__EditBooking__int_EList();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE = eINSTANCE.getBookingHandler__EditBooking__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___EDIT_BOOKING__INT_DATE_DATE_ELIST = eINSTANCE.getBookingHandler__EditBooking__int_Date_Date_EList();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___CANCEL_BOOKING__INT = eINSTANCE.getBookingHandler__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Clear All Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___CLEAR_ALL_BOOKINGS = eINSTANCE.getBookingHandler__ClearAllBookings();

		/**
		 * The meta object literal for the '<em><b>Bookingcatalogue</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___BOOKINGCATALOGUE = eINSTANCE.getBookingHandler__Bookingcatalogue();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___GET_FREE_ROOMS__INT_DATE_DATE = eINSTANCE.getBookingHandler__GetFreeRooms__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Free Room Types For Period</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___GET_FREE_ROOM_TYPES_FOR_PERIOD__STRING_DATE_DATE = eINSTANCE.getBookingHandler__GetFreeRoomTypesForPeriod__String_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_HANDLER___GET_BOOKING__INT = eINSTANCE.getBookingHandler__GetBooking__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.BookingStatus
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.booking.impl.BookingPackageImpl#getBookingStatus()
		 * @generated
		 */
		EEnum BOOKING_STATUS = eINSTANCE.getBookingStatus();

	}

} //BookingPackage
