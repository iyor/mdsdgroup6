/**
 */
package se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IOFactory
 * @model kind="package"
 * @generated
 */
public interface IOPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "IO";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group06/ClassDiagram/IO.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IOPackage eINSTANCE = se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 6;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 7;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.FreeRoomTypesDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 1;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 2;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelCustomerProvides()
	 * @generated
	 */
	int HOTEL_CUSTOMER_PROVIDES = 3;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Convert Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONVERT_DATE__STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelStartupProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelStartupProvides()
	 * @generated
	 */
	int HOTEL_STARTUP_PROVIDES = 4;

	/**
	 * The feature id for the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__CHECKIOHANDLER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__BOOKINGHANDLER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__ROOMHANDLER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator <em>IAdministrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIAdministrator()
	 * @generated
	 */
	int IADMINISTRATOR = 5;

	/**
	 * The number of structural features of the '<em>IAdministrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = 0;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___BLOCK_ROOM__INT = 1;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___UNBLOCK_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___ADD_ROOM__INT_STRING = 3;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___REMOVE_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Change Room Type Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___CHANGE_ROOM_TYPE_ROOM__INT_STRING = 5;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___ADD_ROOM_TYPE__INT_STRING_DOUBLE = 6;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = 7;

	/**
	 * The number of operations of the '<em>IAdministrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMINISTRATOR_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist <em>IReceptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIReceptionist()
	 * @generated
	 */
	int IRECEPTIONIST = 6;

	/**
	 * The number of structural features of the '<em>IReceptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING_ELIST = 0;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___EDIT_BOOKING__INT_ELIST = 1;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING = 2;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CANCEL_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___ADD_EXTRA__INT_STRING_FLOAT = 4;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___INITIATE_CHECK_IN__INT = 5;

	/**
	 * The operation id for the '<em>Check In Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHECK_IN_ROOM_BY_ID__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 8;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___INITIATE_CHECKOUT__INT = 9;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT = 10;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE = 11;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_CHECK_INS__DATE = 12;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_CHECK_INS__DATE_DATE = 13;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_CHECK_OUTS__DATE = 14;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE = 15;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_FLOAT = 16;

	/**
	 * The number of operations of the '<em>IReceptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_OPERATION_COUNT = 17;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl <em>Hotel Administrator Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelAdministratorProvides()
	 * @generated
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES = 7;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER = IADMINISTRATOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER = IADMINISTRATOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER = IADMINISTRATOR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT = IADMINISTRATOR_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = IADMINISTRATOR___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT = IADMINISTRATOR___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT = IADMINISTRATOR___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING = IADMINISTRATOR___ADD_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT = IADMINISTRATOR___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Change Room Type Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___CHANGE_ROOM_TYPE_ROOM__INT_STRING = IADMINISTRATOR___CHANGE_ROOM_TYPE_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__INT_STRING_DOUBLE = IADMINISTRATOR___ADD_ROOM_TYPE__INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = IADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT = IADMINISTRATOR_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Hotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES_OPERATION_COUNT = IADMINISTRATOR_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl <em>Hotel Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelReceptionistProvides()
	 * @generated
	 */
	int HOTEL_RECEPTIONIST_PROVIDES = 8;

	/**
	 * The feature id for the '<em><b>Roomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Checkiohandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING_ELIST = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_ELIST = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_STRING_FLOAT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Check In Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM_BY_ID__INT_INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING_1 = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_1 = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT_1 = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT_1 = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 14;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 15;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA__INT_INT_STRING_FLOAT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 16;

	/**
	 * The operation id for the '<em>Convert Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CONVERT_DATE__STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 17;

	/**
	 * The number of operations of the '<em>Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 18;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides
	 * @generated
	 */
	EClass getHotelCustomerProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getRoomhandler()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Roomhandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getCheckiohandler <em>Checkiohandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Checkiohandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getCheckiohandler()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Checkiohandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getBookinghandler <em>Bookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#getBookinghandler()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Bookinghandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#convertDate(java.lang.String) <em>Convert Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Convert Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelCustomerProvides#convertDate(java.lang.String)
	 * @generated
	 */
	EOperation getHotelCustomerProvides__ConvertDate__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides
	 * @generated
	 */
	EClass getHotelStartupProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getCheckiohandler <em>Checkiohandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Checkiohandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getCheckiohandler()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Checkiohandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getBookinghandler <em>Bookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getBookinghandler()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Bookinghandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelStartupProvides#getRoomhandler()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Roomhandler();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator <em>IAdministrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAdministrator</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator
	 * @generated
	 */
	EClass getIAdministrator();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#addRoomType(int, java.lang.String, double) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#addRoomType(int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIAdministrator__AddRoomType__int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#updateRoomType(java.lang.String, int, java.lang.String, double) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#updateRoomType(java.lang.String, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIAdministrator__UpdateRoomType__String_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIAdministrator__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#blockRoom(int)
	 * @generated
	 */
	EOperation getIAdministrator__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#unblockRoom(int)
	 * @generated
	 */
	EOperation getIAdministrator__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdministrator__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#removeRoom(int)
	 * @generated
	 */
	EOperation getIAdministrator__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#changeRoomTypeRoom(int, java.lang.String) <em>Change Room Type Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator#changeRoomTypeRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdministrator__ChangeRoomTypeRoom__int_String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist <em>IReceptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IReceptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist
	 * @generated
	 */
	EClass getIReceptionist();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, java.lang.String, java.lang.String, org.eclipse.emf.common.util.EList) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, java.lang.String, java.lang.String, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIReceptionist__EditBooking__int_String_String_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, org.eclipse.emf.common.util.EList) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIReceptionist__EditBooking__int_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, java.lang.String, java.lang.String) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#editBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionist__EditBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#cancelBooking(int)
	 * @generated
	 */
	EOperation getIReceptionist__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#addExtra(int, java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#addExtra(int, java.lang.String, float)
	 * @generated
	 */
	EOperation getIReceptionist__AddExtra__int_String_float();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateCheckIn(int) <em>Initiate Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateCheckIn(int)
	 * @generated
	 */
	EOperation getIReceptionist__InitiateCheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#checkInRoomById(int, int) <em>Check In Room By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room By Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#checkInRoomById(int, int)
	 * @generated
	 */
	EOperation getIReceptionist__CheckInRoomById__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionist__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionist__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIReceptionist__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIReceptionist__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listOccupiedRooms(java.util.Date) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ListOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckIns(java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckIns(java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ListCheckIns__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckOuts(java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckOuts(java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ListCheckOuts__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#addExtra(int, int, java.lang.String, float) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist#addExtra(int, int, java.lang.String, float)
	 * @generated
	 */
	EOperation getIReceptionist__AddExtra__int_int_String_float();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides <em>Hotel Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Administrator Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides
	 * @generated
	 */
	EClass getHotelAdministratorProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getRoomhandler()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Roomhandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getCheckiohandler <em>Checkiohandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Checkiohandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getCheckiohandler()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Checkiohandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getBookinghandler <em>Bookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelAdministratorProvides#getBookinghandler()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Bookinghandler();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides <em>Hotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Receptionist Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides
	 * @generated
	 */
	EClass getHotelReceptionistProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getRoomhandler <em>Roomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getRoomhandler()
	 * @see #getHotelReceptionistProvides()
	 * @generated
	 */
	EReference getHotelReceptionistProvides_Roomhandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getCheckiohandler <em>Checkiohandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Checkiohandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getCheckiohandler()
	 * @see #getHotelReceptionistProvides()
	 * @generated
	 */
	EReference getHotelReceptionistProvides_Checkiohandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getBookinghandler <em>Bookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#getBookinghandler()
	 * @see #getHotelReceptionistProvides()
	 * @generated
	 */
	EReference getHotelReceptionistProvides_Bookinghandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#convertDate(java.lang.String) <em>Convert Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Convert Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.HotelReceptionistProvides#convertDate(java.lang.String)
	 * @generated
	 */
	EOperation getHotelReceptionistProvides__ConvertDate__String();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IOFactory getIOFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.FreeRoomTypesDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IHotelStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelCustomerProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelCustomerProvides()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__ROOMHANDLER = eINSTANCE.getHotelCustomerProvides_Roomhandler();

		/**
		 * The meta object literal for the '<em><b>Checkiohandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__CHECKIOHANDLER = eINSTANCE.getHotelCustomerProvides_Checkiohandler();

		/**
		 * The meta object literal for the '<em><b>Bookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__BOOKINGHANDLER = eINSTANCE.getHotelCustomerProvides_Bookinghandler();

		/**
		 * The meta object literal for the '<em><b>Convert Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HOTEL_CUSTOMER_PROVIDES___CONVERT_DATE__STRING = eINSTANCE.getHotelCustomerProvides__ConvertDate__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelStartupProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelStartupProvides()
		 * @generated
		 */
		EClass HOTEL_STARTUP_PROVIDES = eINSTANCE.getHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Checkiohandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__CHECKIOHANDLER = eINSTANCE.getHotelStartupProvides_Checkiohandler();

		/**
		 * The meta object literal for the '<em><b>Bookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__BOOKINGHANDLER = eINSTANCE.getHotelStartupProvides_Bookinghandler();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__ROOMHANDLER = eINSTANCE.getHotelStartupProvides_Roomhandler();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator <em>IAdministrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IAdministrator
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIAdministrator()
		 * @generated
		 */
		EClass IADMINISTRATOR = eINSTANCE.getIAdministrator();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___ADD_ROOM_TYPE__INT_STRING_DOUBLE = eINSTANCE.getIAdministrator__AddRoomType__int_String_double();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_INT_STRING_DOUBLE = eINSTANCE.getIAdministrator__UpdateRoomType__String_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIAdministrator__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___BLOCK_ROOM__INT = eINSTANCE.getIAdministrator__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___UNBLOCK_ROOM__INT = eINSTANCE.getIAdministrator__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___ADD_ROOM__INT_STRING = eINSTANCE.getIAdministrator__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___REMOVE_ROOM__INT = eINSTANCE.getIAdministrator__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMINISTRATOR___CHANGE_ROOM_TYPE_ROOM__INT_STRING = eINSTANCE.getIAdministrator__ChangeRoomTypeRoom__int_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist <em>IReceptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.IReceptionist
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getIReceptionist()
		 * @generated
		 */
		EClass IRECEPTIONIST = eINSTANCE.getIReceptionist();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING_ELIST = eINSTANCE.getIReceptionist__EditBooking__int_String_String_EList();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___EDIT_BOOKING__INT_ELIST = eINSTANCE.getIReceptionist__EditBooking__int_EList();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___EDIT_BOOKING__INT_STRING_STRING = eINSTANCE.getIReceptionist__EditBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CANCEL_BOOKING__INT = eINSTANCE.getIReceptionist__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___ADD_EXTRA__INT_STRING_FLOAT = eINSTANCE.getIReceptionist__AddExtra__int_String_float();

		/**
		 * The meta object literal for the '<em><b>Initiate Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___INITIATE_CHECK_IN__INT = eINSTANCE.getIReceptionist__InitiateCheckIn__int();

		/**
		 * The meta object literal for the '<em><b>Check In Room By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHECK_IN_ROOM_BY_ID__INT_INT = eINSTANCE.getIReceptionist__CheckInRoomById__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIReceptionist__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIReceptionist__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___INITIATE_CHECKOUT__INT = eINSTANCE.getIReceptionist__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIReceptionist__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE = eINSTANCE.getIReceptionist__ListOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_CHECK_INS__DATE = eINSTANCE.getIReceptionist__ListCheckIns__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getIReceptionist__ListCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_CHECK_OUTS__DATE = eINSTANCE.getIReceptionist__ListCheckOuts__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getIReceptionist__ListCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_FLOAT = eINSTANCE.getIReceptionist__AddExtra__int_int_String_float();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl <em>Hotel Administrator Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelAdministratorProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelAdministratorProvides()
		 * @generated
		 */
		EClass HOTEL_ADMINISTRATOR_PROVIDES = eINSTANCE.getHotelAdministratorProvides();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__ROOMHANDLER = eINSTANCE.getHotelAdministratorProvides_Roomhandler();

		/**
		 * The meta object literal for the '<em><b>Checkiohandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__CHECKIOHANDLER = eINSTANCE.getHotelAdministratorProvides_Checkiohandler();

		/**
		 * The meta object literal for the '<em><b>Bookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__BOOKINGHANDLER = eINSTANCE.getHotelAdministratorProvides_Bookinghandler();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl <em>Hotel Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.HotelReceptionistProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group06.ClassDiagram.IO.impl.IOPackageImpl#getHotelReceptionistProvides()
		 * @generated
		 */
		EClass HOTEL_RECEPTIONIST_PROVIDES = eINSTANCE.getHotelReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Roomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_RECEPTIONIST_PROVIDES__ROOMHANDLER = eINSTANCE.getHotelReceptionistProvides_Roomhandler();

		/**
		 * The meta object literal for the '<em><b>Checkiohandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_RECEPTIONIST_PROVIDES__CHECKIOHANDLER = eINSTANCE.getHotelReceptionistProvides_Checkiohandler();

		/**
		 * The meta object literal for the '<em><b>Bookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_RECEPTIONIST_PROVIDES__BOOKINGHANDLER = eINSTANCE.getHotelReceptionistProvides_Bookinghandler();

		/**
		 * The meta object literal for the '<em><b>Convert Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation HOTEL_RECEPTIONIST_PROVIDES___CONVERT_DATE__STRING = eINSTANCE.getHotelReceptionistProvides__ConvertDate__String();

	}

} //IOPackage
