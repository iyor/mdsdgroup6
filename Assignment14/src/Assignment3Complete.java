public class Assignment3Complete {
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;
	private static final int MAX_ROOMS = 3;
	private final long[] roomReservation = new long[MAX_ROOMS];
	private long[] bookingMessage = new long[1];
	private boolean reservationInitialized = false;
	
	public long initiateBooking() {
		return ++currentBookingId;
		
	}

	public boolean addRoomToBooking(long bookingId) {
		if (bookingId < 1|| bookingId > currentBookingId) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		} else {
			++currentReservationNumber;
			return true;
		}
	}
	
	public boolean freeRoom() {
		if (currentReservationNumber > 0){
			--currentReservationNumber;
			return true;
		}
		return false;
	}
	
	public void changeReservations(long roomsToHandle){
		bookingMessage = new long[(int)roomsToHandle];
	}
	
	public void setReservationID(long roomsToHandle, long reservationID){
		bookingMessage[bookingMessage.length - (int) roomsToHandle] = reservationID;
	}
	
	public long getReservationID(long roomsToHandle){
		int index = bookingMessage.length - (int) roomsToHandle;
		return bookingMessage[index];
	}
	
	public boolean checkOut(long roomNumber)
	{
		long reservationID = (long) roomReservation[(int)roomNumber];
		if (reservationID < 0) {return false;}
		for (int i=0; i < bookingMessage.length; i++ ){
			if (bookingMessage[i]==reservationID)
			{
				return true;
			}
		} 
		return false;
	}
	
	// Rooms
	public void addReservationValue(long roomNumber, long reservationID){
		if(!reservationInitialized)
		{
			for (int i = 0; i < MAX_ROOMS; i++)
			{
				roomReservation[i]=-2;
			}
			reservationInitialized = !reservationInitialized;
		}
		roomReservation[(int) roomNumber] = reservationID;
	}
	
	public void removeReservationValue(long roomNumber) {
		roomReservation[(int) roomNumber] = -2;
	}
	
	public long getReservationValue(long roomNumber){
		return (long) roomReservation[(int) roomNumber];
	}
	
	public long getMaxRooms() {
		return MAX_ROOMS ;
	}
}
